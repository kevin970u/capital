<div class="container px-6 mx-auto grid">
    <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
        Inicio
    </h2>
    <!-- CTA -->
    <a class="flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple"
        href="<?php echo site_url('cajeros/index'); ?>">
        <div class="flex items-center">
            <i class="bi bi-flag-fill"></i> &nbsp;
            <span>Estamos en todas las provincias del Ecuador para atenderte en tus gestiones
                bancarias.</span>
        </div>
    </a>
    <!-- Cards -->
    <div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4">
        <!-- Card -->
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-building-check" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Institucion
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    Banco Capital
                </p>
            </div>
        </div>
        <!-- Card -->
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-orange-500 bg-orange-100 rounded-full dark:text-orange-100 dark:bg-orange-500">
                <i class="bi bi-bank" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Total Agencias
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php if (is_array($agencias) && count($agencias) > 0): ?>
                        <?php echo count($agencias); ?>
                    <?php else: ?>
                        0
                    <?php endif; ?>
                </p>
            </div>
        </div>
        <!-- Card -->
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-full dark:text-green-100 dark:bg-green-500">
                <i class="bi bi-cash-coin" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Total Cajeros Automáticos
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php if (is_array($cajeros) && count($cajeros) > 0): ?>
                        <?php echo count($cajeros); ?>
                    <?php else: ?>
                        0
                    <?php endif; ?>
                </p>
            </div>
        </div>
        <!-- Card -->
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-blue-500 bg-blue-100 rounded-full dark:text-blue-100 dark:bg-blue-500">
                <i class="bi bi-shop" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Total Corresponsales
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php if (is_array($corresponsales) && count($corresponsales) > 0): ?>
                        <?php echo count($corresponsales); ?>
                    <?php else: ?>
                        0
                    <?php endif; ?>
                </p>
            </div>
        </div>
    </div>

    <div id="reporteMapa" style="width:100%;height:900px;border:0;"></div>
</div>

<script>
    function initMap() {
        var coordenadaCentral = new google.maps.LatLng(-0.2805707076097901, -78.53181468129077);
        var miMapa = new google.maps.Map(document.getElementById('reporteMapa'), {
            center: coordenadaCentral,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        <?php if (is_array($bancos) && count($bancos) > 0): ?>
            <?php foreach ($bancos as $banco): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $banco->latitud_ban; ?>,
                    <?php echo $banco->longitud_ban; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Matriz: <?php echo $banco->nombre_ban; ?>",
                    icon: '<?php echo base_url('assets/img/matriz.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (is_array($agencias) && count($agencias) > 0): ?>
            <?php foreach ($agencias as $agencia): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $agencia->latitud_age; ?>,
                    <?php echo $agencia->longitud_age; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Agencia: <?php echo $agencia->direccion_age; ?>",
                    icon: '<?php echo base_url('assets/img/bank.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (is_array($cajeros) && count($cajeros) > 0): ?>
            <?php foreach ($cajeros as $cajero): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $cajero->latitud_caj; ?>,
                    <?php echo $cajero->longitud_caj; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Cajero Automatico " + " <?php echo $cajero->direccion_caj; ?>",
                    icon: '<?php echo base_url('assets/img/cajero.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (is_array($corresponsales) && count($corresponsales) > 0): ?>
            <?php foreach ($corresponsales as $corresponsal): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $corresponsal->latitud_cor; ?>,
                    <?php echo $corresponsal->longitud_cor; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Corresponsal Bancario: <?php echo $corresponsal->tipo_negocio_cor; ?>",
                    icon: '<?php echo base_url('assets/img/corresponsal.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
    }
</script>