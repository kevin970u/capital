<link rel="stylesheet" href="<?php echo base_url('assets/library/boostrap/bootstrap.css'); ?>">
<div class="container px-6 flex justify-center">
    <!-- Modal body -->
    <div class="mt-4 mb-6" style="width:70%;">
        <h2 class="mt-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Editar datos de la Institucion
        </h2>
        <!-- Modal description -->
        <p class="text-sm text-gray-500 dark:text-gray-400">
            Por favor, ingrese los datos de la institucion.
        </p>

        <form id="form-editar-banco" enctype="multipart/form-data"
            action="<?php echo site_url('bancos/actualizarBanco') ?>" method="POST"
            class="w-full flex flex-col items-center justify-center px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">
             <input type="hidden" name="id_ban" value="<?php echo $banco->id_ban; ?>">
            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Nombre</span>
                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                value="<?php echo $banco->nombre_ban; ?>"
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="Banco Capital" name="nombre_ban" id="nombre_ban"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-braces-asterisk w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>

                <div class=" ml-2 mt-4" style="width:50%">
                    <label class="block text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Telefono</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                value="<?php echo $banco->telefono_ban; ?>"
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="0905485874" name="telefono_ban" id="telefono_ban"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-braces-asterisk w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Email</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                value="<?php echo $banco->email_ban; ?>" type="email"
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="banco12@gmail.com" name="email_ban" id="email_ban"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-modem w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>

                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Fecha Apertura</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                value="<?php echo $banco->fecha_apertura_ban; ?>"
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                type="date" name="fecha_apertura_ban" id="fecha_apertura_ban"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-braces-asterisk w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Provincia</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                name="provincia_ban" id="provincia_ban" placeholder="Pichincha" value="<?php echo $banco->provincia_ban; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-buildings w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>

                <div class="ml-2 mt-4" style="width:50%">
                    <label class="block text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Ciudad</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="Quito" name="ciudad_ban"
                                id="ciudad_ban" value="<?php echo $banco->ciudad_ban; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-calendar-date-fill w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="flex flex-row  w-full">
                <div class="w-full mr-2 mt-4">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Direccion</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="Av 10 de Diciembre frente al parque" name="direccion_ban"
                                id="direccion_ban" value="<?php echo $banco->direccion_ban; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-signpost-split w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
            <div class="flex flex-row  w-full">
                <div class="w-full mr-2 mt-4">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Mision</span>
                            <textarea required
                                class="block w-full mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="Mision" name="mision_ban"
                                id="mision_ban" rows="5"
                            ><?php echo $banco->mision_ban; ?></textarea>
                    </label>
                </div>
            </div>
            <div class="flex flex-row  w-full">
                <div class="w-full mr-2 mt-4">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Vision</span>
                            <textarea required
                                class="block w-full mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="Vision" name="vision_ban"
                                id="vision_ban"
                                rows="3"
                            >
                                <?php echo $banco->vision_ban; ?>
                            </textarea>
                    </label>
                </div>
            </div>

            <div class="flex flex-row  w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Logo Actual</span>

                        <?php if (!empty($banco->logo_ban)): ?>

                            <a target="_blank"
                                href="<?php echo base_url('uploads/bancos/') . $banco->logo_ban; ?>">
                                <img width="300"
                                    src="<?php echo base_url('uploads/bancos/') . $banco->logo_ban; ?>" alt="">
                            </a>
                        <?php else: ?>
                            <p>No hay logo</p>
                        <?php endif ?>
                    </label>
                </div>
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black"> Cambiar Logo Agencia</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                type="file" accept=".png" name="logo_ban" id="logo_ban" />
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-signpost-split w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>


            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Latitud</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="readonly block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="0.0000000000" readonly name="latitud_ban" id="latitud_ban" value="<?php echo $banco->latitud_ban; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-geo w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black font-black">Longitud</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="readonly block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="0.0000000000" readonly name="longitud_ban" id="longitud_ban" value="<?php echo $banco->longitud_ban; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-geo w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <br>
            <br>
            <div id='mapa' style="height: 400px; width: 100%;margin-bottom:2rem;"></div>

            <footer
                class="flex flex-col items-center justify-end px-6 py-3 -mx-6 -mb-4 space-y-4 sm:space-y-0 sm:space-x-6 sm:flex-row bg-gray-50 dark:bg-gray-800">
                <a href="<?php echo site_url('bancos/institucion'); ?>"
                    class="w-full px-5 py-3 text-sm font-medium leading-5 text-white text-gray-700 transition-colors duration-150 border border-gray-300  dark:text-gray-400 font-black sm:px-4 sm:py-2 sm:w-auto active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray">
                    Cancelar
                </a>
                <button type="submit"
                    class="w-full px-5 py-3 text-sm font-medium leading-5 text-white transition-colors duration-150 bg-purple-600 border border-transparent sm:w-auto sm:px-4 sm:py-2 active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">
                    Actualizar Informacion
                </button>
            </footer>
        </form>
    </div>
</div>

<script>
    function initMap() {
        const coordCentral = new google.maps.LatLng(<?php echo $banco->latitud_ban; ?>, <?php echo $banco->longitud_ban; ?>);
        const mapa = document.getElementById('mapa');
        const miMapa = new google.maps.Map(mapa, {
            center: coordCentral,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        })

        const marcador = new google.maps.Marker({
            position: coordCentral,
            map: miMapa,
            title: 'Selecciona la ubicacion de la Institucion',
            draggable: true,
            icon: '<?php echo base_url('assets/img/matriz.svg') ?>'
        })

        google.maps.event.addListener(
            marcador,
            'dragend',
            function () {
                const latitud = this.getPosition().lat();
                const longitud = this.getPosition().lng();

                document.getElementById('latitud_ban').value = latitud;
                document.getElementById('longitud_ban').value = longitud;
            }
        )
    }
</script>


<script>

    $(document).ready(function () {
        $("#logo_ban").fileinput({
            lenguage: "es",
            maxFileSize: 5000,
            showUpload: false,
        });
    });

    const today = new Date().toISOString().split('T')[0];
    document.getElementById('fecha_apertura_ban').setAttribute('max', today);
</script>