<div class="container px-6 mx-auto grid">
    <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
       Institucion
    </h2>
    <!-- CTA -->
    <a class="flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple"
        href="<?php echo site_url('bancos/index'); ?>">
        <div class="flex items-center">
            <i class="bi bi-flag-fill"></i> &nbsp;
            <span>Estamos en todas las provincias del Ecuador para atenderte en tus gestiones
                bancarias.</span>
        </div>
    </a>

    <div class="mb-4">
        <a href="<?php echo site_url('bancos/editar'); ?>"
            class="px-4 py-2 text-sm font-medium leading-5 text-white transition-colors duration-150 bg-purple-600 border border-transparent active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">
           Editar Informacion
        </a>
    </div>

    <!-- Cards -->
    <div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4">
        <!-- Card -->
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-building-check" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Institucion
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php echo $banco->nombre_ban; ?>
                </p>
            </div>
        </div>
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-envelope-at" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Email
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php echo $banco->email_ban; ?>
                </p>
            </div>
        </div>
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
             <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-telephone-x" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Telefono
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php echo $banco->telefono_ban; ?>
                </p>
            </div>
        </div>
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Logo
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                        <?php if (!empty($banco->logo_ban)): ?>
                            <a target="_blank"
                                href="<?php echo base_url('uploads/bancos/') . $banco->logo_ban; ?>">
                                <img width="200"
                                    src="<?php echo base_url('uploads/bancos/') . $banco->logo_ban; ?>" alt="Logo Banco">
                            </a>
                        <?php else: ?>
                            <p>No hay logo</p>
                        <?php endif ?>
                </p>
            </div>
        </div>

        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-flag" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Provincia Matriz
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php echo $banco->provincia_ban; ?>
                </p>
            </div>
        </div>
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-buildings" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Ciudad
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php echo $banco->ciudad_ban; ?>
                </p>
            </div>
        </div>
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
             <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-signpost" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Direccion
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php echo $banco->direccion_ban; ?>
                </p>
            </div>
        </div>
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-calendar-date" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Fecha Apertura
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php echo $banco->fecha_apertura_ban; ?>
                </p>
            </div>
        </div>
    </div>

    <div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-2">
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
             <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-award" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Mision
                </p>
                <p class="text-lg  text-gray-700 dark:text-gray-200">
                    <?php echo $banco->mision_ban; ?>
                </p>
            </div>
        </div>
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-full dark:text-red-100 dark:bg-red-500">
                <i class="bi bi-bookmark-star" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Vision
                </p>
                <p class="text-lg  text-gray-700 dark:text-gray-200">
                    <?php echo $banco->vision_ban; ?>
                </p>
            </div>
        </div>
    </div>

     <div id="mapa" style="width:100%;height:900px;border:0;"></div>
</div>

<script>
    function initMap() {
        const coordCentral = new google.maps.LatLng(<?php echo $banco->latitud_ban; ?>, <?php echo $banco->longitud_ban; ?>);
        const mapa = document.getElementById('mapa');
        const miMapa = new google.maps.Map(mapa, {
            center: coordCentral,
            zoom: 19,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        })

        const marcador = new google.maps.Marker({
            position: coordCentral,
            map: miMapa,
            title: 'Matriz: <?php echo $banco->nombre_ban; ?> - <?php echo $banco->direccion_ban; ?>',
            icon: '<?php echo base_url('assets/img/matriz.svg') ?>'
        })
    }

</script>