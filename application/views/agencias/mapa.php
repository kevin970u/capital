<div class="container px-6 mx-auto grid">
    <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
        Agencias Bancarias
    </h2>

    <!-- Cards -->
    <div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4">
        <!-- Card -->
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
            <div class="p-3 mr-4 text-orange-500 bg-orange-100 rounded-full dark:text-orange-100 dark:bg-orange-500">
                <i class="bi bi-bank" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Total Agencias
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php if(is_array($agencias) && count($agencias) > 0): ?>
                        <?php echo count($agencias); ?>
                    <?php else: ?>
                        0
                    <?php endif; ?>
                </p>
            </div>
        </div>
    </div>
    <div id="reporteMapa" style="width:100%;height:900px;border:0;"></div>
</div>




<script>
    function initMap() {
        var coordenadaCentral = new google.maps.LatLng(-0.2805707076097901, -78.53181468129077);
        var miMapa = new google.maps.Map(document.getElementById('reporteMapa'), {
            center: coordenadaCentral,
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        <?php if(is_array($agencias) && count($agencias) > 0): ?>
            <?php foreach ($agencias as $agencia): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $agencia->latitud_age; ?>,
                    <?php echo $agencia->longitud_age; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Agencia: <?php echo $agencia->direccion_age; ?>",
                    icon: '<?php echo base_url('assets/img/bank.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
    }
</script>