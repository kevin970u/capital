<div class="container px-6 mx-auto grid">
    <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
        Cajeros Automáticos
    </h2>

    <!-- Cards -->
    <div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4">
        <!-- Card -->
        <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
             <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-full dark:text-green-100 dark:bg-green-500">
                <i class="bi bi-cash-coin" style="font-size:1.5rem;"></i>
            </div>
            <div>
                <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                    Total Cajeros Automáticos
                </p>
                <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                    <?php if(is_array($cajeros) && count($cajeros) > 0): ?>
                        <?php echo count($cajeros); ?>
                    <?php else: ?>
                        0
                    <?php endif; ?>
                </p>
            </div>
        </div>
    </div>
    <div id="reporteMapa" style="width:100%;height:900px;border:0;"></div>
</div>




<script>
    function initMap() {
        var coordenadaCentral = new google.maps.LatLng(-0.2805707076097901, -78.53181468129077);
        var miMapa = new google.maps.Map(document.getElementById('reporteMapa'), {
            center: coordenadaCentral,
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        <?php if(is_array($cajeros) && count($cajeros) > 0): ?>
            <?php foreach ($cajeros as $cajero): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $cajero->latitud_caj; ?>,
                    <?php echo $cajero->longitud_caj; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Cajero Automatico " + " <?php echo $cajero->direccion_caj; ?>",
                    icon: '<?php echo base_url('assets/img/cajero.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
    }
</script>