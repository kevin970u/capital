<div class="container px-6 mx-auto grid">
    <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
        Cajeros
    </h2>
    <!-- CTA -->
    <a class="flex items-center justify-between p-4 mb-8 text-sm font-semibold text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple"
        href="<?php echo site_url('cajeros/mapa'); ?>">
        <div class="flex items-center">
            <i class="bi bi-flag-fill"></i> &nbsp;
            <span>Estamos en todas las provincias del Ecuador para atenderte en tus gestiones
                bancarias.</span>
        </div>
    </a>

    <div class="mb-4">
        <a href="<?php echo site_url('cajeros/nuevo'); ?>"
            class="px-4 py-2 text-sm font-medium leading-5 text-white transition-colors duration-150 bg-purple-600 border border-transparent active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">
            Agregar Cajero <i class="bi bi-plus-circle"></i>
        </a>
    </div>

    <div class="mt-4 w-full overflow-hidden rounded-lg shadow-xs">
        <div class="w-full overflow-x-auto">
            <?php if($cajeros): ?>
                <table class="w-full whitespace-no-wrap" id="tabla">
                    <thead>
                        <tr
                            class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800">
                            <th class="px-4 py-3">Cajero</th>
                            <th class="px-4 py-3">Fecha Apertura</th>
                            <th class="px-4 py-3">Modelo</th>
                            <th class="px-4 py-3">Capacidad</th>
                            <th class="px-4 py-3">Provincia</th>
                            <th class="px-4 py-3">Ciudad</th>
                            <th class="px-4 py-3">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
                        <?php foreach ($cajeros as $cajero): ?>
                            <tr class="text-gray-700 dark:text-gray-400">
                                <td class="px-4 py-3">
                                    <div class="flex items-center text-sm">
                                        <!-- Avatar with inset shadow -->
                                        <div class="relative hidden w-8 h-8 mr-3 rounded-full md:block">
                                            <?php if ($cajero->imagen_caj != ""): ?>
                                                <a href="<?php echo base_url('uploads/cajeros/') . $cajero->imagen_caj; ?>"
                                                    target="_blank">
                                                    <img class="object-cover w-full h-full rounded-full"
                                                        src="<?php echo base_url('uploads/cajeros/') . $cajero->imagen_caj; ?>"
                                                        alt="" loading="lazy" />
                                                    <div class="absolute inset-0 rounded-full shadow-inner" aria-hidden="true">
                                                    </div>
                                                </a>
                                            <?php else: ?>
                                                <img class="object-cover w-full h-full rounded-full"
                                                    src="https://media.istockphoto.com/id/1396814518/es/vector/imagen-pr%C3%B3ximamente-sin-foto-sin-imagen-en-miniatura-disponible-ilustraci%C3%B3n-vectorial.jpg?s=612x612&w=0&k=20&c=aA0kj2K7ir8xAey-SaPc44r5f-MATKGN0X0ybu_A774="
                                                    alt="" loading="lazy" />
                                            <?php endif; ?>
                                        </div>
                                        <div>
                                            <p class="font-semibold">
                                                <?php echo $cajero->tipo_caj ?>
                                            </p>
                                            <p class="text-xs text-gray-600 dark:text-gray-400">
                                                Gerente Agencia:  <?php
                                                    foreach ($agencias as $agencia) {
                                                        if ($agencia->id_age == $cajero->id_age) {
                                                            echo $agencia->gerente_age;
                                                            break;
                                                        }
                                                    }
                                                    ?>
                                            </p>
                                        </div>
                                    </div>
                                </td>
                                <td class="px-4 py-3 text-sm">
                                    <?php echo $cajero->fecha_apertura_caj ?>
                                </td>
                                <td class="px-4 py-3 text-xs">
                                    <span
                                        class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-full dark:bg-green-700 dark:text-green-100">
                                        <?php echo $cajero->modelo_caj ?>
                                    </span>
                                </td>
                                <td class="px-4 py-3 text-sm">
                                    <?php echo $cajero->capacidad_caj ?>
                                </td>
                                <td class="px-4 py-3 text-sm">
                                    <?php echo $cajero->provincia_caj ?>
                                </td>
                                <td class="px-4 py-3 text-sm">
                                    <?php echo $cajero->ciudad_caj ?>
                                </td>

                                <td class="px-4 py-3 text-sm">
                                    <a href="<?php echo site_url('cajeros/editar/') . $cajero->id_caj; ?>">Editar</a>
                                    <a href="javascript:void(0)"
                                        onclick="confirmarEliminar('<?php echo site_url('cajeros/eliminar/') . $cajero->id_caj; ?>', 'Cajero');">Eliminar</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <p>No hay cajeros registrados</p>
            <?php endif; ?>
        </div>
    </div>


</div>