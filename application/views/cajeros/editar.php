<link rel="stylesheet" href="<?php echo base_url('assets/library/boostrap/bootstrap.css'); ?>">
<div class="container px-6 flex justify-center">
    <!-- Modal body -->
    <div class="mt-4 mb-6" style="width:70%;">
        <h2 class="mt-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Editar datos del Cajero
        </h2>
        <!-- Modal description -->
        <p class="text-sm text-gray-500 dark:text-gray-400">
            Por favor, ingrese los datos del cajero a editar.
        </p>

        <form id="form-add-cajero" enctype="multipart/form-data"
            action="<?php echo site_url('cajeros/actualizarCajero') ?>" method="POST"
            class="w-full flex flex-col items-center justify-center px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">
             <input type="hidden" name="id_caj" value="<?php echo $cajeroEditar->id_caj; ?>">
            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Agencia</span>
                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <select required name="id_age" id="id_age" class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input">
                                <option value="">--Seleccione la Agencia--</option>
                                <?php foreach ($agencias as $agencia) : ?>
                                    <option value="<?php echo $agencia->id_age; ?>">
                                        <?php echo $agencia->provincia_age; ?> - <?php echo $agencia->ciudad_age; ?> - <?php echo $agencia->gerente_age; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                    <i class="bi bi-bank w-5 h-5"></i>
                                </div>
                        </div>
                    </label>
                </div>

                <div class=" ml-2 mt-4" style="width:50%">
                    <label class="block text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Tipo Cajero</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="Retiro" name="tipo_caj" id="tipo_caj" value="<?php echo $cajeroEditar->tipo_caj; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-braces-asterisk w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Modelo</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="Diebold Nixdorf" name="modelo_caj" id="modelo_caj" value="<?php echo $cajeroEditar->modelo_caj; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-modem w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>

                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Provincia</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">

                            <select required name="provincia_caj" id="provincia_caj"
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input">
                                <option value="">--Selecciona una provincia--</option>
                                <option value="Azuay">Azuay</option>
                                <option value="Bolívar">Bolívar</option>
                                <option value="Cañar">Cañar</option>
                                <option value="Carchi">Carchi</option>
                                <option value="Chimborazo">Chimborazo</option>
                                <option value="Cotopaxi">Cotopaxi</option>
                                <option value="El Oro">El Oro</option>
                                <option value="Esmeraldas">Esmeraldas</option>
                                <option value="Galápagos">Galápagos</option>
                                <option value="Guayas">Guayas</option>
                                <option value="Imbabura">Imbabura</option>
                                <option value="Loja">Loja</option>
                                <option value="Los Ríos">Los Ríos</option>
                                <option value="Manabí">Manabí</option>
                                <option value="Morona Santiago">Morona Santiago</option>
                                <option value="Napo">Napo</option>
                                <option value="Orellana">Orellana</option>
                                <option value="Pastaza">Pastaza</option>
                                <option value="Pichincha">Pichincha</option>
                                <option value="Santa Elena">Santa Elena</option>
                                <option value="Santo Domingo de los Tsáchilas">Santo Domingo de los Tsáchilas
                                </option>
                                <option value="Sucumbíos">Sucumbíos</option>
                                <option value="Tungurahua">Tungurahua</option>
                                <option value="Zamora Chinchipe">Zamora Chinchipe</option>
                            </select>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-flag w-5 h-5"></i>
                            </div>

                        </div>
                    </label>
                </div>
            </div>

            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Ciudad</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                name="ciudad_caj" id="ciudad_caj" placeholder="Quito" value="<?php echo $cajeroEditar->ciudad_caj; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-buildings w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>

                <div class="ml-2 mt-4" style="width:50%">
                    <label class="block text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Fecha de Apertura</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="0985417485" type="date" name="fecha_apertura_caj"
                                id="fecha_apertura_caj" value="<?php echo $cajeroEditar->fecha_apertura_caj; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-calendar-date-fill w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Capacidad Dinero</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                name="capacidad_caj" id="capacidad_caj" placeholder="20000" type="number" value="<?php echo $cajeroEditar->capacidad_caj; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-buildings w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="flex flex-row  w-full">
                <div class="w-full mr-2 mt-4">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Direccion</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="Av 10 de Diciembre frente al parque" name="direccion_caj"
                                id="direccion_caj" value="<?php echo $cajeroEditar->direccion_caj; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-signpost-split w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="flex flex-row  w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Imagen Actual</span>

                        <?php if (!empty($cajeroEditar->imagen_caj)): ?>

                            <a target="_blank"
                                href="<?php echo base_url('uploads/cajeros/') . $cajeroEditar->imagen_caj; ?>">
                                <img width="300"
                                    src="<?php echo base_url('uploads/cajeros/') . $cajeroEditar->imagen_caj; ?>" alt="">
                            </a>
                        <?php else: ?>
                            <p>No hay imagen</p>
                        <?php endif ?>
                    </label>
                </div>
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black"> Cambiar Imagen Agencia</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input
                                class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                type="file" accept="image/*" name="imagen_caj" id="imagen_caj" />
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-signpost-split w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>


            <div class="flex flex-row w-full">
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black">Latitud</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="readonly block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="0.0000000000" readonly name="latitud_caj" id="latitud_caj" value="<?php echo $cajeroEditar->latitud_caj; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-geo w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
                <div class="mr-2 mt-4" style="width:50%">
                    <label class="text-sm">
                        <span class="text-gray-700 dark:text-gray-400 font-black font-black">Longitud</span>

                        <div
                            class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                            <input required
                                class="readonly block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input"
                                placeholder="0.0000000000" readonly name="longitud_caj" id="longitud_caj" value="<?php echo $cajeroEditar->longitud_caj; ?>"/>
                            <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                                <i class="bi bi-geo w-5 h-5"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <br>
            <br>
            <div id='mapa' style="height: 400px; width: 100%;margin-bottom:2rem;"></div>

            <footer
                class="flex flex-col items-center justify-end px-6 py-3 -mx-6 -mb-4 space-y-4 sm:space-y-0 sm:space-x-6 sm:flex-row bg-gray-50 dark:bg-gray-800">
                <a href="<?php echo site_url('cajeros/index'); ?>"
                    class="w-full px-5 py-3 text-sm font-medium leading-5 text-white text-gray-700 transition-colors duration-150 border border-gray-300  dark:text-gray-400 font-black sm:px-4 sm:py-2 sm:w-auto active:bg-transparent hover:border-gray-500 focus:border-gray-500 active:text-gray-500 focus:outline-none focus:shadow-outline-gray">
                    Cancelar
                </a>
                <button type="submit"
                    class="w-full px-5 py-3 text-sm font-medium leading-5 text-white transition-colors duration-150 bg-purple-600 border border-transparent sm:w-auto sm:px-4 sm:py-2 active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple">
                    Actualizar Cajero
                </button>
            </footer>
        </form>
    </div>
</div>

<script>
    function initMap() {
        const coordCentral = new google.maps.LatLng(<?php echo $cajeroEditar->latitud_caj; ?>, <?php echo $cajeroEditar->longitud_caj; ?>);
        const mapa = document.getElementById('mapa');
        const miMapa = new google.maps.Map(mapa, {
            center: coordCentral,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        })

        const marcador = new google.maps.Marker({
            position: coordCentral,
            map: miMapa,
            title: 'Selecciona la ubicacion del cajero',
            draggable: true,
            icon: '<?php echo base_url('assets/img/cajero.svg') ?>'
        })

        google.maps.event.addListener(
            marcador,
            'dragend',
            function () {
                const latitud = this.getPosition().lat();
                const longitud = this.getPosition().lng();

                document.getElementById('latitud_caj').value = latitud;
                document.getElementById('longitud_caj').value = longitud;
            }
        )
    }
</script>


<script>
    document.getElementById('provincia_caj').value = "<?php echo $cajeroEditar->provincia_caj; ?>";
    document.getElementById('id_age').value = "<?php echo $cajeroEditar->id_age; ?>";

    $(document).ready(function () {
        $("#imagen_caj").fileinput({
            lenguage: "es",
            maxFileSize: 5000,
            showUpload: false,
        });
    });

    const today = new Date().toISOString().split('T')[0];
    document.getElementById('fecha_apertura_caj').setAttribute('max', today);
</script>