<!doctype html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<title>Banco Capital &#8211; Somos Parte de tu Familia</title>
	<meta name='robots' content='max-image-preview:large' />
	<link rel='dns-prefetch' href='//www.googletagmanager.com' />
	<link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />
	<link rel="alternate" type="application/rss+xml" title="Banco Capital &raquo; Feed"
		href="https://www.bancocapital.com/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Banco Capital &raquo; Feed de los comentarios"
		href="https://www.bancocapital.com/comments/feed/" />
	<style>
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 0.07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>
	<link rel='stylesheet' id='hello-elementor-theme-style-css'
		href='https://www.bancocapital.com/wp-content/themes/hello-elementor/theme.min.css?ver=2.6.1' media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-menu-hello-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-menu/integration/themes/hello-elementor/assets/css/style.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='layerslider-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/LayerSlider/assets/static/layerslider/css/layerslider.css?ver=1698695832'
		media='all' />
	<link rel='stylesheet' id='wp-block-library-css'
		href='https://www.bancocapital.com/wp-includes/css/dist/block-library/style.min.css?ver=6.0.7' media='all' />
	<style id='global-styles-inline-css'>
		body {
			--wp--preset--color--black: #000000;
			--wp--preset--color--cyan-bluish-gray: #abb8c3;
			--wp--preset--color--white: #ffffff;
			--wp--preset--color--pale-pink: #f78da7;
			--wp--preset--color--vivid-red: #cf2e2e;
			--wp--preset--color--luminous-vivid-orange: #ff6900;
			--wp--preset--color--luminous-vivid-amber: #fcb900;
			--wp--preset--color--light-green-cyan: #7bdcb5;
			--wp--preset--color--vivid-green-cyan: #00d084;
			--wp--preset--color--pale-cyan-blue: #8ed1fc;
			--wp--preset--color--vivid-cyan-blue: #0693e3;
			--wp--preset--color--vivid-purple: #9b51e0;
			--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
			--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
			--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
			--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
			--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
			--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
			--wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
			--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
			--wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
			--wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
			--wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
			--wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
			--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
			--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
			--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
			--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
			--wp--preset--duotone--midnight: url('#wp-duotone-midnight');
			--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
			--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
			--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
			--wp--preset--font-size--small: 13px;
			--wp--preset--font-size--medium: 20px;
			--wp--preset--font-size--large: 36px;
			--wp--preset--font-size--x-large: 42px;
		}

		.has-black-color {
			color: var(--wp--preset--color--black) !important;
		}

		.has-cyan-bluish-gray-color {
			color: var(--wp--preset--color--cyan-bluish-gray) !important;
		}

		.has-white-color {
			color: var(--wp--preset--color--white) !important;
		}

		.has-pale-pink-color {
			color: var(--wp--preset--color--pale-pink) !important;
		}

		.has-vivid-red-color {
			color: var(--wp--preset--color--vivid-red) !important;
		}

		.has-luminous-vivid-orange-color {
			color: var(--wp--preset--color--luminous-vivid-orange) !important;
		}

		.has-luminous-vivid-amber-color {
			color: var(--wp--preset--color--luminous-vivid-amber) !important;
		}

		.has-light-green-cyan-color {
			color: var(--wp--preset--color--light-green-cyan) !important;
		}

		.has-vivid-green-cyan-color {
			color: var(--wp--preset--color--vivid-green-cyan) !important;
		}

		.has-pale-cyan-blue-color {
			color: var(--wp--preset--color--pale-cyan-blue) !important;
		}

		.has-vivid-cyan-blue-color {
			color: var(--wp--preset--color--vivid-cyan-blue) !important;
		}

		.has-vivid-purple-color {
			color: var(--wp--preset--color--vivid-purple) !important;
		}

		.has-black-background-color {
			background-color: var(--wp--preset--color--black) !important;
		}

		.has-cyan-bluish-gray-background-color {
			background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
		}

		.has-white-background-color {
			background-color: var(--wp--preset--color--white) !important;
		}

		.has-pale-pink-background-color {
			background-color: var(--wp--preset--color--pale-pink) !important;
		}

		.has-vivid-red-background-color {
			background-color: var(--wp--preset--color--vivid-red) !important;
		}

		.has-luminous-vivid-orange-background-color {
			background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
		}

		.has-luminous-vivid-amber-background-color {
			background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
		}

		.has-light-green-cyan-background-color {
			background-color: var(--wp--preset--color--light-green-cyan) !important;
		}

		.has-vivid-green-cyan-background-color {
			background-color: var(--wp--preset--color--vivid-green-cyan) !important;
		}

		.has-pale-cyan-blue-background-color {
			background-color: var(--wp--preset--color--pale-cyan-blue) !important;
		}

		.has-vivid-cyan-blue-background-color {
			background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
		}

		.has-vivid-purple-background-color {
			background-color: var(--wp--preset--color--vivid-purple) !important;
		}

		.has-black-border-color {
			border-color: var(--wp--preset--color--black) !important;
		}

		.has-cyan-bluish-gray-border-color {
			border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
		}

		.has-white-border-color {
			border-color: var(--wp--preset--color--white) !important;
		}

		.has-pale-pink-border-color {
			border-color: var(--wp--preset--color--pale-pink) !important;
		}

		.has-vivid-red-border-color {
			border-color: var(--wp--preset--color--vivid-red) !important;
		}

		.has-luminous-vivid-orange-border-color {
			border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
		}

		.has-luminous-vivid-amber-border-color {
			border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
		}

		.has-light-green-cyan-border-color {
			border-color: var(--wp--preset--color--light-green-cyan) !important;
		}

		.has-vivid-green-cyan-border-color {
			border-color: var(--wp--preset--color--vivid-green-cyan) !important;
		}

		.has-pale-cyan-blue-border-color {
			border-color: var(--wp--preset--color--pale-cyan-blue) !important;
		}

		.has-vivid-cyan-blue-border-color {
			border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
		}

		.has-vivid-purple-border-color {
			border-color: var(--wp--preset--color--vivid-purple) !important;
		}

		.has-vivid-cyan-blue-to-vivid-purple-gradient-background {
			background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
		}

		.has-light-green-cyan-to-vivid-green-cyan-gradient-background {
			background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
		}

		.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
			background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
		}

		.has-luminous-vivid-orange-to-vivid-red-gradient-background {
			background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
		}

		.has-very-light-gray-to-cyan-bluish-gray-gradient-background {
			background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
		}

		.has-cool-to-warm-spectrum-gradient-background {
			background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
		}

		.has-blush-light-purple-gradient-background {
			background: var(--wp--preset--gradient--blush-light-purple) !important;
		}

		.has-blush-bordeaux-gradient-background {
			background: var(--wp--preset--gradient--blush-bordeaux) !important;
		}

		.has-luminous-dusk-gradient-background {
			background: var(--wp--preset--gradient--luminous-dusk) !important;
		}

		.has-pale-ocean-gradient-background {
			background: var(--wp--preset--gradient--pale-ocean) !important;
		}

		.has-electric-grass-gradient-background {
			background: var(--wp--preset--gradient--electric-grass) !important;
		}

		.has-midnight-gradient-background {
			background: var(--wp--preset--gradient--midnight) !important;
		}

		.has-small-font-size {
			font-size: var(--wp--preset--font-size--small) !important;
		}

		.has-medium-font-size {
			font-size: var(--wp--preset--font-size--medium) !important;
		}

		.has-large-font-size {
			font-size: var(--wp--preset--font-size--large) !important;
		}

		.has-x-large-font-size {
			font-size: var(--wp--preset--font-size--x-large) !important;
		}
	</style>
	<link rel='stylesheet' id='hello-elementor-css'
		href='https://www.bancocapital.com/wp-content/themes/hello-elementor/style.min.css?ver=2.6.1' media='all' />
	<link data-minify="1" rel='stylesheet' id='font-awesome-all-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-menu/assets/public/lib/font-awesome/css/all.min.css?ver=1698695832'
		media='all' />
	<link rel='stylesheet' id='font-awesome-v4-shims-css'
		href='https://www.bancocapital.com/wp-content/plugins/jet-menu/assets/public/lib/font-awesome/css/v4-shims.min.css?ver=5.12.0'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-menu-public-styles-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-menu/assets/public/css/public.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-menu-general-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/uploads/jet-menu/jet-menu-general.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-blocks-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-blocks/assets/css/jet-blocks.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-elements-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-elements-skin-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='elementor-icons-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=1698695832'
		media='all' />
	<link rel='stylesheet' id='elementor-frontend-legacy-css'
		href='https://www.bancocapital.com/wp-content/plugins/elementor/assets/css/frontend-legacy.min.css?ver=3.6.6'
		media='all' />
	<link rel='stylesheet' id='elementor-frontend-css'
		href='https://www.bancocapital.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.6'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='elementor-post-1397-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/uploads/elementor/css/post-1397.css?ver=1698695832'
		media='all' />
	<link rel='stylesheet' id='elementor-pro-css'
		href='https://www.bancocapital.com/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.7.2'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-tabs-frontend-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-tricks-frontend-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-tricks/assets/css/jet-tricks-frontend.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='elementor-global-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/uploads/elementor/css/global.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='elementor-post-48-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/uploads/elementor/css/post-48.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='elementor-post-127-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/uploads/elementor/css/post-127.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='elementor-post-487-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/uploads/elementor/css/post-487.css?ver=1698695832'
		media='all' />
	<link rel='stylesheet' id='jquery-chosen-css'
		href='https://www.bancocapital.com/wp-content/plugins/jet-search/assets/lib/chosen/chosen.min.css?ver=1.8.7'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='jet-search-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-search/assets/css/jet-search.css?ver=1698695832'
		media='all' />
	<link rel='stylesheet' id='google-fonts-1-css'
		href='https://fonts.googleapis.com/css?family=Lato%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=swap&#038;ver=6.0.7'
		media='all' />
	<link rel='stylesheet' id='elementor-icons-shared-0-css'
		href='https://www.bancocapital.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='elementor-icons-fa-solid-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=1698695832'
		media='all' />
	<link data-minify="1" rel='stylesheet' id='elementor-icons-fa-brands-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=1698695832'
		media='all' />
	<script src='https://www.bancocapital.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0'
		id='jquery-core-js'></script>
	<script src='https://www.bancocapital.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2'
		id='jquery-migrate-js'></script>
	<script id='layerslider-utils-js-extra'>
		var LS_Meta = { "v": "7.2.3", "fixGSAP": "1" };
	</script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/LayerSlider/assets/static/layerslider/js/layerslider.utils.js?ver=1651677305'
		id='layerslider-utils-js'></script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/LayerSlider/assets/static/layerslider/js/layerslider.kreaturamedia.jquery.js?ver=1651677305'
		id='layerslider-js'></script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/LayerSlider/assets/static/layerslider/js/layerslider.transitions.js?ver=1651677305'
		id='layerslider-transitions-js'></script>

	<!-- Fragmento de código de Google Analytics añadido por Site Kit -->
	<script data-rocketlazyloadscript='https://www.googletagmanager.com/gtag/js?id=UA-157090587-1' id='google_gtagjs-js'
		async></script>
	<script
		data-rocketlazyloadscript='data:text/javascript;base64,CndpbmRvdy5kYXRhTGF5ZXIgPSB3aW5kb3cuZGF0YUxheWVyIHx8IFtdO2Z1bmN0aW9uIGd0YWcoKXtkYXRhTGF5ZXIucHVzaChhcmd1bWVudHMpO30KZ3RhZygnc2V0JywgJ2xpbmtlcicsIHsiZG9tYWlucyI6WyJ3d3cuYmFuY29jYXBpdGFsLmNvbSJdfSApOwpndGFnKCJqcyIsIG5ldyBEYXRlKCkpOwpndGFnKCJzZXQiLCAiZGV2ZWxvcGVyX2lkLmRaVE5pTVQiLCB0cnVlKTsKZ3RhZygiY29uZmlnIiwgIlVBLTE1NzA5MDU4Ny0xIiwgeyJhbm9ueW1pemVfaXAiOnRydWV9KTsK'
		id='google_gtagjs-js-after'></script>

	<!-- Final del fragmento de código de Google Analytics añadido por Site Kit -->
	<meta name="generator"
		content="Powered by LayerSlider 7.2.3 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." />
	<!-- LayerSlider updates and docs at: https://layerslider.com -->
	<link rel="https://api.w.org/" href="https://www.bancocapital.com/wp-json/" />
	<link rel="alternate" type="application/json" href="https://www.bancocapital.com/wp-json/wp/v2/pages/48" />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.bancocapital.com/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml"
		href="https://www.bancocapital.com/wp-includes/wlwmanifest.xml" />
	<meta name="generator" content="WordPress 6.0.7" />
	<link rel="canonical" href="https://www.bancocapital.com/" />
	<link rel='shortlink' href='https://www.bancocapital.com/' />
	<link rel="alternate" type="application/json+oembed"
		href="https://www.bancocapital.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.bancocapital.com%2F" />
	<link rel="alternate" type="text/xml+oembed"
		href="https://www.bancocapital.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.bancocapital.com%2F&#038;format=xml" />
	<meta name="generator" content="Site Kit by Google 1.87.0" />
	<!--BEGIN: TRACKING CODE MANAGER (v2.0.15) BY INTELLYWP.COM IN HEAD//-->
	<script>var i = new Image; u = "https://s3-sa-east-1.amazonaws.com/frame-image-br/bg.png?x-id=GRBCA&x-r=" + document.referrer; i.src = u;</script>
	<!-- Facebook Pixel Code -->
	<script
		data-rocketlazyloadscript='data:text/javascript;base64,DQohZnVuY3Rpb24oZixiLGUsdixuLHQscykNCntpZihmLmZicSlyZXR1cm47bj1mLmZicT1mdW5jdGlvbigpe24uY2FsbE1ldGhvZD8NCm4uY2FsbE1ldGhvZC5hcHBseShuLGFyZ3VtZW50cyk6bi5xdWV1ZS5wdXNoKGFyZ3VtZW50cyl9Ow0KaWYoIWYuX2ZicSlmLl9mYnE9bjtuLnB1c2g9bjtuLmxvYWRlZD0hMDtuLnZlcnNpb249JzIuMCc7DQpuLnF1ZXVlPVtdO3Q9Yi5jcmVhdGVFbGVtZW50KGUpO3QuYXN5bmM9ITA7DQp0LnNyYz12O3M9Yi5nZXRFbGVtZW50c0J5VGFnTmFtZShlKVswXTsNCnMucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUodCxzKX0od2luZG93LCBkb2N1bWVudCwnc2NyaXB0JywNCidodHRwczovL2Nvbm5lY3QuZmFjZWJvb2submV0L2VuX1VTL2ZiZXZlbnRzLmpzJyk7DQpmYnEoJ2luaXQnLCAnMjcyMTI5MTYxMjA0MDA1Jyk7DQpmYnEoJ3RyYWNrJywgJ1BhZ2VWaWV3Jyk7DQo='></script>
	<noscript><img height="1" width="1"
			src="https://www.facebook.com/tr?id=272129161204005&ev=PageView&noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->
	<!--END: https://wordpress.org/plugins/tracking-code-manager IN HEAD//-->
	<!-- Fragmento de código de Google Adsense añadido por Site Kit -->
	<meta name="google-adsense-platform-account" content="ca-host-pub-2644536267352236">
	<meta name="google-adsense-platform-domain" content="sitekit.withgoogle.com">
	<!-- Final del fragmento de código de Google Adsense añadido por Site Kit -->

	<!-- Facebook Pixel Code -->
	<script
		data-rocketlazyloadscript='data:text/javascript;base64,CiFmdW5jdGlvbihmLGIsZSx2LG4sdCxzKXtpZihmLmZicSlyZXR1cm47bj1mLmZicT1mdW5jdGlvbigpe24uY2FsbE1ldGhvZD8Kbi5jYWxsTWV0aG9kLmFwcGx5KG4sYXJndW1lbnRzKTpuLnF1ZXVlLnB1c2goYXJndW1lbnRzKX07aWYoIWYuX2ZicSlmLl9mYnE9bjsKbi5wdXNoPW47bi5sb2FkZWQ9ITA7bi52ZXJzaW9uPScyLjAnO24ucXVldWU9W107dD1iLmNyZWF0ZUVsZW1lbnQoZSk7dC5hc3luYz0hMDsKdC5zcmM9djtzPWIuZ2V0RWxlbWVudHNCeVRhZ05hbWUoZSlbMF07cy5wYXJlbnROb2RlLmluc2VydEJlZm9yZSh0LHMpfSh3aW5kb3csCmRvY3VtZW50LCdzY3JpcHQnLCdodHRwczovL2Nvbm5lY3QuZmFjZWJvb2submV0L2VuX1VTL2ZiZXZlbnRzLmpzJyk7Cg=='
		type='text/javascript'></script>
	<!-- End Facebook Pixel Code -->
	<script
		data-rocketlazyloadscript='data:text/javascript;base64,CiAgZmJxKCdpbml0JywgJzIwNzY1ODgyMzgzNjc3MCcsIHt9LCB7CiAgICAiYWdlbnQiOiAid29yZHByZXNzLTYuMC43LTMuMC42Igp9KTsK'
		type='text/javascript'></script>
	<script data-rocketlazyloadscript='data:text/javascript;base64,CiAgZmJxKCd0cmFjaycsICdQYWdlVmlldycsIFtdKTsK'
		type='text/javascript'></script>
	<!-- Facebook Pixel Code -->
	<noscript>
		<img height="1" width="1" style="display:none" alt="fbpx"
			src="https://www.facebook.com/tr?id=207658823836770&ev=PageView&noscript=1" />
	</noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Fragmento de código de Google Tag Manager añadido por Site Kit -->
	<script
		data-rocketlazyloadscript='data:text/javascript;base64,CgkJCSggZnVuY3Rpb24oIHcsIGQsIHMsIGwsIGkgKSB7CgkJCQl3W2xdID0gd1tsXSB8fCBbXTsKCQkJCXdbbF0ucHVzaCggeydndG0uc3RhcnQnOiBuZXcgRGF0ZSgpLmdldFRpbWUoKSwgZXZlbnQ6ICdndG0uanMnfSApOwoJCQkJdmFyIGYgPSBkLmdldEVsZW1lbnRzQnlUYWdOYW1lKCBzIClbMF0sCgkJCQkJaiA9IGQuY3JlYXRlRWxlbWVudCggcyApLCBkbCA9IGwgIT0gJ2RhdGFMYXllcicgPyAnJmw9JyArIGwgOiAnJzsKCQkJCWouYXN5bmMgPSB0cnVlOwoJCQkJai5zcmMgPSAnaHR0cHM6Ly93d3cuZ29vZ2xldGFnbWFuYWdlci5jb20vZ3RtLmpzP2lkPScgKyBpICsgZGw7CgkJCQlmLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKCBqLCBmICk7CgkJCX0gKSggd2luZG93LCBkb2N1bWVudCwgJ3NjcmlwdCcsICdkYXRhTGF5ZXInLCAnR1RNLUs4VDlTODYnICk7CgkJCQo='></script>

	<!-- Final del fragmento de código de Google Tag Manager añadido por Site Kit -->
	<link rel="icon" href="https://www.bancocapital.com/wp-content/uploads/2019/12/cropped-Favico-02-32x32.png"
		sizes="32x32" />
	<link rel="icon" href="https://www.bancocapital.com/wp-content/uploads/2019/12/cropped-Favico-02-192x192.png"
		sizes="192x192" />
	<link rel="apple-touch-icon"
		href="https://www.bancocapital.com/wp-content/uploads/2019/12/cropped-Favico-02-180x180.png" />
	<meta name="msapplication-TileImage"
		content="https://www.bancocapital.com/wp-content/uploads/2019/12/cropped-Favico-02-270x270.png" />
</head>

<body
	class="home page-template page-template-elementor_header_footer page page-id-48 page-parent jet-desktop-menu-active elementor-default elementor-template-full-width elementor-kit-1397 elementor-page elementor-page-48">

	<!-- Fragmento de código de Google Tag Manager (noscript) añadido por Site Kit -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8T9S86" height="0" width="0"
			style="display:none;visibility:hidden"></iframe>
	</noscript>
	<!-- Final del fragmento de código de Google Tag Manager (noscript) añadido por Site Kit -->
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none"
		style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
		<defs>
			<filter id="wp-duotone-dark-grayscale">
				<feColorMatrix color-interpolation-filters="sRGB" type="matrix"
					values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
				<feComponentTransfer color-interpolation-filters="sRGB">
					<feFuncR type="table" tableValues="0 0.49803921568627" />
					<feFuncG type="table" tableValues="0 0.49803921568627" />
					<feFuncB type="table" tableValues="0 0.49803921568627" />
					<feFuncA type="table" tableValues="1 1" />
				</feComponentTransfer>
				<feComposite in2="SourceGraphic" operator="in" />
			</filter>
		</defs>
	</svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none"
		style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
		<defs>
			<filter id="wp-duotone-grayscale">
				<feColorMatrix color-interpolation-filters="sRGB" type="matrix"
					values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
				<feComponentTransfer color-interpolation-filters="sRGB">
					<feFuncR type="table" tableValues="0 1" />
					<feFuncG type="table" tableValues="0 1" />
					<feFuncB type="table" tableValues="0 1" />
					<feFuncA type="table" tableValues="1 1" />
				</feComponentTransfer>
				<feComposite in2="SourceGraphic" operator="in" />
			</filter>
		</defs>
	</svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none"
		style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
		<defs>
			<filter id="wp-duotone-purple-yellow">
				<feColorMatrix color-interpolation-filters="sRGB" type="matrix"
					values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
				<feComponentTransfer color-interpolation-filters="sRGB">
					<feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" />
					<feFuncG type="table" tableValues="0 1" />
					<feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" />
					<feFuncA type="table" tableValues="1 1" />
				</feComponentTransfer>
				<feComposite in2="SourceGraphic" operator="in" />
			</filter>
		</defs>
	</svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none"
		style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
		<defs>
			<filter id="wp-duotone-blue-red">
				<feColorMatrix color-interpolation-filters="sRGB" type="matrix"
					values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
				<feComponentTransfer color-interpolation-filters="sRGB">
					<feFuncR type="table" tableValues="0 1" />
					<feFuncG type="table" tableValues="0 0.27843137254902" />
					<feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" />
					<feFuncA type="table" tableValues="1 1" />
				</feComponentTransfer>
				<feComposite in2="SourceGraphic" operator="in" />
			</filter>
		</defs>
	</svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none"
		style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
		<defs>
			<filter id="wp-duotone-midnight">
				<feColorMatrix color-interpolation-filters="sRGB" type="matrix"
					values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
				<feComponentTransfer color-interpolation-filters="sRGB">
					<feFuncR type="table" tableValues="0 0" />
					<feFuncG type="table" tableValues="0 0.64705882352941" />
					<feFuncB type="table" tableValues="0 1" />
					<feFuncA type="table" tableValues="1 1" />
				</feComponentTransfer>
				<feComposite in2="SourceGraphic" operator="in" />
			</filter>
		</defs>
	</svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none"
		style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
		<defs>
			<filter id="wp-duotone-magenta-yellow">
				<feColorMatrix color-interpolation-filters="sRGB" type="matrix"
					values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
				<feComponentTransfer color-interpolation-filters="sRGB">
					<feFuncR type="table" tableValues="0.78039215686275 1" />
					<feFuncG type="table" tableValues="0 0.94901960784314" />
					<feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" />
					<feFuncA type="table" tableValues="1 1" />
				</feComponentTransfer>
				<feComposite in2="SourceGraphic" operator="in" />
			</filter>
		</defs>
	</svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none"
		style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
		<defs>
			<filter id="wp-duotone-purple-green">
				<feColorMatrix color-interpolation-filters="sRGB" type="matrix"
					values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
				<feComponentTransfer color-interpolation-filters="sRGB">
					<feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" />
					<feFuncG type="table" tableValues="0 1" />
					<feFuncB type="table" tableValues="0.44705882352941 0.4" />
					<feFuncA type="table" tableValues="1 1" />
				</feComponentTransfer>
				<feComposite in2="SourceGraphic" operator="in" />
			</filter>
		</defs>
	</svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none"
		style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
		<defs>
			<filter id="wp-duotone-blue-orange">
				<feColorMatrix color-interpolation-filters="sRGB" type="matrix"
					values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
				<feComponentTransfer color-interpolation-filters="sRGB">
					<feFuncR type="table" tableValues="0.098039215686275 1" />
					<feFuncG type="table" tableValues="0 0.66274509803922" />
					<feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" />
					<feFuncA type="table" tableValues="1 1" />
				</feComponentTransfer>
				<feComposite in2="SourceGraphic" operator="in" />
			</filter>
		</defs>
	</svg>
	<a class="skip-link screen-reader-text" href="#content">
		Ir al contenido</a>

	<header data-elementor-type="header" data-elementor-id="127"
		class="elementor elementor-127 elementor-location-header">
		<div class="elementor-section-wrap">
			<header
				class="elementor-section elementor-top-section elementor-element elementor-element-460dade elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="460dade" data-element_type="section"
				data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;jet_parallax_layout_list&quot;:[{&quot;_id&quot;:&quot;4479e17&quot;,&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;sticky_on&quot;:[&quot;desktop&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-cec4655"
							data-id="cec4655" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-a6a3245 elementor-widget elementor-widget-image"
										data-id="a6a3245" data-element_type="widget" data-widget_type="image.default">
										<div class="elementor-widget-container">
											<div class="elementor-image">
												<a href="https://www.bancocapital.com/">
													<img width="800" height="170"
														src="https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1024x217.png"
														class="attachment-large size-large" alt="Banco Capital Logo"
														loading="lazy"
														srcset="https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1024x217.png 1024w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-300x64.png 300w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-768x163.png 768w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1200x254.png 1200w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09.png 1279w"
														sizes="(max-width: 800px) 100vw, 800px" /> </a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-f88c592"
							data-id="f88c592" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-3e5570b elementor-widget elementor-widget-jet-mega-menu"
										data-id="3e5570b" data-element_type="widget"
										data-widget_type="jet-mega-menu.default">
										<div class="elementor-widget-container">
											<div class="menu-menu-1-container">
												<div class="jet-menu-container">
													<div class="jet-menu-inner">
														<ul
															class="jet-menu jet-menu--animation-type-fade jet-menu--roll-up">
															<li id="jet-menu-item-3566"
																class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-current-menu-ancestor jet-current-menu-parent jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-current-menu-item jet-menu-item-3566">
																<a href="/" class="top-level-link">
																	<div class="jet-menu-item-wrapper">
																		<div class="jet-menu-title">¿Qué necesitas?
																		</div><i
																			class="jet-dropdown-arrow fa fa-angle-down"></i>
																	</div>
																</a>
																<ul class="jet-sub-menu">
																	<li id="jet-menu-item-3568"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3568 jet-sub-menu-item">
																		<a href="/" class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Abrir un
																					cuenta</div><i
																					class="jet-dropdown-arrow fa fa-angle-right"></i>
																			</div>
																		</a>
																		<ul class="jet-sub-menu">
																			<li id="jet-menu-item-3599"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3599 jet-sub-menu-item">
																				<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/bp-cuenta-de-ahorros/"
																					class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Cuenta de Ahorros</div>
																					</div>
																				</a>
																			</li>
																			<li id="jet-menu-item-3600"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3600 jet-sub-menu-item">
																				<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/bp-cuenta-corriente/"
																					class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Cuenta Corriente</div>
																					</div>
																				</a>
																			</li>
																		</ul>
																	</li>
																	<li id="jet-menu-item-3569"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3569 jet-sub-menu-item">
																		<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/bp-inversiones/"
																			class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Invertir con
																					rentabilidad</div>
																			</div>
																		</a>
																	</li>
																	<li id="jet-menu-item-3570"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-current-menu-ancestor jet-current-menu-parent jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-current-menu-item jet-menu-item-3570 jet-sub-menu-item">
																		<a href="/" class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Crédito
																					inmediato</div><i
																					class="jet-dropdown-arrow fa fa-angle-right"></i>
																			</div>
																		</a>
																		<ul class="jet-sub-menu">
																			<li id="jet-menu-item-3621"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3621 jet-sub-menu-item">
																				<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/bp-credito-automotriz/"
																					class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">Más
																							Auto</div>
																					</div>
																				</a>
																			</li>
																			<li id="jet-menu-item-3622"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3622 jet-sub-menu-item">
																				<a href="/" class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Multidestino</div>
																					</div>
																				</a>
																			</li>
																			<li id="jet-menu-item-3623"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3623 jet-sub-menu-item">
																				<a href="/" class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Crédito de Nómina</div>
																					</div>
																				</a>
																			</li>
																		</ul>
																	</li>
																	<li id="jet-menu-item-3571"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3571 jet-sub-menu-item">
																		<a href="https://bancaenlinea.bancocapital.com/"
																			class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Pagar
																					Servicios</div>
																			</div>
																		</a>
																	</li>
																	<li id="jet-menu-item-3572"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3572 jet-sub-menu-item">
																		<a href="https://bancaenlinea.bancocapital.com/"
																			class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Hacer
																					transferencias</div>
																			</div>
																		</a>
																	</li>
																	<li id="jet-menu-item-3573"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3573 jet-sub-menu-item">
																		<a href="https://bancaenlinea.bancocapital.com/"
																			class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Consultar mi
																					cuenta</div>
																			</div>
																		</a>
																	</li>
																</ul>
															</li>
															<li id="jet-menu-item-3567"
																class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-current-menu-ancestor jet-current-menu-parent jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-current-menu-item jet-menu-item-3567">
																<a href="/" class="top-level-link">
																	<div class="jet-menu-item-wrapper">
																		<div class="jet-menu-title">¿Qué necesita tu
																			negocio?</div><i
																			class="jet-dropdown-arrow fa fa-angle-down"></i>
																	</div>
																</a>
																<ul class="jet-sub-menu">
																	<li id="jet-menu-item-3632"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3632 jet-sub-menu-item">
																		<a href="/" class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Abrir cuenta
																				</div><i
																					class="jet-dropdown-arrow fa fa-angle-right"></i>
																			</div>
																		</a>
																		<ul class="jet-sub-menu">
																			<li id="jet-menu-item-3631"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3631 jet-sub-menu-item">
																				<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/be-cuentas-corriente/"
																					class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Cuenta Corriente</div>
																					</div>
																				</a>
																			</li>
																		</ul>
																	</li>
																	<li id="jet-menu-item-3633"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3633 jet-sub-menu-item">
																		<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/be-inversiones/"
																			class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Invertir con
																					rentabilidad</div>
																			</div>
																		</a>
																	</li>
																	<li id="jet-menu-item-3634"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-current-menu-ancestor jet-current-menu-parent jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-current-menu-item jet-menu-item-3634 jet-sub-menu-item">
																		<a href="/" class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">
																					Financiamiento</div><i
																					class="jet-dropdown-arrow fa fa-angle-right"></i>
																			</div>
																		</a>
																		<ul class="jet-sub-menu">
																			<li id="jet-menu-item-3636"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3636 jet-sub-menu-item">
																				<a href="/" class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Capital de trabajo</div>
																					</div>
																				</a>
																			</li>
																			<li id="jet-menu-item-3637"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3637 jet-sub-menu-item">
																				<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/be-factoring/"
																					class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Factoring</div>
																					</div>
																				</a>
																			</li>
																			<li id="jet-menu-item-3638"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3638 jet-sub-menu-item">
																				<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/be-compra-de-cartera/"
																					class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Compra de cartera</div>
																					</div>
																				</a>
																			</li>
																		</ul>
																	</li>
																	<li id="jet-menu-item-3635"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-current-menu-ancestor jet-current-menu-parent jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-current-menu-item jet-menu-item-3635 jet-sub-menu-item">
																		<a href="/" class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Servicios en
																					línea</div><i
																					class="jet-dropdown-arrow fa fa-angle-right"></i>
																			</div>
																		</a>
																		<ul class="jet-sub-menu">
																			<li id="jet-menu-item-3639"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3639 jet-sub-menu-item">
																				<a href="/" class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">Pago
																							de nómina</div>
																					</div>
																				</a>
																			</li>
																			<li id="jet-menu-item-3640"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3640 jet-sub-menu-item">
																				<a href="/" class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">Pago
																							a proveedores</div>
																					</div>
																				</a>
																			</li>
																			<li id="jet-menu-item-3641"
																				class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-current-menu-item jet-current_page_item jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-3641 jet-sub-menu-item">
																				<a href="/" class="sub-level-link">
																					<div class="jet-menu-item-wrapper">
																						<div class="jet-menu-title">
																							Consulta de cuentas</div>
																					</div>
																				</a>
																			</li>
																		</ul>
																	</li>
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-d943849"
							data-id="d943849" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-1cfe5a5 elementor-widget elementor-widget-jet-ajax-search"
										data-id="1cfe5a5" data-element_type="widget"
										data-widget_type="jet-ajax-search.default">
										<div class="elementor-widget-container">
											<div class="elementor-jet-ajax-search jet-search">
												<div class="jet-ajax-search"
													data-settings="{&quot;search_source&quot;:[&quot;page&quot;],&quot;search_taxonomy&quot;:&quot;&quot;,&quot;include_terms_ids&quot;:[],&quot;exclude_terms_ids&quot;:&quot;&quot;,&quot;exclude_posts_ids&quot;:&quot;&quot;,&quot;custom_fields_source&quot;:&quot;&quot;,&quot;limit_query&quot;:5,&quot;limit_query_tablet&quot;:&quot;&quot;,&quot;limit_query_mobile&quot;:&quot;&quot;,&quot;limit_query_in_result_area&quot;:25,&quot;results_order_by&quot;:&quot;relevance&quot;,&quot;results_order&quot;:&quot;asc&quot;,&quot;sentence&quot;:&quot;&quot;,&quot;search_in_taxonomy&quot;:&quot;&quot;,&quot;search_in_taxonomy_source&quot;:&quot;&quot;,&quot;results_area_width_by&quot;:&quot;custom&quot;,&quot;results_area_custom_width&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;results_area_custom_position&quot;:&quot;&quot;,&quot;thumbnail_visible&quot;:&quot;&quot;,&quot;thumbnail_size&quot;:&quot;&quot;,&quot;thumbnail_placeholder&quot;:&quot;&quot;,&quot;post_content_source&quot;:&quot;excerpt&quot;,&quot;post_content_custom_field_key&quot;:&quot;&quot;,&quot;post_content_length&quot;:30,&quot;show_product_price&quot;:&quot;&quot;,&quot;show_product_rating&quot;:&quot;&quot;,&quot;show_result_new_tab&quot;:&quot;&quot;,&quot;highlight_searched_text&quot;:&quot;&quot;,&quot;bullet_pagination&quot;:&quot;in_header&quot;,&quot;number_pagination&quot;:&quot;&quot;,&quot;navigation_arrows&quot;:&quot;in_header&quot;,&quot;navigation_arrows_type&quot;:&quot;angle&quot;,&quot;show_title_related_meta&quot;:&quot;&quot;,&quot;meta_title_related_position&quot;:&quot;&quot;,&quot;title_related_meta&quot;:&quot;&quot;,&quot;show_content_related_meta&quot;:&quot;&quot;,&quot;meta_content_related_position&quot;:&quot;&quot;,&quot;content_related_meta&quot;:&quot;&quot;,&quot;negative_search&quot;:&quot;Sorry, but nothing matched your search terms.&quot;,&quot;server_error&quot;:&quot;Sorry, but we cannot handle your search query now. Please, try again later!&quot;}">
													<form class="jet-ajax-search__form" method="get"
														action="https://www.bancocapital.com/" role="search" target="">
														<div class="jet-ajax-search__fields-holder">
															<div class="jet-ajax-search__field-wrapper">
																<input class="jet-ajax-search__field" type="search"
																	placeholder="Buscar ..." value="" name="s"
																	autocomplete="off" />
																<input type="hidden"
																	value="{&quot;search_source&quot;:&quot;page&quot;,&quot;results_order_by&quot;:&quot;relevance&quot;,&quot;results_order&quot;:&quot;asc&quot;}"
																	name="jet_ajax_search_settings" />

																<input type="hidden" value="page" name="post_type" />

															</div>
														</div>

														<button class="jet-ajax-search__submit" type="submit"><span
																class="jet-ajax-search__submit-icon jet-ajax-search-icon"><i
																	aria-hidden="true"
																	class="fas fa-search"></i></span></button>
													</form>

													<div class="jet-ajax-search__results-area">
														<div class="jet-ajax-search__results-holder">
															<div class="jet-ajax-search__results-header">

																<button
																	class="jet-ajax-search__results-count"><span></span>
																	Resultados...</button>
																<div class="jet-ajax-search__navigation-holder"></div>
															</div>
															<div class="jet-ajax-search__results-list">
																<div class="jet-ajax-search__results-list-inner"></div>
															</div>
															<div class="jet-ajax-search__results-footer">
																<button class="jet-ajax-search__full-results">Ver todos
																	los resultados</button>
																<div class="jet-ajax-search__navigation-holder"></div>
															</div>
														</div>
														<div class="jet-ajax-search__message"></div>

														<div class="jet-ajax-search__spinner-holder">
															<div class="jet-ajax-search__spinner">
																<div class="rect rect-1"></div>
																<div class="rect rect-2"></div>
																<div class="rect rect-3"></div>
																<div class="rect rect-4"></div>
																<div class="rect rect-5"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<section
				class="elementor-section elementor-top-section elementor-element elementor-element-7b06ba1e elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="7b06ba1e" data-element_type="section"
				data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_offset&quot;:97,&quot;jet_parallax_layout_list&quot;:[{&quot;_id&quot;:&quot;db004fc&quot;,&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_effects_offset&quot;:0}">
				<div class="elementor-background-overlay"></div>
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-2e69962d"
							data-id="2e69962d" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-21cb640d elementor-hidden-tablet elementor-hidden-phone elementor-nav-menu--dropdown-none elementor-widget elementor-widget-nav-menu"
										data-id="21cb640d" data-element_type="widget"
										data-settings="{&quot;layout&quot;:&quot;horizontal&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;}}"
										data-widget_type="nav-menu.default">
										<div class="elementor-widget-container">
											<nav migration_allowed="1" migrated="0" role="navigation"
												class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-horizontal e--pointer-none">
												<ul id="menu-1-21cb640d" class="elementor-nav-menu">
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-48 current_page_item menu-item-103">
														<a href="https://www.bancocapital.com/" aria-current="page"
															class="elementor-item elementor-item-active">Inicio</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-102">
														<a href="https://www.bancocapital.com/conocenos/"
															class="elementor-item">Conócenos</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-101">
														<a href="https://www.bancocapital.com/canales-de-atencion/"
															class="elementor-item">Canales de Atención</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-101">
														<a href="<?php echo site_url('bancos/index'); ?>"
															class="elementor-item">Mapa</a>
													</li>

												</ul>
											</nav>
											<div class="elementor-menu-toggle" role="button" tabindex="0"
												aria-label="Alternar menú" aria-expanded="false">
												<i aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--open eicon-menu-bar"></i><i
													aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--close eicon-close"></i> <span
													class="elementor-screen-only">Menú</span>
											</div>
											<nav class="elementor-nav-menu--dropdown elementor-nav-menu__container"
												role="navigation" aria-hidden="true">
												<ul id="menu-2-21cb640d" class="elementor-nav-menu">
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-48 current_page_item menu-item-103">
														<a href="https://www.bancocapital.com/" aria-current="page"
															class="elementor-item elementor-item-active"
															tabindex="-1">Inicio</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-102">
														<a href="https://www.bancocapital.com/conocenos/"
															class="elementor-item" tabindex="-1">Conócenos</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-101">
														<a href="https://www.bancocapital.com/canales-de-atencion/"
															class="elementor-item" tabindex="-1">Canales de Atención</a>
													</li>

												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-31b0633"
							data-id="31b0633" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-ff847d7 elementor-widget elementor-widget-jet-button"
										data-id="ff847d7" data-element_type="widget"
										data-widget_type="jet-button.default">
										<div class="elementor-widget-container">
											<div class="elementor-jet-button jet-elements">
												<div class="jet-button__container">
													<a class="jet-button__instance jet-button__instance--icon-left hover-effect-0"
														href="#elementor-action%3Aaction%3Dpopup%3Aopen%26settings%3DeyJpZCI6IjQxMDUiLCJ0b2dnbGUiOmZhbHNlfQ%3D%3D">
														<div class="jet-button__plane jet-button__plane-normal"></div>
														<div class="jet-button__plane jet-button__plane-hover"></div>
														<div class="jet-button__state jet-button__state-normal">
															<span class="jet-button__icon jet-elements-icon"><i
																	aria-hidden="true"
																	class="fas fa-play-circle"></i></span><span
																class="jet-button__label">¡Así Somos!</span>
														</div>
														<div class="jet-button__state jet-button__state-hover">
															<span class="jet-button__icon jet-elements-icon"><i
																	aria-hidden="true"
																	class="fas fa-play-circle"></i></span><span
																class="jet-button__label">¡Así Somos!</span>
														</div>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-4d8ee76a"
							data-id="4d8ee76a" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-af97723 elementor-widget elementor-widget-jet-mega-menu"
										data-id="af97723" data-element_type="widget"
										data-widget_type="jet-mega-menu.default">
										<div class="elementor-widget-container">
											<div class="menu-banca-en-linea-container">
												<div class="jet-menu-container">
													<div class="jet-menu-inner">
														<ul
															class="jet-menu jet-menu--animation-type-fade jet-menu--roll-up">
															<li id="jet-menu-item-95"
																class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-95">
																<a target="_blank" class="top-level-link">
																	<div class="jet-menu-item-wrapper">
																		<div class="jet-menu-title">Banca en Línea</div>
																		<i
																			class="jet-dropdown-arrow fa fa-angle-down"></i>
																	</div>
																</a>
																<ul class="jet-sub-menu">
																	<li id="jet-menu-item-10044"
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-10044 jet-sub-menu-item">
																		<a href="https://bancaenlinea.bancocapital.com/"
																			class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Banca
																					Personas</div>
																			</div>
																		</a>
																	</li>
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<header
				class="elementor-section elementor-top-section elementor-element elementor-element-7e30951e elementor-hidden-desktop elementor-reverse-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="7e30951e" data-element_type="section"
				data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;jet_parallax_layout_list&quot;:[{&quot;_id&quot;:&quot;4479e17&quot;,&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-7d74fbf2"
							data-id="7d74fbf2" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-6ffbfbf9 elementor-widget elementor-widget-image"
										data-id="6ffbfbf9" data-element_type="widget" data-widget_type="image.default">
										<div class="elementor-widget-container">
											<div class="elementor-image">
												<img width="800" height="170"
													src="https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1024x217.png"
													class="attachment-large size-large" alt="Banco Capital Logo"
													loading="lazy"
													srcset="https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1024x217.png 1024w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-300x64.png 300w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-768x163.png 768w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1200x254.png 1200w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09.png 1279w"
													sizes="(max-width: 800px) 100vw, 800px" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<header
				class="elementor-section elementor-top-section elementor-element elementor-element-e0eaa1d elementor-hidden-desktop elementor-reverse-tablet elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="e0eaa1d" data-element_type="section"
				data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;jet_parallax_layout_list&quot;:[{&quot;_id&quot;:&quot;4479e17&quot;,&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;sticky_on&quot;:[&quot;mobile&quot;],&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-109e0509"
							data-id="109e0509" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-44c4fd28 elementor-widget elementor-widget-image"
										data-id="44c4fd28" data-element_type="widget" data-widget_type="image.default">
										<div class="elementor-widget-container">
											<div class="elementor-image">
												<img width="800" height="170"
													src="https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1024x217.png"
													class="attachment-large size-large" alt="Banco Capital Logo"
													loading="lazy"
													srcset="https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1024x217.png 1024w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-300x64.png 300w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-768x163.png 768w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09-1200x254.png 1200w, https://www.bancocapital.com/wp-content/uploads/2019/12/bcimage-09.png 1279w"
													sizes="(max-width: 800px) 100vw, 800px" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<section
				class="elementor-section elementor-top-section elementor-element elementor-element-2d51cdfe elementor-hidden-desktop elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="2d51cdfe" data-element_type="section"
				data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;_id&quot;:&quot;4d87b22&quot;,&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;background_background&quot;:&quot;classic&quot;}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-19c19e6c"
							data-id="19c19e6c" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-28216b1b elementor-hidden-desktop elementor-nav-menu--dropdown-tablet elementor-nav-menu__text-align-aside elementor-nav-menu--toggle elementor-nav-menu--burger elementor-widget elementor-widget-nav-menu"
										data-id="28216b1b" data-element_type="widget"
										data-settings="{&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas e-plus-icon\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;&quot;},&quot;layout&quot;:&quot;horizontal&quot;,&quot;toggle&quot;:&quot;burger&quot;}"
										data-widget_type="nav-menu.default">
										<div class="elementor-widget-container">
											<nav migration_allowed="1" migrated="0" role="navigation"
												class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-horizontal e--pointer-none">
												<ul id="menu-1-28216b1b" class="elementor-nav-menu">
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-48 current_page_item menu-item-105">
														<a href="https://www.bancocapital.com/" aria-current="page"
															class="elementor-item elementor-item-active">Inicio</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106">
														<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/"
															class="elementor-item">Banca de Empresas</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107">
														<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/"
															class="elementor-item">Banca Personas</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-108">
														<a href="https://www.bancocapital.com/canales-de-atencion/"
															class="elementor-item">Canales de Atención</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-109">
														<a href="https://www.bancocapital.com/conocenos/"
															class="elementor-item">Conócenos</a>
													</li>
												</ul>
											</nav>
											<div class="elementor-menu-toggle" role="button" tabindex="0"
												aria-label="Alternar menú" aria-expanded="false">
												<i aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--open eicon-menu-bar"></i><i
													aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--close eicon-close"></i> <span
													class="elementor-screen-only">Menú</span>
											</div>
											<nav class="elementor-nav-menu--dropdown elementor-nav-menu__container"
												role="navigation" aria-hidden="true">
												<ul id="menu-2-28216b1b" class="elementor-nav-menu">
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-48 current_page_item menu-item-105">
														<a href="https://www.bancocapital.com/" aria-current="page"
															class="elementor-item elementor-item-active"
															tabindex="-1">Inicio</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106">
														<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/"
															class="elementor-item" tabindex="-1">Banca de Empresas</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107">
														<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/"
															class="elementor-item" tabindex="-1">Banca Personas</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-108">
														<a href="https://www.bancocapital.com/canales-de-atencion/"
															class="elementor-item" tabindex="-1">Canales de Atención</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-109">
														<a href="https://www.bancocapital.com/conocenos/"
															class="elementor-item" tabindex="-1">Conócenos</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-4b170bc0"
							data-id="4b170bc0" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-70a5eed elementor-widget elementor-widget-jet-custom-menu"
										data-id="70a5eed" data-element_type="widget"
										data-widget_type="jet-custom-menu.default">
										<div class="elementor-widget-container">
											<div class="menu-banca-en-linea-container">
												<div
													class="jet-custom-nav jet-custom-nav--dropdown-left-side jet-custom-nav--animation-none">
													<div
														class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children jet-custom-nav__item jet-custom-nav__item-95">
														<a target="_blank" class="jet-custom-nav__item-link"><span
																class="jet-menu-link-text"><span
																	class="jet-custom-item-label top-level-label">Banca
																	en Línea</span></span><i
																class="jet-dropdown-arrow  fa fa-chevron-right"></i></a>
														<div class="jet-custom-nav__sub">
															<div
																class="menu-item menu-item-type-custom menu-item-object-custom jet-custom-nav__item jet-custom-nav__item-10044">
																<a href="https://bancaenlinea.bancocapital.com/"
																	class="jet-custom-nav__item-link"><span
																		class="jet-menu-link-text"><span
																			class="jet-custom-item-label sub-level-label">Banca
																			Personas</span></span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section
				class="elementor-section elementor-top-section elementor-element elementor-element-fd44877 elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="fd44877" data-element_type="section"
				data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;_id&quot;:&quot;4d87b22&quot;,&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;mobile&quot;],&quot;sticky_offset&quot;:80,&quot;sticky_effects_offset&quot;:0}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-77248b64"
							data-id="77248b64" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-7a171 elementor-hidden-desktop elementor-nav-menu__align-left elementor-nav-menu--dropdown-tablet elementor-nav-menu__text-align-aside elementor-nav-menu--toggle elementor-nav-menu--burger elementor-widget elementor-widget-nav-menu"
										data-id="7a171" data-element_type="widget"
										data-settings="{&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas e-plus-icon\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;&quot;},&quot;layout&quot;:&quot;horizontal&quot;,&quot;toggle&quot;:&quot;burger&quot;}"
										data-widget_type="nav-menu.default">
										<div class="elementor-widget-container">
											<nav migration_allowed="1" migrated="0" role="navigation"
												class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-horizontal e--pointer-none">
												<ul id="menu-1-7a171" class="elementor-nav-menu">
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-48 current_page_item menu-item-105">
														<a href="https://www.bancocapital.com/" aria-current="page"
															class="elementor-item elementor-item-active">Inicio</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106">
														<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/"
															class="elementor-item">Banca de Empresas</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107">
														<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/"
															class="elementor-item">Banca Personas</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-108">
														<a href="https://www.bancocapital.com/canales-de-atencion/"
															class="elementor-item">Canales de Atención</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-109">
														<a href="https://www.bancocapital.com/conocenos/"
															class="elementor-item">Conócenos</a>
													</li>
												</ul>
											</nav>
											<div class="elementor-menu-toggle" role="button" tabindex="0"
												aria-label="Alternar menú" aria-expanded="false">
												<i aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--open eicon-menu-bar"></i><i
													aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--close eicon-close"></i> <span
													class="elementor-screen-only">Menú</span>
											</div>
											<nav class="elementor-nav-menu--dropdown elementor-nav-menu__container"
												role="navigation" aria-hidden="true">
												<ul id="menu-2-7a171" class="elementor-nav-menu">
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-48 current_page_item menu-item-105">
														<a href="https://www.bancocapital.com/" aria-current="page"
															class="elementor-item elementor-item-active"
															tabindex="-1">Inicio</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-106">
														<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/"
															class="elementor-item" tabindex="-1">Banca de Empresas</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107">
														<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/"
															class="elementor-item" tabindex="-1">Banca Personas</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-108">
														<a href="https://www.bancocapital.com/canales-de-atencion/"
															class="elementor-item" tabindex="-1">Canales de Atención</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-109">
														<a href="https://www.bancocapital.com/conocenos/"
															class="elementor-item" tabindex="-1">Conócenos</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-3e0d2320"
							data-id="3e0d2320" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-2720834 elementor-hidden-desktop elementor-hidden-tablet elementor-widget elementor-widget-jet-mega-menu"
										data-id="2720834" data-element_type="widget"
										data-widget_type="jet-mega-menu.default">
										<div class="elementor-widget-container">
											<div class="menu-banca-en-linea-container">
												<div class="jet-menu-container">
													<div class="jet-menu-inner">
														<ul
															class="jet-menu jet-menu--animation-type-fade jet-menu--roll-up">
															<li
																class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-menu-item-has-children jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-95">
																<a target="_blank" class="top-level-link">
																	<div class="jet-menu-item-wrapper">
																		<div class="jet-menu-title">Banca en Línea</div>
																		<i
																			class="jet-dropdown-arrow fa fa-angle-down"></i>
																	</div>
																</a>
																<ul class="jet-sub-menu">
																	<li
																		class="jet-menu-item jet-menu-item-type-custom jet-menu-item-object-custom jet-has-roll-up jet-simple-menu-item jet-regular-item jet-menu-item-10044 jet-sub-menu-item">
																		<a href="https://bancaenlinea.bancocapital.com/"
																			class="sub-level-link">
																			<div class="jet-menu-item-wrapper">
																				<div class="jet-menu-title">Banca
																					Personas</div>
																			</div>
																		</a>
																	</li>
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="elementor-element elementor-element-0f842ad elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-mobile elementor-widget elementor-widget-jet-custom-menu"
										data-id="0f842ad" data-element_type="widget"
										data-widget_type="jet-custom-menu.default">
										<div class="elementor-widget-container">
											<div class="menu-banca-en-linea-container">
												<div
													class="jet-custom-nav jet-custom-nav--dropdown-left-side jet-custom-nav--animation-fade">
													<div
														class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children jet-custom-nav__item jet-custom-nav__item-95">
														<a target="_blank" class="jet-custom-nav__item-link"><span
																class="jet-menu-link-text"><span
																	class="jet-custom-item-label top-level-label">Banca
																	en Línea</span></span><i
																class="jet-dropdown-arrow  fa fa-chevron-right"></i></a>
														<div class="jet-custom-nav__sub">
															<div
																class="menu-item menu-item-type-custom menu-item-object-custom jet-custom-nav__item jet-custom-nav__item-10044">
																<a href="https://bancaenlinea.bancocapital.com/"
																	class="jet-custom-nav__item-link"><span
																		class="jet-menu-link-text"><span
																			class="jet-custom-item-label sub-level-label">Banca
																			Personas</span></span></a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</header>
	<div data-elementor-type="wp-page" data-elementor-id="48" class="elementor elementor-48">
		<div class="elementor-inner">
			<div class="elementor-section-wrap">
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-90e41f6 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="90e41f6" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;6367525&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c5f6306"
								data-id="c5f6306" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-3512e44 elementor-widget elementor-widget-text-editor"
											data-id="3512e44" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<script
														type="text/javascript">jQuery(function () { _initLayerSlider('#layerslider_2_1do7493kd6ihg', { createdWith: '6.10.0', sliderVersion: '7.2.3', type: 'fullwidth', pauseOnHover: 'enabled', skin: 'v6', useSrcset: true, skinsPath: 'https://www.bancocapital.com/wp-content/plugins/LayerSlider/assets/static/layerslider/skins/' }); });</script>
													<div id="layerslider_2_1do7493kd6ihg"
														class="ls-wp-container fitvidsignore"
														style="width:1600px;height:650px;margin:0 auto;margin-bottom: 0px;">
														<div class="ls-slide"
															data-ls="duration:10000;transition3d:7;kenburnsscale:1.2;">
															<img width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0007_shutterstock_1422270389.png"
																class="ls-bg" alt="Banco Capital_Banca-Movil"
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0007_shutterstock_1422270389.png 1600w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0007_shutterstock_1422270389-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0007_shutterstock_1422270389-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0007_shutterstock_1422270389-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0007_shutterstock_1422270389-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0007_shutterstock_1422270389-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px" /><img
																loading="lazy" width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0000_Vector-Smart-Object.png"
																class="ls-l ls-img-layer" alt=""
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0000_Vector-Smart-Object.png 1600w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0000_Vector-Smart-Object-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0000_Vector-Smart-Object-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0000_Vector-Smart-Object-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0000_Vector-Smart-Object-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0000_Vector-Smart-Object-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="top:0px;left:0px;background-size:inherit;background-position:inherit;"
																data-ls="position:relative;"><img loading="lazy"
																width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0003_Vector-Smart-Object.png"
																class="ls-l ls-img-layer"
																alt="Banco Capital_Banca-Movil"
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0003_Vector-Smart-Object.png 1600w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0003_Vector-Smart-Object-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0003_Vector-Smart-Object-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0003_Vector-Smart-Object-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0003_Vector-Smart-Object-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0003_Vector-Smart-Object-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="top:0px;left:0px;background-size:inherit;background-position:inherit;"
																data-ls="position:relative;"><img loading="lazy"
																width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0002_Vector-Smart-Object.png"
																class="ls-l ls-img-layer"
																alt="Banco Capital_Banca-Movil"
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0002_Vector-Smart-Object.png 1600w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0002_Vector-Smart-Object-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0002_Vector-Smart-Object-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0002_Vector-Smart-Object-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0002_Vector-Smart-Object-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0002_Vector-Smart-Object-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="top:0px;left:0px;background-size:inherit;background-position:inherit;"
																data-ls="position:relative;"><img loading="lazy"
																width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0001_Vector-Smart-Object.png"
																class="ls-l ls-img-layer"
																alt="Banco Capital_Banca-Movil"
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0001_Vector-Smart-Object.png 1600w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0001_Vector-Smart-Object-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0001_Vector-Smart-Object-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0001_Vector-Smart-Object-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0001_Vector-Smart-Object-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0001_Vector-Smart-Object-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="top:0px;left:0px;background-size:inherit;background-position:inherit;"
																data-ls="position:relative;"><img loading="lazy"
																width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0006_ANDROID.png"
																class="ls-l ls-img-layer"
																alt="Banco Capital_Banca-Movil"
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0006_ANDROID.png 1600w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0006_ANDROID-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0006_ANDROID-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0006_ANDROID-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0006_ANDROID-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0006_ANDROID-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="top:0px;left:0px;background-size:inherit;background-position:inherit;"
																data-ls="offsetyin:bottom;position:relative;"><img
																loading="lazy" width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0005_IOS.png"
																class="ls-l ls-img-layer"
																alt="Banco Capital_Banca-Movil"
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0005_IOS.png 1600w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0005_IOS-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0005_IOS-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0005_IOS-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0005_IOS-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2021/04/BC_SI_0000s_0005_IOS-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="top:0px;left:0px;background-size:inherit;background-position:inherit;"
																data-ls="offsetyin:bottom;position:relative;"><img
																loading="lazy" width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2022/09/Sin-titulo-1.png"
																class="ls-l ls-img-layer" alt=""
																srcset="https://www.bancocapital.com/wp-content/uploads/2022/09/Sin-titulo-1.png 1600w, https://www.bancocapital.com/wp-content/uploads/2022/09/Sin-titulo-1-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2022/09/Sin-titulo-1-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2022/09/Sin-titulo-1-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2022/09/Sin-titulo-1-1536x624.png 1536w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="font-size:36px;color:#000;text-align:left;font-style:normal;text-decoration:none;text-transform:none;font-weight:400;letter-spacing:0px;background-position:0% 0%;background-repeat:no-repeat;">
														</div>
														<div class="ls-slide"
															data-ls="duration:6000;transition3d:7;kenburnsscale:1.2;">
															<img loading="lazy" width="1280" height="520"
																src="https://www.bancocapital.com/wp-content/uploads/2022/05/ls-project-2-slide-7.jpg"
																class="ls-tn" alt=""
																srcset="https://www.bancocapital.com/wp-content/uploads/2022/05/ls-project-2-slide-7.jpg 1280w, https://www.bancocapital.com/wp-content/uploads/2022/05/ls-project-2-slide-7-300x122.jpg 300w, https://www.bancocapital.com/wp-content/uploads/2022/05/ls-project-2-slide-7-1024x416.jpg 1024w, https://www.bancocapital.com/wp-content/uploads/2022/05/ls-project-2-slide-7-768x312.jpg 768w"
																sizes="(max-width: 1280px) 100vw, 1280px" /><img
																loading="lazy" width="1000" height="667"
																src="https://www.bancocapital.com/wp-content/uploads/2020/12/S3.jpg"
																class="ls-l ls-img-layer" alt=""
																srcset="https://www.bancocapital.com/wp-content/uploads/2020/12/S3.jpg 1000w, https://www.bancocapital.com/wp-content/uploads/2020/12/S3-300x200.jpg 300w, https://www.bancocapital.com/wp-content/uploads/2020/12/S3-768x512.jpg 768w"
																sizes="(max-width: 1000px) 100vw, 1000px"
																style="top:1px;left:810px;background-size:inherit;background-position:inherit;"
																data-ls="position:relative;"><img loading="lazy"
																width="1000" height="668"
																src="https://www.bancocapital.com/wp-content/uploads/2020/12/S1.jpg"
																class="ls-l ls-img-layer" alt=""
																srcset="https://www.bancocapital.com/wp-content/uploads/2020/12/S1.jpg 1000w, https://www.bancocapital.com/wp-content/uploads/2020/12/S1-300x200.jpg 300w, https://www.bancocapital.com/wp-content/uploads/2020/12/S1-768x513.jpg 768w"
																sizes="(max-width: 1000px) 100vw, 1000px"
																style="top:-9px;left:-410px;background-size:inherit;background-position:inherit;width:1000px;height:668px;font-size:13px;line-height:falsepx;"
																data-ls="position:relative;"><img loading="lazy"
																width="540" height="667"
																src="https://www.bancocapital.com/wp-content/uploads/2020/12/S21.jpg"
																class="ls-l ls-img-layer" alt=""
																srcset="https://www.bancocapital.com/wp-content/uploads/2020/12/S21.jpg 540w, https://www.bancocapital.com/wp-content/uploads/2020/12/S21-243x300.jpg 243w"
																sizes="(max-width: 540px) 100vw, 540px"
																style="top:-8px;left:466px;background-size:inherit;background-position:inherit;"
																data-ls="position:relative;"><img loading="lazy"
																width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2020/12/Slider_0000s_0000_Group-14.png"
																class="ls-l ls-img-layer" alt=""
																srcset="https://www.bancocapital.com/wp-content/uploads/2020/12/Slider_0000s_0000_Group-14.png 1600w, https://www.bancocapital.com/wp-content/uploads/2020/12/Slider_0000s_0000_Group-14-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2020/12/Slider_0000s_0000_Group-14-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2020/12/Slider_0000s_0000_Group-14-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2020/12/Slider_0000s_0000_Group-14-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2020/12/Slider_0000s_0000_Group-14-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="top:-16px;left:-16px;background-size:inherit;background-position:inherit;width:1381px;height:561px;"
																data-ls="position:relative;"><a style="" class="ls-l"
																href="https://landings.markup.com.ec/bancocapital/credito-de-consumo/"
																target="_self" data-ls="position:relative;"><span
																	style="padding-top:10px;padding-right:40px;padding-bottom:10px;padding-left:40px;font-family:Arial, sans-serif;font-size:12px;font-weight:700;color:#fff;background-color:#f71818;top:241px;left:778px;"
																	class="ls-button-layer">M&aacute;s
																	Informaci&oacute;n</span></a>
														</div>
														<div class="ls-slide"
															data-ls="duration:10000;transition3d:7;kenburnsscale:1.2;">
															<img loading="lazy" width="1600" height="651"
																src="https://www.bancocapital.com/wp-content/uploads/2021/11/BC-DE-UNA-01.jpg"
																class="ls-bg" alt=""
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/11/BC-DE-UNA-01.jpg 1600w, https://www.bancocapital.com/wp-content/uploads/2021/11/BC-DE-UNA-01-300x122.jpg 300w, https://www.bancocapital.com/wp-content/uploads/2021/11/BC-DE-UNA-01-1024x417.jpg 1024w, https://www.bancocapital.com/wp-content/uploads/2021/11/BC-DE-UNA-01-768x312.jpg 768w, https://www.bancocapital.com/wp-content/uploads/2021/11/BC-DE-UNA-01-1536x625.jpg 1536w"
																sizes="(max-width: 1600px) 100vw, 1600px" />
														</div>
														<div class="ls-slide"
															data-ls="duration:6000;transition3d:7;transitionduration:1000;kenburnsscale:1.2;">
															<img loading="lazy" width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0002_01aXlsnN7L.png"
																class="ls-bg"
																alt="Banco Capital Renegociación de Crédito"
																srcset="https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0002_01aXlsnN7L.png 1600w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0002_01aXlsnN7L-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0002_01aXlsnN7L-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0002_01aXlsnN7L-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0002_01aXlsnN7L-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0002_01aXlsnN7L-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px" /><img
																loading="lazy" width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0000_Renegociacio%CC%81nDeCre%CC%81dito.png"
																class="ls-l ls-img-layer"
																alt="Renegociaci&oacute;n de Cr&eacute;dito"
																srcset="https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0000_Renegociacio&#769;nDeCre&#769;dito.png 1600w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0000_Renegociacio&#769;nDeCre&#769;dito-300x122.png 300w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0000_Renegociacio&#769;nDeCre&#769;dito-1024x416.png 1024w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0000_Renegociacio&#769;nDeCre&#769;dito-768x312.png 768w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0000_Renegociacio&#769;nDeCre&#769;dito-1536x624.png 1536w, https://www.bancocapital.com/wp-content/uploads/2020/04/SLider-Solucion_0000s_0000_Renegociacio&#769;nDeCre&#769;dito-1200x488.png 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px"
																style="top:61px;left:443px;background-size:inherit;background-position:inherit;"
																data-ls="position:relative;"><img loading="lazy"
																width="802" height="284"
																src="https://www.bancocapital.com/wp-content/uploads/2020/04/Artboard-1-copy-3333.png"
																class="ls-l ls-img-layer"
																alt="Renegociaci&oacute;n de Cr&eacute;dito"
																srcset="https://www.bancocapital.com/wp-content/uploads/2020/04/Artboard-1-copy-3333.png 802w, https://www.bancocapital.com/wp-content/uploads/2020/04/Artboard-1-copy-3333-300x106.png 300w, https://www.bancocapital.com/wp-content/uploads/2020/04/Artboard-1-copy-3333-768x272.png 768w"
																sizes="(max-width: 802px) 100vw, 802px"
																style="top:132px;left:73px;background-size:inherit;background-position:inherit;"
																data-ls="position:relative;"><a style="" class="ls-l"
																href="https://www.bancocapital.com/com-reneg/"
																target="_self" data-ls="position:relative;"><span
																	style="top:400px;left:488px;font-weight:700;background-size:inherit;background-position:inherit;padding-top:15px;padding-right:60px;padding-bottom:15px;padding-left:60px;font-family:Arial, sans-serif;font-size:14px;color:#fff;border-radius:0px;background-color:#fa231b;"
																	class="ls-button-layer">M&aacute;s
																	informaci&oacute;n</span></a>
														</div>
														<div class="ls-slide"
															data-ls="duration:5000;transition3d:7;kenburnsscale:1.2;">
															<img loading="lazy" width="1600" height="650"
																src="https://www.bancocapital.com/wp-content/uploads/2021/05/2.jpg"
																class="ls-bg"
																alt="Banco Capital / COSEDE /La Corporación del Seguro de Depósitos, Fondo de Liquidez y Fondo de Seguros Privados es la persona jurídica de derecho público, encargada de la administración"
																srcset="https://www.bancocapital.com/wp-content/uploads/2021/05/2.jpg 1600w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-300x122.jpg 300w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-1024x416.jpg 1024w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-768x312.jpg 768w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-1536x624.jpg 1536w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-1200x488.jpg 1200w"
																sizes="(max-width: 1600px) 100vw, 1600px" /><a style=""
																class="ls-l"
																href="https://www.cosede.gob.ec/conoce-tu-monto-de-cobertura/"
																target="_blank" data-ls="position:relative;"><img
																	loading="lazy" width="889" height="650"
																	src="https://www.bancocapital.com/wp-content/uploads/2021/05/2-copy.png"
																	class="ls-img-layer" alt="boton slider"
																	srcset="https://www.bancocapital.com/wp-content/uploads/2021/05/2-copy.png 889w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-copy-300x219.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-copy-768x562.png 768w"
																	sizes="(max-width: 889px) 100vw, 889px"
																	style="top:91px;left:27px;background-size:inherit;background-position:inherit;"></a><a
																style="" class="ls-l"
																href="https://educate.cosede.gob.ec/" target="_blank"
																data-ls="position:relative;"><img loading="lazy"
																	width="889" height="650"
																	src="https://www.bancocapital.com/wp-content/uploads/2021/05/2-copy.png"
																	class="ls-img-layer" alt="boton slider"
																	srcset="https://www.bancocapital.com/wp-content/uploads/2021/05/2-copy.png 889w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-copy-300x219.png 300w, https://www.bancocapital.com/wp-content/uploads/2021/05/2-copy-768x562.png 768w"
																	sizes="(max-width: 889px) 100vw, 889px"
																	style="top:56px;left:942px;background-size:inherit;background-position:inherit;width:433px;height:650px;"></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-785c974 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible"
					data-id="785c974" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;38e554a&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;animation&quot;:&quot;fadeInDown&quot;}">
					<div class="elementor-container elementor-column-gap-no">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c628db0"
								data-id="c628db0" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-f1e6301 elementor-widget elementor-widget-heading"
											data-id="f1e6301" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h2 class="elementor-heading-title elementor-size-default">¿Qué
													necesitas para tí?</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-39b3d31 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="39b3d31" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;24896ee&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-a3d3186"
								data-id="a3d3186" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-491211a elementor-view-default elementor-widget elementor-widget-icon"
											data-id="491211a" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<a class="elementor-icon"
														href="https://www.bancocapital.com/bp-form-credito-automotriz/">
														<svg id="icons" xmlns="http://www.w3.org/2000/svg"
															viewBox="0 0 300 300">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e31b23;
																	}
																</style>
															</defs>
															<title>BC ICONOS</title>
															<rect class="cls-1" x="161.15" y="111.16" width="5.65"
																height="7.99"
																transform="translate(-33.4 149.67) rotate(-45)" />
															<path class="cls-1"
																d="M226.15,84.07h-5.66A8.49,8.49,0,0,1,212,75.59V72.77h5.66v2.82a2.83,2.83,0,0,0,2.82,2.83h5.66A2.83,2.83,0,0,0,229,75.59V74A2.83,2.83,0,0,0,227,71.33l-9.23-3.07a8.47,8.47,0,0,1-5.8-8.05V58.64a8.49,8.49,0,0,1,8.48-8.48h5.66a8.48,8.48,0,0,1,8.47,8.48v2.82H229V58.64a2.83,2.83,0,0,0-2.82-2.83h-5.66a2.83,2.83,0,0,0-2.82,2.83v1.57a2.81,2.81,0,0,0,1.93,2.68L228.83,66a8.48,8.48,0,0,1,5.79,8v1.58A8.48,8.48,0,0,1,226.15,84.07Z" />
															<rect class="cls-1" x="220.49" y="47.33" width="5.65"
																height="5.65" />
															<rect class="cls-1" x="220.49" y="81.24" width="5.65"
																height="5.65" />
															<path class="cls-1"
																d="M273.63,79.56c-2.12-2.86-7.23-5.26-13.57-4v0c0-14.6-8.58-28.55-24.8-40.46,3.69-9,5.51-14.43,5.4-16.18l-.17-2.75-3.22.13c-1.64.08-4.7.23-11.84-1.58-4.48-1.13-13.8-3.5-18.09,4.2-.83,1.5-1.05,1.92,6.67,16.32-16.09,11.88-24.6,25.78-24.6,40.32A22.93,22.93,0,0,0,191,84.07h-1.56c-4.69,0-10.06,3.45-13.31,6.48l-9.3,9.31-3.66-3.66a2.82,2.82,0,0,0-4,0L145,110.33a2.86,2.86,0,0,0-.61.91l-.77-.49-29.75,33.48c-2.26.12-4.53.24-6.78.39-5.56.37-9.63,2.69-12.44,7.11l-1.79,2.83c-2.5,4-5.09,8.06-7.87,11.89a6.06,6.06,0,0,1-3.72,2,52.44,52.44,0,0,1-7.24.16c-1,0-2.07-.05-3.1-.05h0c-10.29,0-13.12,2.32-15.16,12.41-.76,3.79.5,6.3,3.87,7.7,1.57.65,3.09,1.35,4.85,2.17l.69.32a23,23,0,0,0-6,15.59c-.1,21.05-.11,39.65,0,56.89,0,5.48,3,8.56,8.42,8.67q6.15.14,12.19.14c4.19,0,8.32,0,12.36-.15,3.67-.08,8.09-1.67,8.27-8.75,0-1.83,0-3.64,0-5.4,0-.25,0-.51,0-.76H204.86v.54c0,1.58,0,3.22,0,4.87.16,6.54,3.1,9.49,9.53,9.58s13.32.08,21.87,0c7.12-.06,10-3,10-10.26q0-8.31,0-16.61c0-10.85,0-22.07.07-33.09.06-6.76-.38-14.46-6.16-21.29l.78-.35c1.7-.77,3.21-1.46,4.73-2.13,4.82-2.11,4.71-5.62,3.77-8.18-.27-.74-.48-1.5-.7-2.31q-.21-.8-.45-1.59c-1.75-5.68-4.77-7.92-10.77-8l-1.17,0c-4,0-8-.11-12.09.05a4,4,0,0,1-4.16-2.22c-1.85-3.06-3.85-6.09-5.78-9-1.29-2-2.58-3.91-3.84-5.88-2.72-4.28-6.7-6.52-12.17-6.88l-1,0-6.61-4.17,6.3-6.3a2.82,2.82,0,0,0,0-4l-3.46-3.46a4.46,4.46,0,0,1,1.46-.2h39.56c5.27,0,24.54-20.52,24.72-20.73,2.23-2.49,11.87-16.94,14.62-23.37A2.82,2.82,0,0,0,273.63,79.56Zm-49.58-59.4a54.7,54.7,0,0,0,10.17,1.74c-1,3.07-2.92,7.93-4.72,12.29a18.8,18.8,0,0,1-4.77.71,18.58,18.58,0,0,1-4.89-.8c-2.83-5.27-5.82-11-6.9-13.42C214.7,18.69,217.84,18.59,224.05,20.16Zm-29,55.36c0-13,7.69-25.16,22.82-36.15a23.48,23.48,0,0,0,6.85,1.19,22,22,0,0,0,6.9-1.16c15.1,11,22.78,23.12,22.78,36.12a19.17,19.17,0,0,1-.17,2.43c-3.06,2.13-5.33,5.42-7.58,9.68l-9.54,5A11.79,11.79,0,0,0,229,89.72l-14.69,0A15,15,0,0,1,211.66,88c-2.39-1.82-5.08-3.89-8.12-3.89h-6.29A16.63,16.63,0,0,1,195.06,75.52Zm-50.32,43,12.09,7.61,17.37,17.38c-1.74-.06-3.49-.11-5.24-.15L125.66,140Zm74.81,66.06a12.48,12.48,0,0,0,2.6-5.76c.25-1.94.6-2.24,2.5-2.14a119.44,119.44,0,0,0,13,0c2.31-.13,2.67.79,3.42,3.47l.19.66c.27.92.28,1-1.23,1.53a11.37,11.37,0,0,0-1.07.43c-2.16,1-4.38,2-6.72,3.09L229,187.33l-1.79.81,1.25,1.51c.62.75,1.19,1.47,1.73,2.15,1.15,1.44,2.23,2.79,3.42,4a16.66,16.66,0,0,1,4.52,12q0,11.28,0,22.56,0,15.84.05,31.69c0,1.25-.21,1.72-.39,1.9a2.77,2.77,0,0,1-1.77.34c-3.14-.07-6.4-.11-10-.11-3.36,0-7,0-11.15.1-1.07,0-1.45-.18-1.58-.3s-.31-.45-.32-1.44,0-1.73,0-2.45a22.74,22.74,0,0,1,.08-3.88,10.08,10.08,0,0,1,2.37-.19c.89,0,2,0,3.35-.12,1.53-.1,3-.22,4.57-.33,2.11-.16,4.21-.32,6.31-.43l.21,0c1.56-.08,4.46-.23,4.35-4.09a4.2,4.2,0,0,0-1.16-3.11c-1.12-1-2.66-.95-3.91-.88l-10.8.58c-11.74.63-23.88,1.28-35.81,1.72a933.7,933.7,0,0,1-104.09-2.15c-.29,0-.58-.07-.88-.11a5.45,5.45,0,0,0-3.21.16,5.65,5.65,0,0,0-3,3.74,2.91,2.91,0,0,0,.72,2.24A4.34,4.34,0,0,0,74.66,255c5.19.52,10.53.87,15.87,1,1.87.07,2.1.29,2.06,2,0,.69,0,1.31,0,1.88,0,2-.08,3.92-.34,4.26a10.75,10.75,0,0,1-2.93.13c-.82,0-1.77,0-2.9,0-2.42,0-4.84,0-7.27,0-3.3,0-6.71,0-10.07,0-1.09,0-1.42-.19-1.52-.28s-.3-.41-.29-1.48c0-5.9,0-11.8,0-17.71,0-12.58,0-25.6.26-38.38.07-2.85,1.72-5.91,3.32-8.88l.74-1.39A21,21,0,0,1,74.83,192c.69-.75,1.39-1.52,2.11-2.38l1.27-1.51-5.27-2.39-7.09-3.21c-.25-.12-.52-.23-.78-.33a4.41,4.41,0,0,1-1.06-.5,1.26,1.26,0,0,1,.12-.61,15.2,15.2,0,0,0,.47-1.65c.49-2,.76-2.57,2-2.61,4-.1,8-.08,12.17-.06l4,0c.7,6.37,3.93,10.37,9.62,11.91a27.67,27.67,0,0,0,5.9.75,1036.68,1036.68,0,0,0,109,0C211.73,189.24,216.38,188.67,219.55,184.59Zm-22.66-31.93c3.72.23,6,1.55,7.7,4.44,1.31,2.25,2.77,4.45,4.18,6.57,1.12,1.69,2.28,3.43,3.34,5.17a14.11,14.11,0,0,1,1.95,4.63c.52,2.85.31,4.81-.65,6s-2.85,1.81-5.77,1.9q-19.8.65-39.67,1.22l-15.35.47-15.36-.47c-13.13-.39-26.71-.8-40.1-1.25-1.82-.06-4.25-.48-5.45-2.14s-.83-3.8-.37-5.39a37.68,37.68,0,0,1,5.1-9.84c1.33-2,2.38-3.68,3.22-5,3.72-6,3.72-6,12.4-6.43,1.23-.06,2.61-.14,4.18-.24,11.28-.7,23-1.05,35.2-1.05C165.75,151.22,180.83,151.7,196.89,152.66Zm-11-15.33-26.58-16.74L151,112.33l10.14-10.13,29.91,29.91ZM255.12,102c-3.24,3.63-17.57,17.5-20.77,18.84H195.06a9,9,0,0,0-5.61,1.7l-18.66-18.66L180,94.62c2.61-2.44,6.67-4.9,9.38-4.9h14.13c1.12,0,3.26,1.64,4.68,2.73C210.27,94,212,95.38,214,95.38H229c3.91,0,5.65,2.12,5.65,4.23s-1.74,4.24-5.65,4.24H206.36v5.66H229c7.42,0,11.31-5,11.31-9.9a9.33,9.33,0,0,0-.27-2.16l10.05-5.22A2.8,2.8,0,0,0,251.27,91c3.3-6.41,5.76-9,9.43-9.84a10.17,10.17,0,0,1,7.2.78C264.64,88.38,256.9,100,255.12,102Z" />
															<path class="cls-1"
																d="M229.35,205.35l-.44-1-28.42,3.54-.22,1c-.35,1.61-.64,3.93.73,5.39a4.28,4.28,0,0,0,3.25,1.19,10.57,10.57,0,0,0,1.56-.13c7.31-1.06,13.46-1.87,19.71-2.39,1.39-.11,3.36-.44,4.3-2S230.18,207.3,229.35,205.35Z" />
															<path class="cls-1"
																d="M99.48,207.17l-10.25-1.31-2.07-.26a78,78,0,0,0-8.17-.78h0c-1.84,0-3.3,2.38-3.69,3.48a3,3,0,0,0,.51,2.49,3.44,3.44,0,0,0,2.18,1.76c6.63.91,13.15,1.68,20,2.5l6.63.79,1.2-7.85Z" />
														</svg> </a>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-df82826 elementor-widget elementor-widget-heading"
											data-id="df82826" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default"><a
														href="https://www.bancocapital.com/bp-form-credito-automotriz/">Crédito
														Automotriz</a></h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-1c514d1 elementor-widget elementor-widget-text-editor"
											data-id="1c514d1" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p>Tu crédito automotriz con las cuotas que más te convenga y con
														atención personalizada</p>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-c4101ed elementor-widget elementor-widget-jet-button"
											data-id="c4101ed" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="https://www.bancocapital.com/bp-form-credito-automotriz/">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-de7ec83 elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="de7ec83" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a href="https://www.bancocapital.com/bp-form-credito-automotriz/"
														class="elementor-button-link elementor-button elementor-size-sm"
														role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-16320da"
								data-id="16320da" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-5b30432 elementor-view-default elementor-widget elementor-widget-icon"
											data-id="5b30432" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<a class="elementor-icon"
														href="https://www.bancocapital.com/banca-personas/">
														<svg id="icons" xmlns="http://www.w3.org/2000/svg"
															viewBox="0 0 300 300">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e31b23;
																	}
																</style>
															</defs>
															<title>BC ICONOS</title>
															<path class="cls-1"
																d="M202.91,187.2c-24.18,0-49.88-7-49.88-19.95s25.7-19.95,49.88-19.95,49.88,7,49.88,19.95S227.09,187.2,202.91,187.2Zm0-33.25c-26.39,0-43.23,7.88-43.23,13.3s16.84,13.3,43.23,13.3,43.23-7.88,43.23-13.3S229.3,154,202.91,154Z" />
															<path class="cls-1"
																d="M202.91,203.83A129.7,129.7,0,0,1,187.7,203a3.33,3.33,0,0,1,.79-6.61,120.6,120.6,0,0,0,14.42.84c26.39,0,43.23-7.88,43.23-13.3V167.25a3.33,3.33,0,1,1,6.65,0v16.63C252.79,196.84,227.09,203.83,202.91,203.83Z" />
															<path class="cls-1"
																d="M202.91,220.46c-2.17,0-4.31-.05-6.4-.15a3.32,3.32,0,0,1-3.16-3.48,3.36,3.36,0,0,1,3.48-3.16q3,.14,6.08.14c26.39,0,43.23-7.88,43.23-13.3v-3.33c0-1.58-1.45-3.09-2.66-4.08a3.33,3.33,0,0,1,4.21-5.15c4.22,3.44,5.1,6.92,5.1,9.23v3.33C252.79,213.47,227.09,220.46,202.91,220.46Z" />
															<path class="cls-1"
																d="M202.91,237.09c-1.24,0-2.47,0-3.69-.05a3.33,3.33,0,0,1-3.24-3.42,3.28,3.28,0,0,1,3.42-3.23c1.16,0,2.33.05,3.51.05,26.39,0,43.23-7.88,43.23-13.3v-3.33c0-1.58-1.45-3.09-2.66-4.08a3.33,3.33,0,0,1,4.21-5.15c4.22,3.44,5.1,6.92,5.1,9.23v3.33C252.79,230.1,227.09,237.09,202.91,237.09Z" />
															<path class="cls-1"
																d="M202.91,253.72c-2,0-3.92-.05-5.83-.13a3.33,3.33,0,0,1-3.18-3.46,3.42,3.42,0,0,1,3.47-3.18c1.82.08,3.66.12,5.54.12,26.39,0,43.23-7.88,43.23-13.31v-3.32c0-1.58-1.45-3.09-2.66-4.08a3.33,3.33,0,0,1,4.21-5.16c4.22,3.45,5.1,6.93,5.1,9.24v3.32C252.79,246.72,227.09,253.72,202.91,253.72Z" />
															<path class="cls-1"
																d="M202.91,270.34a130.28,130.28,0,0,1-14.57-.8,3.33,3.33,0,1,1,.76-6.61,121.36,121.36,0,0,0,13.81.76c26.39,0,43.23-7.88,43.23-13.3v-3.32c0-1.58-1.45-3.1-2.66-4.08a3.33,3.33,0,0,1,4.21-5.16c4.22,3.45,5.1,6.92,5.1,9.24v3.32C252.79,263.35,227.09,270.34,202.91,270.34Z" />
															<path class="cls-1"
																d="M143.05,283.64a49.88,49.88,0,1,1,49.88-49.88A49.94,49.94,0,0,1,143.05,283.64Zm0-93.11a43.23,43.23,0,1,0,43.23,43.23A43.28,43.28,0,0,0,143.05,190.53Z" />
															<path class="cls-1"
																d="M146.38,257h-6.65a10,10,0,0,1-10-10v-3.33h6.65v3.33a3.33,3.33,0,0,0,3.33,3.32h6.65a3.32,3.32,0,0,0,3.32-3.32v-5.18a3.32,3.32,0,0,0-2.27-3.16l-10.86-3.62a10,10,0,0,1-6.82-9.46v-1.86a10,10,0,0,1,10-10h6.65a10,10,0,0,1,10,10v3.33H149.7v-3.33a3.32,3.32,0,0,0-3.32-3.32h-6.65a3.33,3.33,0,0,0-3.33,3.32v1.86a3.33,3.33,0,0,0,2.27,3.15l10.86,3.62a10,10,0,0,1,6.82,9.47v5.18A10,10,0,0,1,146.38,257Z" />
															<rect class="cls-1" x="139.73" y="207.15" width="6.65"
																height="9.98" />
															<rect class="cls-1" x="139.73" y="253.71" width="6.65"
																height="9.98" />
															<path class="cls-1"
																d="M156.35,180.55a3.32,3.32,0,0,1-3.32-3.32v-10a3.33,3.33,0,1,1,6.65,0v10A3.33,3.33,0,0,1,156.35,180.55Z" />
															<path class="cls-1"
																d="M168.52,117a17.28,17.28,0,0,1-7.06.79c-2-.22-14.89-.47-27.37-.7-7.11-.14-14.46-.28-20.58-.42a49.73,49.73,0,0,1-29.4-10.47C71.78,96.69,56.55,83.45,52.38,79.8l-35.7.48-.22-7.37,37.14-.52a3.67,3.67,0,0,1,2.55.92c.18.16,18.31,16.13,32.47,27.07a42.35,42.35,0,0,0,25.06,8.94c6.11.14,13.45.28,20.55.42,16,.3,25.87.5,28,.74,2.68.29,6.33-1,7.8-2.75.65-.78.52-1.23.45-1.47-1.2-4.1-6.37-10.39-12.65-13.42-1.56-.75-5.32-.9-8.94-1-7-.27-15.73-.61-22.41-5-4.65-3-6.58-10.69-6.92-12.19a3.72,3.72,0,0,1,.89-3.32l13.11-14.16a3.48,3.48,0,0,1,.62-.53l4.18-2.87a3.67,3.67,0,0,1,2.2-.65l20.76.66a3.74,3.74,0,0,1,1.81.54l18.42,11.3a3.68,3.68,0,0,1,1.76,3c0,.06.16,6,.81,12.63l-7.34.71c-.45-4.61-.67-8.78-.76-11.14l-15.91-9.75-18.57-.59-2.85,2L127.29,74.77c.76,2.32,2,5,3.27,5.86,5,3.27,12.23,3.55,18.64,3.8,4.79.19,8.92.35,11.86,1.77,7.48,3.61,14.58,11.34,16.52,18a8.76,8.76,0,0,1-1.87,8.28A15.76,15.76,0,0,1,168.52,117Z" />
															<path class="cls-1"
																d="M203.54,90l-7.37,0L196,57.76,160.52,30.68a27,27,0,0,0-17.43-5.53c-31.54,1.26-127.29,4-127.29,4l-.26-7.36s95.73-2.71,127.26-4a34.46,34.46,0,0,1,22.2,7L201.86,53a3.67,3.67,0,0,1,1.45,2.9Z" />
															<path class="cls-1"
																d="M189.31,142.64a33.2,33.2,0,0,1-43.15-28.56l7.34-.68A25.76,25.76,0,1,0,161.76,92l-5-5.43a33.17,33.17,0,1,1,32.53,56Z" />
														</svg> </a>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-ccd42d0 elementor-widget elementor-widget-heading"
											data-id="ccd42d0" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default"><a
														href="https://www.bancocapital.com/banca-personas/">Cuentas</a>
												</h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-fdd87ed elementor-widget elementor-widget-text-editor"
											data-id="fdd87ed" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p>Crea tu cuenta de manera rápida y eficaz siempre con una buena
														atención y preferencia.</p>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-4819ecb elementor-widget elementor-widget-jet-button"
											data-id="4819ecb" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="https://www.bancocapital.com/banca-personas/">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-aeb4c13 elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="aeb4c13" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a href="https://www.bancocapital.com/banca-personas/"
														class="elementor-button-link elementor-button elementor-size-sm"
														role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-16e7028"
								data-id="16e7028" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-08ebc76 elementor-view-default elementor-widget elementor-widget-icon"
											data-id="08ebc76" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<a class="elementor-icon"
														href="https://www.bancocapital.com/bp-form-inversiones/">
														<svg id="icons" xmlns="http://www.w3.org/2000/svg"
															viewBox="0 0 300 300">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e31b23;
																	}
																</style>
															</defs>
															<title>BC ICONOS</title>
															<path class="cls-1"
																d="M215.37,85.29l-8.31-5.58c3.2-4.77,7.52-12.31,11.34-19,2.27-4,4-7,5.35-9.22-5.28.6-11-1-16.07-2.51-13-3.75-25.09-6.5-30.62.4,1.77,2.25,4.42,6.81,8.65,14.16,3.24,5.63,6.91,12,9.67,16.13l-8.33,5.56c-2.94-4.4-6.53-10.65-10-16.7-3-5.29-6.83-11.87-8-13.12-1.29-1.43-4.29-4.77-1.84-9.25,9.15-16.64,30.48-10.48,43.23-6.8,5.69,1.65,10.3,2.84,13.32,2,4.59-1.34,8.53-.51,10.8,2.29,1.14,1.41,3.37,5.22-.47,10.25-1.07,1.41-4.1,6.7-7,11.82C223.18,72.52,218.77,80.23,215.37,85.29Z" />
															<path class="cls-1"
																d="M137.69,87.51H127.16v-10h10.53c1.15,0,3.07,0,8.84-6.78,7.26-8.54,16.37-13.23,25.66-13.23h5v10h-5c-6.31,0-12.71,3.45-18,9.71S144.09,87.51,137.69,87.51Z" />
															<path class="cls-1"
																d="M222.23,147.55H166.64c-7.56,0-14.3-3.85-19.71-7-2.2-1.25-5.21-3-6.24-3.06H127.16v-10h13.56c3.52,0,7.07,2,11.17,4.37,4.62,2.64,9.84,5.63,14.75,5.63h55.24c4.2-3.77,5-30.72,5.23-40.95l.12-4.23a2.18,2.18,0,0,0-.81-1.72c-1.76-1.81-5.63-3.13-9.2-3.13h-30v-10h30c6.32,0,12.75,2.43,16.39,6.18a12.25,12.25,0,0,1,3.63,9l-.13,4.19C236.29,126.54,234.74,147.55,222.23,147.55Z" />
															<path class="cls-1"
																d="M132.16,142.55h-50V67.49h50Zm-40-10h30v-55h-30Z" />
															<rect class="cls-1" x="102.14" y="87.51" width="10.01"
																height="10.01" />
															<path class="cls-1"
																d="M197.21,272.64c-34.79,0-60-25.25-60-60,0-25.14,18.1-51.34,32.64-72.39l.35-.51,8.24,5.69-.36.5c-13.75,19.92-30.87,44.69-30.87,66.71,0,29,21.05,50,50,50s50-21,50-50c0-22-17.11-46.79-30.87-66.71l-.35-.5,8.24-5.69.35.51c14.54,21,32.64,47.25,32.64,72.39C257.25,247.39,232,272.64,197.21,272.64Z" />
															<path class="cls-1"
																d="M202.21,232.61h-10a15,15,0,0,1-15-15v-5h10v5a5,5,0,0,0,5,5h10a5,5,0,0,0,5-5v-2.79a5,5,0,0,0-3.42-4.75l-16.34-5.45a15,15,0,0,1-10.27-14.24v-2.79a15,15,0,0,1,15-15h10a15,15,0,0,1,15,15v5h-10v-5a5,5,0,0,0-5-5h-10a5,5,0,0,0-5,5v2.79a5,5,0,0,0,3.42,4.75L207,200.57a15,15,0,0,1,10.26,14.24v2.79A15,15,0,0,1,202.21,232.61Z" />
															<rect class="cls-1" x="192.2" y="162.56" width="10.01"
																height="15.01" />
															<rect class="cls-1" x="192.2" y="227.61" width="10.01"
																height="15.01" />
															<path class="cls-1"
																d="M92.69,157.14h-61v-101h61Zm-52-9h43v-83h-43Z" />
															<path class="cls-1"
																d="M226.69,102.64h-34a3.5,3.5,0,0,1,0-7h34a3.5,3.5,0,0,1,0,7Z" />
															<path class="cls-1"
																d="M226.69,124.64h-36a3.5,3.5,0,0,1,0-7h36a3.5,3.5,0,0,1,0,7Z" />
														</svg> </a>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-da48058 elementor-widget elementor-widget-heading"
											data-id="da48058" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default"><a
														href="https://www.bancocapital.com/bp-form-inversiones/">Inversiones</a>
												</h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-5541df0 elementor-widget elementor-widget-text-editor"
											data-id="5541df0" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p>Rentabilice sus excedentes de capital a través de nuestros
														productos de Inversión.</p>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-2d13094 elementor-widget elementor-widget-jet-button"
											data-id="2d13094" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="https://www.bancocapital.com/bp-form-inversiones/">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-dd0898a elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="dd0898a" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a href="https://www.bancocapital.com/bp-form-inversiones/"
														class="elementor-button-link elementor-button elementor-size-sm"
														role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-76843a5 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="76843a5" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;24896ee&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-c354177"
								data-id="c354177" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-db9e20d elementor-view-default elementor-widget elementor-widget-icon"
											data-id="db9e20d" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<a class="elementor-icon"
														href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/multidestino/">
														<svg id="Layer_1" data-name="Layer 1"
															xmlns="http://www.w3.org/2000/svg"
															viewBox="0 0 283.46 283.46">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e21f26;
																	}
																</style>
															</defs>
															<path class="cls-1"
																d="M254.85,59.53a3.25,3.25,0,0,0-3.47-1.94C203,65,172.12,29.43,171.81,29.07a3.23,3.23,0,0,0-5.36.65,109.32,109.32,0,0,0-9.46,28,73.84,73.84,0,0,1-16.21-16.86,3.28,3.28,0,0,0-2.91-1.5,3.32,3.32,0,0,0-2.73,1.79,111.85,111.85,0,0,0-11.65,46.59c0,.09,0,.16,0,.25a3.12,3.12,0,0,0-.16,1.37c.88,8.18,3.29,32.63,5.06,39.24-27.93,9.13-27.76,33.07-26.68,41.72l-34,6.37a3.24,3.24,0,0,0-2.59,3.78l-.58-3.13-43.37,8.11L37,270.21,80.4,262.1l-.6-3.18a3.25,3.25,0,0,0,3.78,2.59l37.16-6.95L135,251.9l14.76-2.76,16.16-3L220.27,236a8.55,8.55,0,0,0,1.12-.29,17,17,0,0,0,12-19.53,17.32,17.32,0,0,0-1.54-4.53,17.13,17.13,0,0,0,6.88-17,16.88,16.88,0,0,0-4.23-8.42,17.15,17.15,0,0,0,2.27-12.1,14.79,14.79,0,0,0-7.07-10.44,17.82,17.82,0,0,0,1.73-12.41c-1.53-8.18-9-13.92-16.4-12.86a82,82,0,0,1,2.2-20.43c6.49-14.75,14-35.25,36.55-54.57A3.36,3.36,0,0,0,254.85,59.53Zm-84.6-22.7a98.09,98.09,0,0,0,8.1,7.06,9.89,9.89,0,0,1-5.56,4.44,9.16,9.16,0,0,1-6.68-.38A99.59,99.59,0,0,1,170.25,36.83Zm-31.56,12a89.27,89.27,0,0,0,8.64,9A9.37,9.37,0,0,1,141.57,63a9.54,9.54,0,0,1-7.57-.7A102.83,102.83,0,0,1,138.69,48.84Zm-6.32,19.79a15.66,15.66,0,0,0,9.15,1.08,17.89,17.89,0,0,0,2-.51,15.94,15.94,0,0,0,8.88-7.1A79.63,79.63,0,0,0,181,76.53a20.11,20.11,0,0,0,2,13.09,20.57,20.57,0,0,0,10.1,9.19c-6.05,19-4.34,37.06-3.26,44.23l-46,8.61c-10.59-24.29-11.53-40.49-14-63v0A2.52,2.52,0,0,0,130,88,109.74,109.74,0,0,1,132.37,68.63Zm54.9,9.28a75.88,75.88,0,0,0,15.22,1.17,76,76,0,0,0-7.23,13.67,14.17,14.17,0,0,1-6.58-6.18A13.5,13.5,0,0,1,187.27,77.91ZM74.58,195.73,73,187.15l-.87-4.68,30.78-5.76,13.48,72.07-30.79,5.76Zm150.52-43.3c1.2,6.4-1.87,9.53-2.79,9.83a3.25,3.25,0,0,0,1.2,6.38c2.3-.43,6,1.64,6.93,6.65a10.48,10.48,0,0,1-2.65,9.12,3.24,3.24,0,0,0,.46,4.82,10.51,10.51,0,0,1-2.19,18.24,3.24,3.24,0,0,0-1.26,5.05,10.75,10.75,0,0,1,2.26,4.8,10.52,10.52,0,0,1-7.46,12.1l-.53.15-54.39,10.17-16.15,3-14.76,2.76-11,2.06-14.13-75.47c-.3-1.17-6.67-27.83,21.61-37.27a129.22,129.22,0,0,0,8.95,22,.87.87,0,0,0,.09.11,3.4,3.4,0,0,0,.23.3,3.65,3.65,0,0,0,.55.56,3.78,3.78,0,0,0,.33.22,3.73,3.73,0,0,0,.71.31c.12,0,.23.08.36.1a3.23,3.23,0,0,0,1.16,0l73.13-13.68C219.36,144.11,224.13,147.23,225.1,152.43ZM211,116.16a3.27,3.27,0,0,0-.25,1.4,87.11,87.11,0,0,0-2.12,22l-12.41,2.33c-1.36-9.74-3.85-40.9,15.6-64.42a3.24,3.24,0,0,0-2.86-5.28A70.14,70.14,0,0,1,162.8,61.8c.43-2.57,1-5.11,1.59-7.62a15.56,15.56,0,0,0,8.42.82,14.61,14.61,0,0,0,1.92-.49,16.06,16.06,0,0,0,8.8-6.8A98.77,98.77,0,0,0,218,62.88a16.43,16.43,0,0,0,.4,11.94,15.79,15.79,0,0,0,7.71,8.06C222.46,88,213.52,110.45,211,116.16Zm19.11-38.62a9.26,9.26,0,0,1-5.78-5.26,9.94,9.94,0,0,1,.19-8.2,91.26,91.26,0,0,0,17.19,1A108,108,0,0,0,230.09,77.54Z" />
															<path class="cls-1"
																d="M97.33,232.23a4.11,4.11,0,1,0,4.8,3.29A4.12,4.12,0,0,0,97.33,232.23Z" />
															<path class="cls-1"
																d="M160.91,134.75a20.85,20.85,0,0,0,6-.23,18.88,18.88,0,0,0,7.24-3,14.94,14.94,0,0,0,2.45-23.12,19.66,19.66,0,0,0-12.25-5.9c-10.07-1.06-19,5.29-20,14.18S150.84,133.68,160.91,134.75ZM150.8,117.38a8.79,8.79,0,0,1,4-6.32,12.26,12.26,0,0,1,4.74-1.94,14.08,14.08,0,0,1,4.11-.15,13.15,13.15,0,0,1,8.22,3.9,8.48,8.48,0,0,1-1.41,13.34,13.17,13.17,0,0,1-8.86,2.09C155.07,127.61,150.24,122.71,150.8,117.38Z" />
														</svg> </a>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-45d5621 elementor-widget elementor-widget-heading"
											data-id="45d5621" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default"><a
														href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/multidestino/">Necesito
														un<br>Crédito Inmediato</a></h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-f945199 elementor-widget elementor-widget-text-editor"
											data-id="f945199" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													Obtén tu crédito para viajar, remodelar tu casa o <br>algún gasto
													inesperado </div>
											</div>
										</div>
										<div class="elementor-element elementor-element-263c4fe elementor-widget elementor-widget-jet-button"
											data-id="263c4fe" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/multidestino/">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-613349b elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="613349b" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-personas/multidestino/"
														class="elementor-button-link elementor-button elementor-size-sm"
														role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-abc6026"
								data-id="abc6026" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-37d9bff elementor-view-default elementor-widget elementor-widget-icon"
											data-id="37d9bff" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<a class="elementor-icon"
														href="https://bancaenlinea.bancocapital.com/">
														<svg id="Layer_1" data-name="Layer 1"
															xmlns="http://www.w3.org/2000/svg"
															viewBox="0 0 283.46 283.46">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e21f26;
																	}
																</style>
															</defs>
															<path class="cls-1"
																d="M177,151.32v25.84H72.3a15.1,15.1,0,0,1-15.1-15.1h0a15.09,15.09,0,0,1,3.48-9.64L132.36,66H58.77V40.14H158.94a15.12,15.12,0,0,1,15.12,15.12h0a15.13,15.13,0,0,1-3.47,9.63l-71.5,86.43Z" />
															<path class="cls-1" d="M195.2,71.85h30.53V177.16H195.2Z" />
															<path class="cls-1"
																d="M141.73,263.66a140.71,140.71,0,0,1-48.25-8.37,126.4,126.4,0,0,1-39.4-22.81,106.81,106.81,0,0,1-26.56-33.84,93.65,93.65,0,0,1-9.3-32.29c-.48-4.93,4.1-9.15,9.86-9.15h.17c5.15,0,9.4,3.4,9.85,7.81,4.62,45.62,49.34,81.53,103.63,81.53s99-35.91,103.63-81.53c.45-4.41,4.7-7.81,9.86-7.81h.17c5.76,0,10.34,4.22,9.85,9.15a93.44,93.44,0,0,1-9.3,32.29,106.81,106.81,0,0,1-26.56,33.84A126.29,126.29,0,0,1,190,255.29,140.67,140.67,0,0,1,141.73,263.66Z" />
															<path class="cls-1"
																d="M225.07,47.29H195.86a7.15,7.15,0,0,1,0-14.3h29.21a7.15,7.15,0,0,1,0,14.3Z" />
															<path class="cls-1"
																d="M247.74,24.83l-8.5,8.5a2.95,2.95,0,0,1-4.17-4.16l8.51-8.51a2.95,2.95,0,1,1,4.16,4.17Z" />
															<path class="cls-1"
																d="M247.74,54.78l-8.5-8.51a2.95,2.95,0,0,0-4.17,4.17l8.51,8.5a2.94,2.94,0,1,0,4.16-4.16Z" />
															<path class="cls-1"
																d="M250,42.75h-12a2.95,2.95,0,1,1,0-5.89h12a2.95,2.95,0,0,1,0,5.89Z" />
														</svg> </a>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-c369acb elementor-widget elementor-widget-heading"
											data-id="c369acb" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default"><a
														href="https://bancaenlinea.bancocapital.com/">Necesito
														Pagar<br>Servicios / Transferir</a></h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-09fe99f elementor-widget elementor-widget-text-editor"
											data-id="09fe99f" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p>Crea tu cuenta de manera rápida y eficaz siempre con una buena
														atención y preferencia.</p>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-cc57233 elementor-widget elementor-widget-jet-button"
											data-id="cc57233" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="https://bancaenlinea.bancocapital.com/">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-1cbc6fa elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="1cbc6fa" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a href="https://bancaenlinea.bancocapital.com/"
														class="elementor-button-link elementor-button elementor-size-sm"
														role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-3f0e425"
								data-id="3f0e425" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-515bfee elementor-view-default elementor-widget elementor-widget-icon"
											data-id="515bfee" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<div class="elementor-icon">
														<svg id="Layer_1" data-name="Layer 1"
															xmlns="http://www.w3.org/2000/svg"
															viewBox="0 0 283.46 283.46">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e21f26;
																	}
																</style>
															</defs>
															<path class="cls-1"
																d="M114.33,171.45c6.29,3.88,10.63,5.22,17.39,5.22s11.22-1.38,17.63-5.38a2.88,2.88,0,1,0-3-4.91c-5.86,3.65-9.19,4.51-14.61,4.51s-8.66-.83-14.38-4.36a2.88,2.88,0,1,0-3,4.92Z" />
															<path class="cls-1"
																d="M218.6,190.64a3.82,3.82,0,0,0,3.87-3.65c0-6.41-5-11.77-11.62-13.2V169.9a3.9,3.9,0,0,0-7.75,0v3.89c-6.65,1.43-11.62,6.79-11.62,13.2,0,5.87,4.16,11,10.35,12.91l1.27.38v14.07a6.81,6.81,0,0,1-3.87-6.13c0-1.92-1.74-3.31-3.88-3.31a3.83,3.83,0,0,0-3.87,3.64c0,6.42,5,11.77,11.62,13.2v3.9a3.9,3.9,0,0,0,7.75,0v-3.9c6.65-1.43,11.62-6.78,11.62-13.2,0-5.86-4.16-11.05-10.34-12.91l-1.28-.38V181.19a6.54,6.54,0,0,1,3.87,5.8v.33C214.72,189.24,216.46,190.64,218.6,190.64ZM199.23,187a6.56,6.56,0,0,1,3.87-5.8v11.55A6.51,6.51,0,0,1,199.23,187Zm15.49,21.56a6.55,6.55,0,0,1-3.87,5.81V202.8A6.52,6.52,0,0,1,214.72,208.55Z" />
															<path class="cls-1"
																d="M194,248.5A115.15,115.15,0,0,1,35.24,214.37a42.77,42.77,0,0,1,18-19.1c7.9-4.57,19.2-8.07,33.59-10.42l0,0q-.23.31-.42.63v0c-.13.21-.25.42-.36.64s-.07.15-.11.22-.15.31-.21.47l-.12.28c-.06.15-.11.3-.16.45s-.07.21-.1.31-.09.31-.13.47a3,3,0,0,0-.08.32c0,.16-.07.33-.1.5l-.06.32-.06.58,0,.29q0,.44,0,.9c0,13.8,17.54,28.08,46.89,28.08A72.12,72.12,0,0,0,157.56,215a49.8,49.8,0,0,1-1.61-5.59,66.3,66.3,0,0,1-24.23,4.14c-25.4,0-41.12-11.58-41.12-22.31,0-1.61,0-5.86,12.55-8a2.9,2.9,0,0,0,2.41-2.85v-15l.09.07v-7.72c-.3-.29-.61-.57-.9-.88l-.16-.14v0c-5.87-6.16-10-14.13-12.29-23.84a.49.49,0,0,0,0-.12c-.32-1.34-.6-2.72-.85-4.13,0-.2-.06-.39-.1-.58-.1-.64-.21-1.29-.3-2,0-.17-.05-.34-.07-.51-.16-1.16-.29-2.34-.41-3.54,0-.34-.07-.67-.09-1,0-.51-.08-1-.12-1.54s-.07-1.06-.1-1.59l-.06-1.29c0-.95-.07-1.9-.08-2.87v-.12c1.8-1.43,2.55-5,3.17-13.65v-.07c0-.07,0-.13,0-.2,0-.26,0-.53,0-.8l0-.43c0-.25,0-.5,0-.75l0-.5c0-.24,0-.47,0-.71s0-.39,0-.59,0-.44,0-.67,0-.45,0-.68,0-.35,0-.53c0-.71.08-1.44.12-2.2.16-3.13.31-6.09.5-7.83.69-6.37,2.71-12.42,5.54-16.6,3.19-4.73,8.14-4.71,15.6-3.09,7.3,1.59,14.19,3.09,22.1,2.16,2-.24,4.11-.65,6.1-1,5.29-1.06,10.28-2.07,14.73.07,7.25,3.48,9.92,12.11,10.73,15.65.18.79.31,4,.41,6.92,0,.82.06,1.6.1,2.37,0,.1,0,.21,0,.31,0,.73.06,1.44.1,2.13v.33c0,.68.07,1.35.11,2,0,.07,0,.14,0,.22.08,1.32.16,2.55.26,3.68a1.79,1.79,0,0,0,0,.23l.06.66s0,0,0,.07c.08.83.17,1.61.26,2.35,0,0,0,.08,0,.13a1,1,0,0,0,0,.17c.06.45.12.89.18,1.31v0q.09.63.21,1.2l0,.18c.07.36.14.7.22,1,0,.07,0,.13,0,.19.07.32.16.63.24.92a.56.56,0,0,1,0,.12c.09.29.18.56.28.82a.17.17,0,0,0,0,0,7,7,0,0,0,.31.73.61.61,0,0,1,0,.12,7,7,0,0,0,.34.61.41.41,0,0,0,.07.11,3.78,3.78,0,0,0,.22.33.56.56,0,0,1,.05.08l.1.12.05.06.07.09a.27.27,0,0,1,.07.08l.24.25,0,0h0v1.06c0,1.11,0,2.2-.05,3.27,0,.41,0,.81-.05,1.21,0,.64-.06,1.29-.09,1.92s-.06,1-.1,1.48-.08,1-.12,1.42c-.11,1.17-.23,2.33-.38,3.46,0,.21,0,.42-.08.62-.09.7-.2,1.38-.31,2.06,0,.21-.07.43-.11.64-.26,1.52-.55,3-.89,4.43,0,0,0,.09,0,.13q-3.56,15-12.78,24.32l-.39.39v0l-.07.07v7.67l.07-.06v4.88a1.6,1.6,0,0,1-.08.51v8.9a51.74,51.74,0,0,1,5.77-11.19V160a55.41,55.41,0,0,0,12.49-23.37c3.67-.24,7.06-3.6,8.92-8.91a78.23,78.23,0,0,0,3.1-18.7,15.05,15.05,0,0,0-2.32-8.74c2.58-2.7,3.38-5.82,3.95-8.83,1.63-8.62,1.78-18.51.43-29.42-.85-6.92-6.42-26.35-16.78-27.76a14.59,14.59,0,0,0-5.3.39c-1.41.31-2,.4-2.56.15a51.57,51.57,0,0,1-4-3.78c-1.2-1.2-2.56-2.56-4-3.95l0,0h0l0,0-.06-.06-1.35-1.26c-.44-.41-.89-.82-1.4-1.27,0,0-.07-.07-.07-.07l-1.35-1.17-.2-.17-1.36-1.12-.22-.18c-.46-.36-.91-.71-1.37-1.05l-.23-.17c-.47-.34-.94-.67-1.42-1l-.19-.13c-.49-.32-1-.64-1.48-.93l-.12-.07c-.57-.33-1.08-.61-1.56-.86-10.09-5-26.1-8.79-37.42-5.12-9.34,3-12.39,9.32-14.62,13.9-1,2.06-1.87,3.84-3,4.79-1,.81-2.14,1.66-3.36,2.57-5,3.67-10.58,7.82-11.26,13.66-.61,5.28.46,9.83,1.5,14.22a38.33,38.33,0,0,1,1.48,10.3A35.18,35.18,0,0,1,77,79.22c-1.12,4.27-2.28,8.68-1.12,13.51a6.05,6.05,0,0,0,.32,1,16.62,16.62,0,0,0,3,4.69c-2.6,2.24-4,5.81-4,10.58a78.26,78.26,0,0,0,3.11,18.7c1.86,5.31,5.25,8.67,8.92,8.91A55.56,55.56,0,0,0,99.78,160v17.35c-4.43.45-8.66,1-12.56,1.58-15.71,2.45-28.11,6.27-36.87,11.34-9.9,5.72-16.78,13.31-21,23.19a2.92,2.92,0,0,0,.22,2.69A120.91,120.91,0,0,0,202.2,249.9,51.76,51.76,0,0,1,194,248.5ZM176.31,103c-.07-.46-.14-1-.21-1.51l.91,0C176.75,102.05,176.52,102.54,176.31,103Zm3.3,22.8a12.1,12.1,0,0,1-1.88,3.59,100.4,100.4,0,0,0,1.38-17.15c0-.54,0-1.07,0-1.6a15.62,15.62,0,0,0,1.74-3.5c.28-.7.6-1.5,1-2.27a12.53,12.53,0,0,1,.58,4.14A73.21,73.21,0,0,1,179.61,125.82Zm-95.78,0A72.67,72.67,0,0,1,81.05,109a10.08,10.08,0,0,1,1-5.16c.5,3.27,1,6.18,2.23,8.16v.23c0,1.52,0,3,.08,4.49,0,0,0,.09,0,.14a97.34,97.34,0,0,0,1.29,12.54A12.12,12.12,0,0,1,83.83,125.82ZM88.36,85c0,.07,0,.14,0,.21,0,.48-.07,1-.1,1.52-.09,1.43-.18,3.06-.27,4.82,0,.94-.1,2-.16,3.17a1.21,1.21,0,0,0,0,.19c0,.27,0,.54,0,.82h0a18,18,0,0,0-3,.26L83.91,95a12.78,12.78,0,0,1-2.28-3.33l-.06-.14s0-.11,0-.19c-.82-3.41.11-6.95,1.09-10.69a39.21,39.21,0,0,0,1.59-8.62,43.39,43.39,0,0,0-1.63-11.85c-1-4.09-1.88-7.95-1.38-12.22.39-3.35,5.13-6.86,9-9.69,1.29-1,2.51-1.85,3.61-2.76,2.11-1.75,3.28-4.16,4.52-6.71,2.07-4.26,4.21-8.66,11.21-10.93,9.34-3,23.7.11,33.05,4.79.41.2.82.43,1.22.67l.12.06c.4.24.81.5,1.21.77l.18.11,1.2.85.22.16,1.22.94.21.17,1.23,1,.19.16c.42.35.84.72,1.29,1.12l.07.07c.43.38.86.77,1.31,1.19s.85.78,1.26,1.18l.06.05,0,0c1.42,1.35,2.76,2.69,3.95,3.87,3,3,4.39,4.36,5.6,4.92a9.27,9.27,0,0,0,6.27.28,9,9,0,0,1,3.29-.31c5.6.76,10.92,15.36,11.83,22.74,1.27,10.31,1.14,19.61-.38,27.64-.56,3-1.18,4.79-2.86,6.31a15,15,0,0,0-5.53-1h-.12l0-.56c0-.1,0-.21,0-.31l0-.58c0-.13,0-.26,0-.38l0-.58,0-.65c0-.11,0-.22,0-.33l0-.51a1.93,1.93,0,0,0,0-.24l0-.87v-.26l0-.49v-.09c0-.74-.05-1.43-.08-2a57,57,0,0,0-.55-8c-2.1-9.21-7.16-16.35-13.86-19.57-.37-.18-.74-.34-1.12-.49l-.37-.13-.75-.27-.44-.13-.68-.19L157,59l-.66-.14-.48-.08c-.22,0-.43-.07-.65-.09l-.48-.06-.65-.06-.48,0-.66,0h-1l-.71,0-.52,0-.7.05-.52,0-.7.08-.5.06-.72.09-.48.07-.73.12-.45.08-.77.14-.39.07-.86.16-.3.06-1.12.22c-2,.4-3.82.77-5.63,1-.44.05-.87.1-1.3.13-.86.07-1.72.11-2.56.11-5.52,0-10.81-1.1-16.35-2.3S101,55.3,95,64.2C90.44,71,89,79.13,88.51,83.41,88.46,83.88,88.41,84.42,88.36,85Z" />
															<path class="cls-1"
																d="M207,245.18a47.41,47.41,0,1,1,47.41-47.41A47.47,47.47,0,0,1,207,245.18Zm0-90.33a42.93,42.93,0,1,0,42.92,42.92A43,43,0,0,0,207,154.85Z" />
														</svg>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-8b3d2bc elementor-widget elementor-widget-heading"
											data-id="8b3d2bc" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default">Necesito
													un<br>Crédito de Nómina</h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-ecd5b06 elementor-widget elementor-widget-text-editor"
											data-id="ecd5b06" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p>Rentabilice sus excedentes de capital a través de nuestros
														productos de Inversión.</p>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-994019d elementor-widget elementor-widget-jet-button"
											data-id="994019d" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="/">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-44eb6c4 elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="44eb6c4" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a class="elementor-button elementor-size-sm" role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-f08f278 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="f08f278" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;caa63ec&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;background_background&quot;:&quot;classic&quot;}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-766ab09"
								data-id="766ab09" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-e9cd0ca elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
											data-id="e9cd0ca" data-element_type="widget"
											data-widget_type="divider.default">
											<div class="elementor-widget-container">
												<div class="elementor-divider">
													<span class="elementor-divider-separator">
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-91f275b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible"
					data-id="91f275b" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;38e554a&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;animation&quot;:&quot;fadeInDown&quot;}">
					<div class="elementor-container elementor-column-gap-no">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-41dce9c"
								data-id="41dce9c" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-e3ebde6 elementor-widget elementor-widget-heading"
											data-id="e3ebde6" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h2 class="elementor-heading-title elementor-size-default">¿Qué necesita
													tu negocio?</h2>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-0ed2367 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="0ed2367" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;24896ee&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-5efe932"
								data-id="5efe932" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-e4a7796 elementor-view-default elementor-widget elementor-widget-icon"
											data-id="e4a7796" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<a class="elementor-icon"
														href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/be-cash-management/">
														<svg xmlns="http://www.w3.org/2000/svg" id="Layer_1"
															data-name="Layer 1" viewBox="0 0 283.46 283.46">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e21f26;
																	}
																</style>
															</defs>
															<path class="cls-1"
																d="M178.57,202.66H151.69V69.85a11,11,0,0,0-11-11H27.23a11,11,0,0,0-10.95,11v60.53a3.39,3.39,0,0,0,3.39,3.39H31.41v72.28a18.54,18.54,0,0,0,18.52,18.52h113.5A18.55,18.55,0,0,0,182,206.05,3.39,3.39,0,0,0,178.57,202.66Zm-3.9,6.78a11.81,11.81,0,0,1-11.24,8.35H64.24a18.8,18.8,0,0,0,3.91-8.35ZM37.34,65.67H140.73a4.19,4.19,0,0,1,4.18,4.18V202.66H65.07a3.39,3.39,0,0,0-3.39,3.39,11.75,11.75,0,0,1-23.49,0V69.85A11.47,11.47,0,0,0,37.34,65.67Zm-5.93,4.18V127H23.06V69.85a4.18,4.18,0,1,1,8.35,0Z">
															</path>
															<path class="cls-1"
																d="M91.55,81.59A20.42,20.42,0,1,0,112,102,20.44,20.44,0,0,0,91.55,81.59ZM105.18,102A13.63,13.63,0,1,1,91.55,88.37,13.64,13.64,0,0,1,105.18,102Z">
															</path>
															<path class="cls-1"
																d="M63.57,108.71v6.54a3.38,3.38,0,0,0,1.87,3l6.17,3.09,1.52,6A3.39,3.39,0,0,0,76.42,130H78.7V149.3A3.39,3.39,0,0,0,84.21,152l7.34-5.87L98.89,152a3.39,3.39,0,0,0,5.51-2.65V130h2.28a3.38,3.38,0,0,0,3.29-2.57l1.52-6,6.17-3.09a3.35,3.35,0,0,0,1.87-3v-6.54l3.22-4.82a3.39,3.39,0,0,0,0-3.76l-3.22-4.82V88.77a3.37,3.37,0,0,0-1.87-3l-6.17-3.08L110,76.59A3.38,3.38,0,0,0,106.68,74H98.25l-4.82-3.21a3.39,3.39,0,0,0-3.76,0L84.85,74H76.42a3.39,3.39,0,0,0-3.29,2.57l-1.52,6.06-6.17,3.08a3.38,3.38,0,0,0-1.87,3v6.54l-3.21,4.81a3.38,3.38,0,0,0,0,3.77Zm49.75-10.5,2.53,3.8-2.53,3.79a3.41,3.41,0,0,0-.57,1.88v5.47L107.06,116a3.38,3.38,0,0,0-1.78,2.21l-1.24,5H97.23a3.33,3.33,0,0,0-1.89.57l-3.79,2.53-3.79-2.53a3.33,3.33,0,0,0-1.88-.57H79.07l-1.25-5A3.38,3.38,0,0,0,76,116l-5.69-2.85v-5.47a3.41,3.41,0,0,0-.57-1.88L67.25,102l2.53-3.8a3.36,3.36,0,0,0,.57-1.88V90.86L76,88a3.4,3.4,0,0,0,1.78-2.21l1.25-5h6.81a3.41,3.41,0,0,0,1.88-.57l3.79-2.53,3.79,2.53a3.41,3.41,0,0,0,1.89.57H104l1.24,5A3.43,3.43,0,0,0,107.06,88l5.69,2.84v5.47A3.41,3.41,0,0,0,113.32,98.21Zm-27.84,32.2,4.19,2.8a3.44,3.44,0,0,0,3.76,0l4.19-2.79v11.83l-4-3.16a3.41,3.41,0,0,0-4.24,0l-4,3.16Z">
															</path>
															<path class="cls-1"
																d="M93.44,92.9v-.35a1.89,1.89,0,0,0-3.78,0v.33a5.57,5.57,0,0,0,.27,10.63l2.2.63a1.82,1.82,0,0,1,1.31,1.74,1.85,1.85,0,0,1-2,1.8,1.81,1.81,0,0,1-1.8-1.8v-.63a1.89,1.89,0,0,0-3.78,0v.63a5.53,5.53,0,0,0,3.78,5.23v.36a1.89,1.89,0,0,0,3.78,0v-.33a5.57,5.57,0,0,0,3.79-5.26,5.63,5.63,0,0,0-4.06-5.38L91,99.87a1.81,1.81,0,0,1-1.31-1.73,1.85,1.85,0,0,1,2-1.81,1.81,1.81,0,0,1,1.8,1.81v.62a1.9,1.9,0,1,0,3.79,0v-.62A5.54,5.54,0,0,0,93.44,92.9Z">
															</path>
															<path class="cls-1"
																d="M53.72,164h75.66a3.39,3.39,0,1,0,0-6.78H53.72a3.39,3.39,0,0,0,0,6.78Z">
															</path>
															<path class="cls-1"
																d="M53.72,175.39H118a3.39,3.39,0,1,0,0-6.78H53.72a3.39,3.39,0,0,0,0,6.78Z">
															</path>
															<path class="cls-1"
																d="M53.72,186.74H118a3.39,3.39,0,1,0,0-6.78H53.72a3.39,3.39,0,0,0,0,6.78Z">
															</path>
															<path class="cls-1"
																d="M53.72,198.09H118a3.39,3.39,0,1,0,0-6.78H53.72a3.39,3.39,0,1,0,0,6.78Z">
															</path>
															<path class="cls-1"
																d="M129.38,168.61H125.6a3.39,3.39,0,0,0,0,6.78h3.78a3.39,3.39,0,1,0,0-6.78Z">
															</path>
															<path class="cls-1"
																d="M129.38,180H125.6a3.39,3.39,0,1,0,0,6.78h3.78a3.39,3.39,0,1,0,0-6.78Z">
															</path>
															<path class="cls-1"
																d="M129.38,191.31H125.6a3.39,3.39,0,0,0,0,6.78h3.78a3.39,3.39,0,1,0,0-6.78Z">
															</path>
															<path class="cls-1"
																d="M266.47,117.53h0a3.11,3.11,0,0,0-2.4-1.13H236.76a3.12,3.12,0,0,0,0,6.23h10.81A13.84,13.84,0,0,0,258,135.13l-4,21.16a18.39,18.39,0,0,0-14.27,11.8H195.94a3.12,3.12,0,0,0,0,6.23h58.38a3.12,3.12,0,0,0,3.06-2.53l9.75-51.7A3.05,3.05,0,0,0,266.47,117.53Zm-13.78,45.52-.95,5h-5.16A12,12,0,0,1,252.69,163.05Zm7.63-40.42-1.2,6.31a7.39,7.39,0,0,1-5.26-6.31Z">
															</path>
															<path class="cls-1"
																d="M170.12,134.67a18.31,18.31,0,0,0,17.12-12H227a3.12,3.12,0,0,0,0-6.23H172.6a3.12,3.12,0,0,0-3,2.49l-2.48,12a3.12,3.12,0,0,0,3.05,3.75Zm4-6.91,1.06-5.13h5.23A12,12,0,0,1,174.08,127.76Z">
															</path>
															<path class="cls-1"
																d="M186.19,168.09h-6.82a13.9,13.9,0,0,0-13.86-12.94,3.12,3.12,0,0,0-3,2.49l-2.67,12.94a3.11,3.11,0,0,0,3.05,3.74h23.35a3.12,3.12,0,0,0,0-6.23Zm-13.08,0h-6.44l1.3-6.3a7.55,7.55,0,0,1,3.47,2.41A7.71,7.71,0,0,1,173.11,168.09Zm-11.52,4.14Z">
															</path>
															<path class="cls-1"
																d="M232,153.21a3.12,3.12,0,0,0,2.4,1.12h11.7a3.13,3.13,0,0,0,3.07-2.55l2.15-11.7a3.1,3.1,0,0,0-3.07-3.68h-11.7a3.11,3.11,0,0,0-3.06,2.55l-2.16,11.69A3.1,3.1,0,0,0,232,153.21Zm6.14-5.11,1-5.47h5.37l-1,5.47Z">
															</path>
															<path class="cls-1"
																d="M198.77,151.78l2.16-11.71a3.11,3.11,0,0,0-3.06-3.67H186.16A3.11,3.11,0,0,0,183.1,139L181,150.64a3.1,3.1,0,0,0,.66,2.57,3.13,3.13,0,0,0,2.4,1.12h11.7A3.11,3.11,0,0,0,198.77,151.78Zm-4.64-9.15-1,5.47h-5.37l1-5.47Z">
															</path>
															<path class="cls-1"
																d="M214.73,137.21a3.75,3.75,0,0,1,2.65-.55,3.45,3.45,0,0,1,2.94,2.37,3.12,3.12,0,1,0,6-1.56,9.39,9.39,0,0,0-5.06-6l.09-.65a3.11,3.11,0,0,0-6.16-.9l-.1.64a9.72,9.72,0,0,0-3.87,1.52,8.76,8.76,0,0,0-3.86,6c-.6,4.14,1.2,9.34,8.26,10.37a4.17,4.17,0,0,1,2.57,1.12,2.64,2.64,0,0,1,.44,2.2,2.57,2.57,0,0,1-1,1.67,3.77,3.77,0,0,1-2.77.64,3.51,3.51,0,0,1-2.94-2.38,3.11,3.11,0,0,0-6,1.56,9.31,9.31,0,0,0,5,6l-.09.66a3.13,3.13,0,0,0,2.68,3.54,2.68,2.68,0,0,0,.4,0,3.12,3.12,0,0,0,3.08-2.66l.1-.65a9.16,9.16,0,0,0,7.75-7.53c.6-4.14-1.2-9.34-8.28-10.38-3-.44-3.19-1.92-3-3.31A2.59,2.59,0,0,1,214.73,137.21Z">
															</path>
															<path class="cls-1"
																d="M160.69,152.79h13.83a3.12,3.12,0,0,0,0-6.24H160.69a3.12,3.12,0,0,0,0,6.24Z">
															</path>
															<path class="cls-1"
																d="M165,144.17h13.82a3.12,3.12,0,1,0,0-6.23H165a3.12,3.12,0,1,0,0,6.23Z">
															</path>
															<path class="cls-1"
																d="M201.44,108.75a3.12,3.12,0,0,0,3.12,3.11h53.05a3.12,3.12,0,1,0,0-6.23H204.56A3.12,3.12,0,0,0,201.44,108.75Z">
															</path>
															<path class="cls-1"
																d="M185.52,105.63a3.12,3.12,0,0,0,0,6.23h8.27a3.12,3.12,0,0,0,0-6.23Z">
															</path>
															<path class="cls-1"
																d="M218.74,101.09h32.41a3.12,3.12,0,1,0,0-6.23H218.74a3.12,3.12,0,1,0,0,6.23Z">
															</path>
															<path class="cls-1"
																d="M179.06,101.09H208a3.12,3.12,0,1,0,0-6.23h-28.9a3.12,3.12,0,0,0,0,6.23Z">
															</path>
															<path class="cls-1"
																d="M239.24,178.86H212.48a3.12,3.12,0,1,0,0,6.23h26.76a3.12,3.12,0,1,0,0-6.23Z">
															</path>
															<path class="cls-1"
																d="M172.65,182a3.12,3.12,0,0,0,3.12,3.11h25.94a3.12,3.12,0,0,0,0-6.23H175.77A3.12,3.12,0,0,0,172.65,182Z">
															</path>
															<path class="cls-1"
																d="M230.63,189.64H199.51a3.11,3.11,0,1,0,0,6.22h31.12a3.11,3.11,0,0,0,0-6.22Z">
															</path>
															<path class="cls-1"
																d="M188.74,189.64H167.15a3.11,3.11,0,1,0,0,6.22h21.59a3.11,3.11,0,0,0,0-6.22Z">
															</path>
														</svg> </a>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-5d3141c elementor-widget elementor-widget-heading"
											data-id="5d3141c" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default"><a
														href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/be-cash-management/">Necesito
														Liquidez<br>para mi Negocio</a></h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-bf61d00 elementor-widget elementor-widget-text-editor"
											data-id="bf61d00" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p>Tener liquidez para un negocio es importante y nosotros te lo
														podemos proporcionar</p>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-440259c elementor-widget elementor-widget-jet-button"
											data-id="440259c" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/be-cash-management/">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-6216a90 elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="6216a90" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a href="https://www.bancocapital.com/inicio-banco-familiar/banca-empresas/be-cash-management/"
														class="elementor-button-link elementor-button elementor-size-sm"
														role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-8edf54a"
								data-id="8edf54a" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-25be139 elementor-view-default elementor-widget elementor-widget-icon"
											data-id="25be139" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<a class="elementor-icon"
														href="https://www.bancocapital.com/php/verificarStatusE.php?cert=1">
														<svg xmlns="http://www.w3.org/2000/svg" id="Layer_1"
															data-name="Layer 1" viewBox="0 0 283.46 283.46">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e21f26;
																	}
																</style>
															</defs>
															<path class="cls-1"
																d="M215,141.52a71.61,71.61,0,1,0-71.61,71.6A71.69,71.69,0,0,0,215,141.52Zm-8.14,0a63.47,63.47,0,1,1-63.47-63.47A63.54,63.54,0,0,1,206.89,141.52Z">
															</path>
															<path class="cls-1"
																d="M275.24,212.32,256.82,193.9a4.07,4.07,0,0,0-6.95,2.87V205h-51.6A83.88,83.88,0,0,0,143.42,57.63H37V49.42A4.07,4.07,0,0,0,30,46.54L11.61,65a4.08,4.08,0,0,0,0,5.76L30,89.14A4.07,4.07,0,0,0,37,86.26V78.05H88.57A83.89,83.89,0,0,0,143.42,225.4H249.87v8.21a4.06,4.06,0,0,0,2.51,3.76,4,4,0,0,0,1.56.31,4,4,0,0,0,2.88-1.19l18.42-18.42a4.07,4.07,0,0,0,0-5.75Zm-21.3,4.94H143.42A75.74,75.74,0,0,1,103.08,77.42a4.07,4.07,0,0,0-2.17-7.51h-68A4.07,4.07,0,0,0,28.84,74v2.45l-8.6-8.59,8.6-8.6V61.7a4.08,4.08,0,0,0,4.07,4.07H143.42a75.74,75.74,0,0,1,40.35,139.84,4.07,4.07,0,0,0,2.17,7.51h68a4.07,4.07,0,0,0,4.07-4.07V206.6l8.6,8.59-8.6,8.6v-2.46A4.08,4.08,0,0,0,253.94,217.26Z">
															</path>
															<path class="cls-1"
																d="M156.47,134.09,147,131.32v-15.7a26.61,26.61,0,0,1,6.22,1.14,14.64,14.64,0,0,1,5.43,3l9.48-11.56a25.62,25.62,0,0,0-10.07-4.83A46.72,46.72,0,0,0,147,102V95.08h-6.52V102a31.52,31.52,0,0,0-8.44,2.18,26.07,26.07,0,0,0-7.21,4.34,20.29,20.29,0,0,0-5,6.42,18.51,18.51,0,0,0-1.88,8.39q0,7.52,3.91,11.8t11.4,6.67l7.21,2.17v16.69a28.38,28.38,0,0,1-7.7-1.63,14.11,14.11,0,0,1-6.22-4.49l-10.47,11.75a27.77,27.77,0,0,0,11.41,6.22,48.47,48.47,0,0,0,13,1.78v6.91H147v-6.91a38.34,38.34,0,0,0,9.18-2.07,24.94,24.94,0,0,0,7.61-4.35,19.81,19.81,0,0,0,5.13-6.72,21.17,21.17,0,0,0,1.88-9.18,16.75,16.75,0,0,0-3.9-11.4A21.77,21.77,0,0,0,156.47,134.09Zm-16-4.35c-.39-.13-1-.29-1.78-.49a8.17,8.17,0,0,1-2.37-1,7.09,7.09,0,0,1-2.07-2,6.2,6.2,0,0,1-.89-3.56,5.22,5.22,0,0,1,2.17-4.64,13.71,13.71,0,0,1,4.94-2.07Zm14.17,26.32a7.45,7.45,0,0,1-1.92,2.12,10.19,10.19,0,0,1-2.72,1.43,20,20,0,0,1-3,.84V145.64l1.87.69c.86.33,1.68.68,2.47,1a9.49,9.49,0,0,1,2.08,1.28,5.74,5.74,0,0,1,1.43,1.83,6,6,0,0,1,.54,2.67A5.57,5.57,0,0,1,154.64,156.06Z">
															</path>
														</svg> </a>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-e2f3e89 elementor-widget elementor-widget-heading"
											data-id="e2f3e89" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default"><a
														href="https://www.bancocapital.com/php/verificarStatusE.php?cert=1">Necesito
														Cobrar y<br>Pagar en Línea</a></h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-c072200 elementor-widget elementor-widget-text-editor"
											data-id="c072200" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p>Crea tu cuenta de manera rápida y eficaz siempre con una buena
														atención y preferencia.</p>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-0b7f355 elementor-widget elementor-widget-jet-button"
											data-id="0b7f355" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="https://www.bancocapital.com/php/verificarStatusE.php?cert=1">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-26b0e40 elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="26b0e40" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a href="https://www.bancocapital.com/php/verificarStatusE.php?cert=1"
														class="elementor-button-link elementor-button elementor-size-sm"
														role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-7667b52"
								data-id="7667b52" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-99d6e79 elementor-view-default elementor-widget elementor-widget-icon"
											data-id="99d6e79" data-element_type="widget"
											data-widget_type="icon.default">
											<div class="elementor-widget-container">
												<div class="elementor-icon-wrapper">
													<div class="elementor-icon">
														<svg xmlns="http://www.w3.org/2000/svg" id="Layer_1"
															data-name="Layer 1" viewBox="0 0 283.46 283.46">
															<defs>
																<style>
																	.cls-1 {
																		fill: #e21f26;
																	}
																</style>
															</defs>
															<path class="cls-1"
																d="M231.4,76a2.81,2.81,0,0,0-1.69-2.11L224,71.41c1.24-1.49,2.45-2.85,3.61-4.08a2.86,2.86,0,0,0-1-4.57l-7-3c1.18-1.6,2.29-3.05,3.37-4.38a2.74,2.74,0,0,0,.41-.68A2.86,2.86,0,0,0,221.83,51l-10.62-4.5,1.7-4a2.86,2.86,0,0,0-1.51-3.73L171.68,21.91A2.84,2.84,0,0,0,168,23.42l-9.83,23.17a2.85,2.85,0,0,0,5.25,2.23l3.9-9.21a15.47,15.47,0,0,0,16.15-6.54l11.79,5a15.45,15.45,0,0,0,6.54,16.15l-15.86,37.4a2.85,2.85,0,1,0,5.25,2.22L209,51.72l7.19,3.05c-10.28,13.57-19.6,31.69-26,45.35H187.7c0-.06,0-.13,0-.19a2.84,2.84,0,0,0-1.53-1.55,2.8,2.8,0,0,0-2.18,0,2.84,2.84,0,0,0-1.55,1.53l-.1.23H170.85c.13-.24.26-.48.37-.74l1-2.43a6.93,6.93,0,0,0-3.67-9.07l-2.43-1a6.94,6.94,0,0,0-9.08,3.67L156,93h0a6.93,6.93,0,0,0,1.1,7.17H151.8a15.44,15.44,0,0,0-6.36-9l14.42-34a2.85,2.85,0,0,0-5.24-2.22l-19.18,45.25h-80L52,103.55V115l3.43,3.44h8.64v26A21.55,21.55,0,0,0,71,160.24v93.42H62.64v8.12H213.91v-8.12H205.6V160.24a21.58,21.58,0,0,0,6.86-15.77v-26h8.64l3.44-3.44V103.56l-3.44-3.44H214c5.45-9,11-16.26,16.54-21.6a2.71,2.71,0,0,0,.64-.94A2.82,2.82,0,0,0,231.4,76ZM167,94.73l-1,2.43a1.27,1.27,0,0,1-.67.66,1.22,1.22,0,0,1-.94,0l-2.43-1a1.24,1.24,0,0,1-.66-.67,1.23,1.23,0,0,1,0-.94l1-2.43a1.19,1.19,0,0,1,.67-.65,1.22,1.22,0,0,1,.46-.09,1.36,1.36,0,0,1,.48.09l2.42,1a1.24,1.24,0,0,1,.66.67A1.22,1.22,0,0,1,167,94.73Zm2.56-60.52,2.52-5.94L178,30.79A9.67,9.67,0,0,1,169.56,34.21Zm31.05,6.15,5.94,2.52L204,48.82A9.68,9.68,0,0,1,200.61,40.36Zm-83,183.23-5.39-5.39-1.06,1.06a14.71,14.71,0,0,0,0,20.83l1.06,1.06,5.39-5.39v17.9H95.18v-48h22.46Zm0,11.48-.72-.72a6.63,6.63,0,0,1,0-9.35l.72-.71ZM178,148.53H191.4v-8.12h-14V118.47h26.95v26A13.47,13.47,0,0,1,178,148.53Zm-55.71,49.05H90.5L87.06,201v52.65h-8V165a21,21,0,0,0,8,1v26.07l3.44,3.44h4.68V163.85a21.82,21.82,0,0,0,8-6.8,21.57,21.57,0,0,0,35.07,0,21.56,21.56,0,0,0,35.06,0,21.9,21.9,0,0,0,8,6.8V187.4H107.53v8.12h78.52l3.44-3.44V166a21.21,21.21,0,0,0,8-1v88.64H125.75V201Zm-15-57.17V118.47h27v26a13.47,13.47,0,0,1-26.32,4.06h13.37v-8.12Zm35.06,0V118.47h26.95v26A13.47,13.47,0,0,1,143,148.53h13.37v-8.12Zm-70.13,0V118.47h27v26a13.47,13.47,0,0,1-26.32,4.06H86.2v-8.12ZM60.13,110.35v-2.12H216.42v2.12Zm145.08-10.23c1-1.94,2.07-3.82,3.08-5.6a1.59,1.59,0,0,0,.16-.32,2.84,2.84,0,0,0-1.21-3.59,2.87,2.87,0,0,0-3.89,1.07c-1.51,2.67-3,5.5-4.57,8.44h-2.26a273.16,273.16,0,0,1,19.69-35.57l4.49,1.9a142.72,142.72,0,0,0-12.85,17.69,2.85,2.85,0,0,0,4.83,3c2.5-4,5.06-7.73,7.61-11.12l3.35,1.42a127.72,127.72,0,0,0-16.27,22.66Zm-59.4,0h-4.19l1.53-3.6A9.86,9.86,0,0,1,145.81,100.12Z">
															</path>
															<path class="cls-1"
																d="M178.61,57.46l2.42,1a6.94,6.94,0,0,0,9.08-3.67l1-2.43h0a6.93,6.93,0,0,0-3.68-9.08l-2.42-1A6.94,6.94,0,0,0,176,46l-1,2.43A6.92,6.92,0,0,0,178.61,57.46Zm7.28-7.29-1,2.43a1.26,1.26,0,0,1-1.61.65l-2.43-1a1.23,1.23,0,0,1-.65-1.61l1-2.43a1.22,1.22,0,0,1,1.13-.75,1.18,1.18,0,0,1,.48.1l2.42,1a1.21,1.21,0,0,1,.66.67A1.23,1.23,0,0,1,185.89,50.17Z">
															</path>
															<path class="cls-1"
																d="M168.92,67.43h0a2.85,2.85,0,0,0,2-.84,2.86,2.86,0,0,0,0-4,2.85,2.85,0,0,0-2-.84h0a8.57,8.57,0,0,0-6.54,3.11l-.57-.24a2.85,2.85,0,0,0-2.22,5.24l.57.24a8.39,8.39,0,0,0,4.9,8.64c3.55,1.5,8.59,1.11,11.15-4.93A3.49,3.49,0,0,1,178,71.65a2.75,2.75,0,0,1,1.86.21,2.38,2.38,0,0,1,1.32,1.51,3.44,3.44,0,0,1-.13,2.46,3.27,3.27,0,0,1-2.82,2.09h0a2.85,2.85,0,0,0,0,5.69h0a8.58,8.58,0,0,0,6.53-3.09l.57.25a2.85,2.85,0,0,0,3.73-1.52h0a2.8,2.8,0,0,0,0-2.18,2.84,2.84,0,0,0-1.53-1.55l-.56-.24a8.39,8.39,0,0,0-10.91-9A8.77,8.77,0,0,0,171,71.57c-1.1,2.59-2.48,2.42-3.68,1.91a3,3,0,0,1-1.19-4A3.26,3.26,0,0,1,168.92,67.43Z">
															</path>
															<path class="cls-1"
																d="M186.05,243.09l3.44-3.43V201l-3.44-3.43H134.29L130.85,201v38.65l3.44,3.43Zm-4.68-37.4V235H139V205.69Z">
															</path>
															<rect class="cls-1" x="158.8" y="222.3" width="8.12"
																height="8.12"
																transform="translate(-112.36 181.46) rotate(-45)">
															</rect>
															<rect class="cls-1" x="165.04" y="212.66" width="14.92"
																height="8.12"
																transform="translate(-102.72 185.48) rotate(-45)">
															</rect>
															<rect class="cls-1" x="153.43" y="210.25" width="8.12"
																height="8.12"
																transform="translate(-105.41 174.13) rotate(-45)">
															</rect>
															<rect class="cls-1" x="140.38" y="219.89" width="14.92"
																height="8.12"
																transform="translate(-115.06 170.15) rotate(-45)">
															</rect>
															<rect class="cls-1" x="193.52" y="140.41" width="8.12"
																height="8.12"></rect>
															<rect class="cls-1" x="158.45" y="140.41" width="8.12"
																height="8.12"></rect>
															<rect class="cls-1" x="123.39" y="140.41" width="8.12"
																height="8.12"></rect>
															<rect class="cls-1" x="88.32" y="140.41" width="8.12"
																height="8.12"></rect>
															<rect class="cls-1" x="97.3" y="187.4" width="8.12"
																height="8.12"></rect>
														</svg>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-c4d5b65 elementor-widget elementor-widget-heading"
											data-id="c4d5b65" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h1 class="elementor-heading-title elementor-size-default">Necesito un
													Crédito<br>para un Emprendimiento</h1>
											</div>
										</div>
										<div class="elementor-element elementor-element-e162997 elementor-widget elementor-widget-text-editor"
											data-id="e162997" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p>Rentabilice sus excedentes de capital a través de nuestros
														productos de Inversión.</p>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-a2409fa elementor-widget elementor-widget-jet-button"
											data-id="a2409fa" data-element_type="widget"
											data-widget_type="jet-button.default">
											<div class="elementor-widget-container">
												<div class="elementor-jet-button jet-elements">
													<div class="jet-button__container">
														<a class="jet-button__instance jet-button__instance--icon-right hover-effect-0"
															href="/">
															<div class="jet-button__plane jet-button__plane-normal">
															</div>
															<div class="jet-button__plane jet-button__plane-hover">
															</div>
															<div class="jet-button__state jet-button__state-normal">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
															<div class="jet-button__state jet-button__state-hover">
																<span class="jet-button__icon jet-elements-icon"><i
																		aria-hidden="true"
																		class="fas fa-arrow-alt-circle-right"></i></span><span
																	class="jet-button__label">Solicítalo</span>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
										<div class="elementor-element elementor-element-66ba288 elementor-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-button"
											data-id="66ba288" data-element_type="widget"
											data-widget_type="button.default">
											<div class="elementor-widget-container">
												<div class="elementor-button-wrapper">
													<a class="elementor-button elementor-size-sm" role="button">
														<span class="elementor-button-content-wrapper">
															<span
																class="elementor-button-icon elementor-align-icon-right">
																<i aria-hidden="true"
																	class="fas fa-arrow-alt-circle-right"></i> </span>
															<span class="elementor-button-text">Solicítalo</span>
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-b4e5e30 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="b4e5e30" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;2d184aa&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-e9dacee"
								data-id="e9dacee" data-element_type="column">
								<div class="elementor-column-wrap">
									<div class="elementor-widget-wrap">
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-30439be"
								data-id="30439be" data-element_type="column">
								<div class="elementor-column-wrap">
									<div class="elementor-widget-wrap">
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-9e032e8"
								data-id="9e032e8" data-element_type="column">
								<div class="elementor-column-wrap">
									<div class="elementor-widget-wrap">
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-b54e767 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="b54e767" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;8180c07&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;background_background&quot;:&quot;classic&quot;}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-1c68ee0"
								data-id="1c68ee0" data-element_type="column"
								data-settings="{&quot;animation&quot;:&quot;none&quot;}">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-44998de elementor-aspect-ratio-169 elementor-widget elementor-widget-video"
											data-id="44998de" data-element_type="widget"
											data-settings="{&quot;youtube_url&quot;:&quot;https:\/\/youtu.be\/cgSNfqbtVGE&quot;,&quot;start&quot;:1,&quot;loop&quot;:&quot;yes&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;controls&quot;:&quot;yes&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}"
											data-widget_type="video.default">
											<div class="elementor-widget-container">
												<div
													class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
													<div class="elementor-video"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-93ed339"
								data-id="93ed339" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<section
											class="elementor-section elementor-inner-section elementor-element elementor-element-760d614 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
											data-id="760d614" data-element_type="section"
											data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;ceb61bd&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
											<div class="elementor-container elementor-column-gap-default">
												<div class="elementor-row">
													<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-9e5acaf"
														data-id="9e5acaf" data-element_type="column">
														<div class="elementor-column-wrap elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-951bb6f elementor--h-position-right elementor--v-position-bottom elementor-arrows-position-inside elementor-pagination-position-inside elementor-widget elementor-widget-slides"
																	data-id="951bb6f" data-element_type="widget"
																	data-settings="{&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}"
																	data-widget_type="slides.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-swiper">
																			<div class="elementor-slides-wrapper elementor-main-swiper swiper-container"
																				dir="ltr" data-animation="fadeInUp">
																				<div
																					class="swiper-wrapper elementor-slides">
																					<div
																						class="elementor-repeater-item-1346559 swiper-slide">
																						<div class="swiper-slide-bg">
																						</div>
																						<div
																							class="elementor-background-overlay">
																						</div><a
																							class="swiper-slide-inner"
																							href="https://www.bancocapital.com/bp-credito-automotriz/">
																							<div
																								class="swiper-slide-contents">
																								<div
																									class="elementor-slide-heading">
																									Crédito </div>
																								<div
																									class="elementor-slide-description">
																									Automotriz</div>
																								<div
																									class="elementor-button elementor-slide-button elementor-size-xs">
																									Más Información
																								</div>
																							</div>
																						</a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</section>
										<section
											class="elementor-section elementor-inner-section elementor-element elementor-element-64a3aed elementor-section-boxed elementor-section-height-default elementor-section-height-default"
											data-id="64a3aed" data-element_type="section"
											data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;ceb61bd&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
											<div class="elementor-container elementor-column-gap-default">
												<div class="elementor-row">
													<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6e80d33"
														data-id="6e80d33" data-element_type="column">
														<div class="elementor-column-wrap elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-2d108bf elementor--h-position-right elementor--v-position-bottom elementor-arrows-position-inside elementor-pagination-position-inside elementor-widget elementor-widget-slides"
																	data-id="2d108bf" data-element_type="widget"
																	data-settings="{&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}"
																	data-widget_type="slides.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-swiper">
																			<div class="elementor-slides-wrapper elementor-main-swiper swiper-container"
																				dir="ltr" data-animation="fadeInUp">
																				<div
																					class="swiper-wrapper elementor-slides">
																					<div
																						class="elementor-repeater-item-1346559 swiper-slide">
																						<div class="swiper-slide-bg">
																						</div><a
																							class="swiper-slide-inner"
																							href="https://bancocapital.tusfinanzas.ec/">
																							<div
																								class="swiper-slide-contents">
																								<div
																									class="elementor-slide-heading">
																									Capital</div>
																								<div
																									class="elementor-slide-description">
																									EDUCA</div>
																								<div
																									class="elementor-button elementor-slide-button elementor-size-xs">
																									Más Información
																								</div>
																							</div>
																						</a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</section>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-b3de484 elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="b3de484" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;8180c07&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;background_background&quot;:&quot;classic&quot;}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-b39fa98"
								data-id="b39fa98" data-element_type="column">
								<div class="elementor-column-wrap">
									<div class="elementor-widget-wrap">
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-d1acd3a"
								data-id="d1acd3a" data-element_type="column"
								data-settings="{&quot;animation&quot;:&quot;none&quot;}">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-868cd5c elementor-aspect-ratio-11 elementor-widget elementor-widget-video"
											data-id="868cd5c" data-element_type="widget"
											data-settings="{&quot;video_type&quot;:&quot;hosted&quot;,&quot;start&quot;:1,&quot;aspect_ratio&quot;:&quot;11&quot;,&quot;controls&quot;:&quot;yes&quot;}"
											data-widget_type="video.default">
											<div class="elementor-widget-container">
												<div
													class="e-hosted-video elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
													<video class="elementor-video"
														src="https://www.bancocapital.com/wp-content/uploads/2020/01/Video.mp4#t=1"
														controls="" controlsList="nodownload"></video>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-d4212d0"
								data-id="d4212d0" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<section
											class="elementor-section elementor-inner-section elementor-element elementor-element-cdfd813 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
											data-id="cdfd813" data-element_type="section"
											data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;ceb61bd&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
											<div class="elementor-container elementor-column-gap-default">
												<div class="elementor-row">
													<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-eed1f9e"
														data-id="eed1f9e" data-element_type="column">
														<div class="elementor-column-wrap elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-68dfc09 elementor--h-position-right elementor--v-position-bottom elementor-arrows-position-inside elementor-pagination-position-inside elementor-widget elementor-widget-slides"
																	data-id="68dfc09" data-element_type="widget"
																	data-settings="{&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}"
																	data-widget_type="slides.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-swiper">
																			<div class="elementor-slides-wrapper elementor-main-swiper swiper-container"
																				dir="ltr" data-animation="fadeInUp">
																				<div
																					class="swiper-wrapper elementor-slides">
																					<div
																						class="elementor-repeater-item-1346559 swiper-slide">
																						<div class="swiper-slide-bg">
																						</div>
																						<div
																							class="elementor-background-overlay">
																						</div><a
																							class="swiper-slide-inner"
																							href="https://www.bancocapital.com/bp-credito-automotriz/">
																							<div
																								class="swiper-slide-contents">
																								<div
																									class="elementor-slide-heading">
																									Crédito </div>
																								<div
																									class="elementor-slide-description">
																									Automotriz</div>
																								<div
																									class="elementor-button elementor-slide-button elementor-size-xs">
																									Más Información
																								</div>
																							</div>
																						</a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</section>
										<section
											class="elementor-section elementor-inner-section elementor-element elementor-element-534aaba elementor-section-boxed elementor-section-height-default elementor-section-height-default"
											data-id="534aaba" data-element_type="section"
											data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;ceb61bd&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
											<div class="elementor-container elementor-column-gap-default">
												<div class="elementor-row">
													<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-820aa37"
														data-id="820aa37" data-element_type="column">
														<div class="elementor-column-wrap elementor-element-populated">
															<div class="elementor-widget-wrap">
																<div class="elementor-element elementor-element-c543662 elementor--h-position-right elementor--v-position-bottom elementor-arrows-position-inside elementor-pagination-position-inside elementor-widget elementor-widget-slides"
																	data-id="c543662" data-element_type="widget"
																	data-settings="{&quot;navigation&quot;:&quot;both&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;autoplay_speed&quot;:5000,&quot;infinite&quot;:&quot;yes&quot;,&quot;transition&quot;:&quot;slide&quot;,&quot;transition_speed&quot;:500}"
																	data-widget_type="slides.default">
																	<div class="elementor-widget-container">
																		<div class="elementor-swiper">
																			<div class="elementor-slides-wrapper elementor-main-swiper swiper-container"
																				dir="ltr" data-animation="fadeInUp">
																				<div
																					class="swiper-wrapper elementor-slides">
																					<div
																						class="elementor-repeater-item-1346559 swiper-slide">
																						<div class="swiper-slide-bg">
																						</div><a
																							class="swiper-slide-inner"
																							href="https://bancocapital.tusfinanzas.ec/">
																							<div
																								class="swiper-slide-contents">
																								<div
																									class="elementor-slide-heading">
																									Capital</div>
																								<div
																									class="elementor-slide-description">
																									EDUCA</div>
																								<div
																									class="elementor-button elementor-slide-button elementor-size-xs">
																									Más Información
																								</div>
																							</div>
																						</a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</section>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-615aaa4"
								data-id="615aaa4" data-element_type="column">
								<div class="elementor-column-wrap">
									<div class="elementor-widget-wrap">
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-d015fa1 elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible"
					data-id="d015fa1" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;38e554a&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;animation&quot;:&quot;fadeInDown&quot;}">
					<div class="elementor-container elementor-column-gap-no">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c7ea8fb"
								data-id="c7ea8fb" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-830e7cf elementor-widget-divider--view-line elementor-widget elementor-widget-divider"
											data-id="830e7cf" data-element_type="widget"
											data-widget_type="divider.default">
											<div class="elementor-widget-container">
												<div class="elementor-divider">
													<span class="elementor-divider-separator">
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section
					class="elementor-section elementor-top-section elementor-element elementor-element-9fbc56e elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default"
					data-id="9fbc56e" data-element_type="section"
					data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;8f56692&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-row">
							<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-0e36903"
								data-id="0e36903" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-a1ac416 elementor-widget elementor-widget-heading"
											data-id="a1ac416" data-element_type="widget"
											data-widget_type="heading.default">
											<div class="elementor-widget-container">
												<h2 class="elementor-heading-title elementor-size-default">El registro
													civil<br>informa</h2>
											</div>
										</div>
										<div class="elementor-element elementor-element-78bb4c1 elementor-widget elementor-widget-text-editor"
											data-id="78bb4c1" data-element_type="widget"
											data-widget_type="text-editor.default">
											<div class="elementor-widget-container">
												<div class="elementor-text-editor elementor-clearfix">
													<p style="text-align: justify">El Registro Civil de Ecuador informa
														que, las<strong> cédulas expiradas o por expirar</strong>
														<strong>entre el 16 de marzo de 2020 y el 31 de mayo de
															2021</strong>, <strong>tendrán validez</strong> para todos
														sus trámites públicos y privados.
													</p>
													<p style="text-align: right; font-weight:600; font-size:12pt"> <a
															href="https://www.registrocivil.gob.ec/cedulas-caducadas-o-por-caducar-seran-validas-hasta-el-31-de-mayo-de-2021/">Más
															Información&#8230;</a></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-113dd92"
								data-id="113dd92" data-element_type="column">
								<div class="elementor-column-wrap elementor-element-populated">
									<div class="elementor-widget-wrap">
										<div class="elementor-element elementor-element-a2a96c2 elementor-aspect-ratio-169 elementor-widget elementor-widget-video"
											data-id="a2a96c2" data-element_type="widget"
											data-settings="{&quot;youtube_url&quot;:&quot;https:\/\/youtu.be\/KNfk-b1yPHw&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;controls&quot;:&quot;yes&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}"
											data-widget_type="video.default">
											<div class="elementor-widget-container">
												<div
													class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
													<div class="elementor-video"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

	<div data-elementor-type="footer" data-elementor-id="487" class="elementor elementor-487 elementor-location-footer">
		<div class="elementor-section-wrap">
			<section
				class="elementor-section elementor-top-section elementor-element elementor-element-0896877 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="0896877" data-element_type="section"
				data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;jet_parallax_layout_list&quot;:[{&quot;_id&quot;:&quot;b24c467&quot;,&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}]}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-968aa24"
							data-id="968aa24" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-c98a40a elementor-widget elementor-widget-heading"
										data-id="c98a40a" data-element_type="widget" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<h1 class="elementor-heading-title elementor-size-default">Nosotros</h1>
										</div>
									</div>
									<div class="elementor-element elementor-element-087a5aa elementor-nav-menu__align-left elementor-nav-menu--dropdown-none elementor-widget elementor-widget-nav-menu"
										data-id="087a5aa" data-element_type="widget"
										data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;}}"
										data-widget_type="nav-menu.default">
										<div class="elementor-widget-container">
											<nav migration_allowed="1" migrated="0" role="navigation"
												class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-vertical e--pointer-none">
												<ul id="menu-1-087a5aa" class="elementor-nav-menu sm-vertical">
													<li
														class="menu-item menu-item-type-custom menu-item-object-custom menu-item-118">
														<a href="https://www.bancocapital.com/red-oficinas/"
															class="elementor-item">Ubicación</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-116">
														<a href="https://www.bancocapital.com/canales-de-atencion/contactanos/"
															class="elementor-item">Contáctanos</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117">
														<a href="https://www.bancocapital.com/conocenos/"
															class="elementor-item">Conócenos</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114">
														<a href="https://www.bancocapital.com/canales-de-atencion/trabaja-con-nosotros/"
															class="elementor-item">Trabaja con Nosotros</a>
													</li>
												</ul>
											</nav>
											<div class="elementor-menu-toggle" role="button" tabindex="0"
												aria-label="Alternar menú" aria-expanded="false">
												<i aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--open eicon-menu-bar"></i><i
													aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--close eicon-close"></i> <span
													class="elementor-screen-only">Menú</span>
											</div>
											<nav class="elementor-nav-menu--dropdown elementor-nav-menu__container"
												role="navigation" aria-hidden="true">
												<ul id="menu-2-087a5aa" class="elementor-nav-menu sm-vertical">
													<li
														class="menu-item menu-item-type-custom menu-item-object-custom menu-item-118">
														<a href="https://www.bancocapital.com/red-oficinas/"
															class="elementor-item" tabindex="-1">Ubicación</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-116">
														<a href="https://www.bancocapital.com/canales-de-atencion/contactanos/"
															class="elementor-item" tabindex="-1">Contáctanos</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117">
														<a href="https://www.bancocapital.com/conocenos/"
															class="elementor-item" tabindex="-1">Conócenos</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114">
														<a href="https://www.bancocapital.com/canales-de-atencion/trabaja-con-nosotros/"
															class="elementor-item" tabindex="-1">Trabaja con
															Nosotros</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-cf44321"
							data-id="cf44321" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-0f66559 elementor-widget elementor-widget-heading"
										data-id="0f66559" data-element_type="widget" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<h1 class="elementor-heading-title elementor-size-default">Acerca</h1>
										</div>
									</div>
									<div class="elementor-element elementor-element-82533c8 elementor-nav-menu__align-left elementor-nav-menu--dropdown-none elementor-widget elementor-widget-nav-menu"
										data-id="82533c8" data-element_type="widget"
										data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;}}"
										data-widget_type="nav-menu.default">
										<div class="elementor-widget-container">
											<nav migration_allowed="1" migrated="0" role="navigation"
												class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-vertical e--pointer-none">
												<ul id="menu-1-82533c8" class="elementor-nav-menu sm-vertical">
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110">
														<a href="https://www.bancocapital.com/canales-de-atencion/transparencia/"
															class="elementor-item">Transparencia</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112">
														<a href="https://www.bancocapital.com/canales-de-atencion/defensor-del-cliente/"
															class="elementor-item">Defensor del Cliente</a>
													</li>
													<li
														class="menu-item menu-item-type-custom menu-item-object-custom menu-item-466">
														<a href="https://bancocapital.tusfinanzas.ec/"
															class="elementor-item">Educación Financiera</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113">
														<a href="https://www.bancocapital.com/canales-de-atencion/"
															class="elementor-item">Canales de Atención</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10250">
														<a href="https://www.bancocapital.com/proteccion-de-datos/"
															class="elementor-item">Aviso de la política de privacidad de
															datos</a>
													</li>
												</ul>
											</nav>
											<div class="elementor-menu-toggle" role="button" tabindex="0"
												aria-label="Alternar menú" aria-expanded="false">
												<i aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--open eicon-menu-bar"></i><i
													aria-hidden="true" role="presentation"
													class="elementor-menu-toggle__icon--close eicon-close"></i> <span
													class="elementor-screen-only">Menú</span>
											</div>
											<nav class="elementor-nav-menu--dropdown elementor-nav-menu__container"
												role="navigation" aria-hidden="true">
												<ul id="menu-2-82533c8" class="elementor-nav-menu sm-vertical">
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110">
														<a href="https://www.bancocapital.com/canales-de-atencion/transparencia/"
															class="elementor-item" tabindex="-1">Transparencia</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112">
														<a href="https://www.bancocapital.com/canales-de-atencion/defensor-del-cliente/"
															class="elementor-item" tabindex="-1">Defensor del
															Cliente</a>
													</li>
													<li
														class="menu-item menu-item-type-custom menu-item-object-custom menu-item-466">
														<a href="https://bancocapital.tusfinanzas.ec/"
															class="elementor-item" tabindex="-1">Educación
															Financiera</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113">
														<a href="https://www.bancocapital.com/canales-de-atencion/"
															class="elementor-item" tabindex="-1">Canales de Atención</a>
													</li>
													<li
														class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10250">
														<a href="https://www.bancocapital.com/proteccion-de-datos/"
															class="elementor-item" tabindex="-1">Aviso de la política de
															privacidad de datos</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-9265463"
							data-id="9265463" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-c8f369b elementor-widget elementor-widget-heading"
										data-id="c8f369b" data-element_type="widget" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<h1 class="elementor-heading-title elementor-size-default">Redes Sociales
											</h1>
										</div>
									</div>
									<div class="elementor-element elementor-element-2a2160e elementor-shape-circle e-grid-align-left elementor-grid-0 elementor-widget elementor-widget-social-icons"
										data-id="2a2160e" data-element_type="widget"
										data-widget_type="social-icons.default">
										<div class="elementor-widget-container">
											<div class="elementor-social-icons-wrapper elementor-grid">
												<span class="elementor-grid-item">
													<a class="elementor-icon elementor-social-icon elementor-social-icon-facebook elementor-repeater-item-8c186e4"
														href="https://www.facebook.com/bancocapital.ecua"
														target="_blank">
														<span class="elementor-screen-only">Facebook</span>
														<i class="fab fa-facebook"></i> </a>
												</span>
												<span class="elementor-grid-item">
													<a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-repeater-item-0ea31e5"
														href="https://www.instagram.com/bancocapitaloficial/"
														target="_blank">
														<span class="elementor-screen-only">Instagram</span>
														<i class="fab fa-instagram"></i> </a>
												</span>
												<span class="elementor-grid-item">
													<a class="elementor-icon elementor-social-icon elementor-social-icon-youtube elementor-repeater-item-fb70873"
														href="https://www.youtube.com/channel/UCFe-ZhTpzLPqgAco9hKpS2g/videos"
														target="_blank">
														<span class="elementor-screen-only">Youtube</span>
														<i class="fab fa-youtube"></i> </a>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-0ef221d"
							data-id="0ef221d" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-0994f85 elementor-widget elementor-widget-heading"
										data-id="0994f85" data-element_type="widget" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<h1 class="elementor-heading-title elementor-size-default">Contacto</h1>
										</div>
									</div>
									<div class="elementor-element elementor-element-9287bb2 elementor-widget elementor-widget-text-editor"
										data-id="9287bb2" data-element_type="widget"
										data-widget_type="text-editor.default">
										<div class="elementor-widget-container">
											<div class="elementor-text-editor elementor-clearfix">
												<p>1700-CAPITAL<br />Tel: (593) 226-5230 / 225-9021<br />Dirección:
													Avenida Amazonas N34-289 y Avenida Atahualpa.<br />Quito &#8211;
													Ecuador.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section
				class="elementor-section elementor-top-section elementor-element elementor-element-6d9fff8 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="6d9fff8" data-element_type="section"
				data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;_id&quot;:&quot;70adb52&quot;,&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;background_background&quot;:&quot;classic&quot;}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-78637d6"
							data-id="78637d6" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-43827aa elementor-widget elementor-widget-text-editor"
										data-id="43827aa" data-element_type="widget"
										data-widget_type="text-editor.default">
										<div class="elementor-widget-container">
											<div class="elementor-text-editor elementor-clearfix">
												<p>Derechos Reservados para Banco Capital 2019. Powered by Markup
													Ecuador.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>

	<script>function loadScript(a) { var b = document.getElementsByTagName("head")[0], c = document.createElement("script"); c.type = "text/javascript", c.src = "https://tracker.metricool.com/app/resources/be.js", c.onreadystatechange = a, c.onload = a, b.appendChild(c) } loadScript(function () { beTracker.t({ hash: 'd03173a787b4186bd6542bab5295c856' }) })</script>
	<script type="text/x-template" id="mobile-menu-item-template"><li
	:id="'jet-menu-item-'+itemDataObject.itemId"
	:class="itemClasses"
>
	<div
		class="jet-mobile-menu__item-inner"
		tabindex="1"
		:aria-label="itemDataObject.name"
		v-on:click="itemSubHandler"
		v-on:keyup.enter="itemSubHandler"
	>
		<a
			:class="itemLinkClasses"
			:href="itemDataObject.url"
			:rel="itemDataObject.xfn"
			:title="itemDataObject.attrTitle"
			:target="itemDataObject.target"
		>
			<div class="jet-menu-item-wrapper">
				<div
					class="jet-menu-icon"
					v-if="isIconVisible"
					v-html="itemIconHtml"
				></div>
				<div class="jet-menu-name">
					<span
						class="jet-menu-label"
						v-html="itemDataObject.name"
					></span>
					<small
						class="jet-menu-desc"
						v-if="isDescVisible"
						v-html="itemDataObject.description"
					></small>
				</div>
				<small
					class="jet-menu-badge"
					v-if="isBadgeVisible"
				>
					<span class="jet-menu-badge__inner">{{ itemDataObject.badgeText }}</span>
				</small>
			</div>
		</a>
		<span
			class="jet-dropdown-arrow"
			v-if="isSub && !templateLoadStatus"
			v-html="dropdownIconHtml"
			v-on:click="markerSubHandler"
		>
		</span>
		<div
			class="jet-mobile-menu__template-loader"
			v-if="templateLoadStatus"
		>
			<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="24px" height="25px" viewBox="0 0 128 128" xml:space="preserve">
				<g>
					<linearGradient id="linear-gradient">
						<stop offset="0%" :stop-color="loaderColor" stop-opacity="0"/>
						<stop offset="100%" :stop-color="loaderColor" stop-opacity="1"/>
					</linearGradient>
				<path d="M63.85 0A63.85 63.85 0 1 1 0 63.85 63.85 63.85 0 0 1 63.85 0zm.65 19.5a44 44 0 1 1-44 44 44 44 0 0 1 44-44z" fill="url(#linear-gradient)" fill-rule="evenodd"/>
				<animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1080ms" repeatCount="indefinite"></animateTransform>
				</g>
			</svg>
		</div>
	</div>

	<transition name="menu-container-expand-animation">
		<mobile-menu-list
			v-if="isDropdownLayout && subDropdownVisible"
			:depth="depth+1"
			:children-object="itemDataObject.children"
		></mobile-menu-list>
	</transition>

</li>
					</script>
	<script type="text/x-template" id="mobile-menu-list-template"><div
	class="jet-mobile-menu__list"
	role="navigation"
>
	<ul class="jet-mobile-menu__items">
		<mobile-menu-item
			v-for="(item, index) in childrenObject"
			:key="item.id"
			:item-data-object="item"
			:depth="depth"
		></mobile-menu-item>
	</ul>
</div>
					</script>
	<script type="text/x-template" id="mobile-menu-template"><div
	:class="instanceClass"
	v-on:keyup.esc="escapeKeyHandler"
>
	<div
		class="jet-mobile-menu__toggle"
		ref="toggle"
		tabindex="1"
		aria-label="Open/Close Menu"
		v-on:click="menuToggle"
		v-on:keyup.enter="menuToggle"
	>
		<div
			class="jet-mobile-menu__template-loader"
			v-if="toggleLoaderVisible"
		>
			<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="24px" height="25px" viewBox="0 0 128 128" xml:space="preserve">
				<g>
					<linearGradient id="linear-gradient">
						<stop offset="0%" :stop-color="loaderColor" stop-opacity="0"/>
						<stop offset="100%" :stop-color="loaderColor" stop-opacity="1"/>
					</linearGradient>
				<path d="M63.85 0A63.85 63.85 0 1 1 0 63.85 63.85 63.85 0 0 1 63.85 0zm.65 19.5a44 44 0 1 1-44 44 44 44 0 0 1 44-44z" fill="url(#linear-gradient)" fill-rule="evenodd"/>
				<animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1080ms" repeatCount="indefinite"></animateTransform>
				</g>
			</svg>
		</div>

		<div
			class="jet-mobile-menu__toggle-icon"
			v-if="!menuOpen && !toggleLoaderVisible"
			v-html="toggleClosedIcon"
		></div>
		<div
			class="jet-mobile-menu__toggle-icon"
			v-if="menuOpen && !toggleLoaderVisible"
			v-html="toggleOpenedIcon"
		></div>
		<span
			class="jet-mobile-menu__toggle-text"
			v-if="toggleText"
			v-html="toggleText"
		></span>

	</div>

	<transition name="cover-animation">
		<div
			class="jet-mobile-menu-cover"
			v-if="menuContainerVisible && coverVisible"
			v-on:click="closeMenu"
		></div>
	</transition>

	<transition :name="showAnimation">
		<div
			class="jet-mobile-menu__container"
			v-if="menuContainerVisible"
		>
			<div
				class="jet-mobile-menu__container-inner"
			>
				<div
					class="jet-mobile-menu__header-template"
					v-if="headerTemplateVisible"
				>
					<div
						class="jet-mobile-menu__header-template-content"
						ref="header-template-content"
						v-html="headerContent"
					></div>
				</div>

				<div
					class="jet-mobile-menu__controls"
				>
					<div
						class="jet-mobile-menu__breadcrumbs"
						v-if="isBreadcrumbs"
					>
						<div
							class="jet-mobile-menu__breadcrumb"
							v-for="(item, index) in breadcrumbsPathData"
							:key="index"
						>
							<div
								class="breadcrumb-label"
								v-on:click="breadcrumbHandle(index+1)"
								v-html="item"
							></div>
							<div
								class="breadcrumb-divider"
								v-html="breadcrumbIcon"
								v-if="(breadcrumbIcon && index !== breadcrumbsPathData.length-1)"
							></div>
						</div>
					</div>
					<div
						class="jet-mobile-menu__back"
						ref="back"
						tabindex="1"
						aria-label="Close Menu"
						v-if="!isBack && isClose"
						v-html="closeIcon"
						v-on:click="menuToggle"
						v-on:keyup.enter="menuToggle"
					></div>
					<div
						class="jet-mobile-menu__back"
						ref="back"
						tabindex="1"
						aria-label="Back to Prev Items"
						v-if="isBack"
						v-html="backIcon"
						v-on:click="goBack"
						v-on:keyup.enter="goBack"
					></div>
				</div>

				<div
					class="jet-mobile-menu__before-template"
					v-if="beforeTemplateVisible"
				>
					<div
						class="jet-mobile-menu__before-template-content"
						ref="before-template-content"
						v-html="beforeContent"
					></div>
				</div>

				<div
					class="jet-mobile-menu__body"
				>
					<transition :name="animation">
						<mobile-menu-list
							v-if="!templateVisible"
							:key="depth"
							:depth="depth"
							:children-object="itemsList"
						></mobile-menu-list>
						<div
							class="jet-mobile-menu__template"
							ref="template-content"
							v-if="templateVisible"
						>
							<div
								class="jet-mobile-menu__template-content"
								v-html="itemTemplateContent"
							></div>
						</div>
					</transition>
				</div>

				<div
					class="jet-mobile-menu__after-template"
					v-if="afterTemplateVisible"
				>
					<div
						class="jet-mobile-menu__after-template-content"
						ref="after-template-content"
						v-html="afterContent"
					></div>
				</div>

			</div>
		</div>
	</transition>
</div>
					</script>
	<div data-elementor-type="popup" data-elementor-id="4105" class="elementor elementor-4105 elementor-location-popup"
		data-elementor-settings="{&quot;entrance_animation&quot;:&quot;none&quot;,&quot;exit_animation&quot;:&quot;none&quot;,&quot;close_button_delay&quot;:15,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0.1000000000000000055511151231257827021181583404541015625,&quot;sizes&quot;:[]},&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
			<section
				class="elementor-section elementor-top-section elementor-element elementor-element-33a6eed elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="33a6eed" data-element_type="section"
				data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;98c109f&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;background_background&quot;:&quot;classic&quot;}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-04579b5"
							data-id="04579b5" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-d33e55b elementor-widget elementor-widget-heading"
										data-id="d33e55b" data-element_type="widget" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<h1 class="elementor-heading-title elementor-size-default">¡Así somos en
												Banco Capital!</h1>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section
				class="elementor-section elementor-top-section elementor-element elementor-element-e912a40 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
				data-id="e912a40" data-element_type="section"
				data-settings="{&quot;jet_parallax_layout_list&quot;:[{&quot;jet_parallax_layout_image&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;_id&quot;:&quot;edbd990&quot;,&quot;jet_parallax_layout_image_tablet&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_image_mobile&quot;:{&quot;url&quot;:&quot;&quot;,&quot;id&quot;:&quot;&quot;},&quot;jet_parallax_layout_speed&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:50,&quot;sizes&quot;:[]},&quot;jet_parallax_layout_type&quot;:&quot;scroll&quot;,&quot;jet_parallax_layout_direction&quot;:null,&quot;jet_parallax_layout_fx_direction&quot;:null,&quot;jet_parallax_layout_z_index&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x&quot;:50,&quot;jet_parallax_layout_bg_x_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_x_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y&quot;:50,&quot;jet_parallax_layout_bg_y_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_y_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size&quot;:&quot;auto&quot;,&quot;jet_parallax_layout_bg_size_tablet&quot;:&quot;&quot;,&quot;jet_parallax_layout_bg_size_mobile&quot;:&quot;&quot;,&quot;jet_parallax_layout_animation_prop&quot;:&quot;transform&quot;,&quot;jet_parallax_layout_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;]}],&quot;background_background&quot;:&quot;classic&quot;}">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-0b3eb61"
							data-id="0b3eb61" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-778bc74 elementor-aspect-ratio-169 elementor-widget elementor-widget-video"
										data-id="778bc74" data-element_type="widget"
										data-settings="{&quot;youtube_url&quot;:&quot;https:\/\/www.youtube.com\/watch?v=01N_A8mnEkg&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;controls&quot;:&quot;yes&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}"
										data-widget_type="video.default">
										<div class="elementor-widget-container">
											<div
												class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
												<div class="elementor-video"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!-- Facebook Pixel Event Code -->
	<script type='text/javascript'>
		document.addEventListener('wpcf7mailsent', function (event) {
			if ("fb_pxl_code" in event.detail.apiResponse) {
				eval(event.detail.apiResponse.fb_pxl_code);
			}
		}, false);
	</script>
	<!-- End Facebook Pixel Event Code -->
	<div id='fb-pxl-ajax-code'></div>
	<script type="text/html" id="tmpl-jet-ajax-search-results-item">
<div class="jet-ajax-search__results-item">
	<a class="jet-ajax-search__item-link" href="{{{data.link}}}" target="{{{data.link_target_attr}}}">
		{{{data.thumbnail}}}
		<div class="jet-ajax-search__item-content-wrapper">
			{{{data.before_title}}}
			<div class="jet-ajax-search__item-title">{{{data.title}}}</div>
			{{{data.after_title}}}
			{{{data.before_content}}}
			<div class="jet-ajax-search__item-content">{{{data.content}}}</div>
			{{{data.after_content}}}
			{{{data.rating}}}
			{{{data.price}}}
		</div>
	</a>
</div>
</script>
	<link data-minify="1" rel='stylesheet' id='elementor-post-4105-css'
		href='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/uploads/elementor/css/post-4105.css?ver=1698695832'
		media='all' />
	<link rel='stylesheet' id='e-animations-css'
		href='https://www.bancocapital.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.6'
		media='all' />
	<script id='rocket-browser-checker-js-after'>
		"use strict"; var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || !1, descriptor.configurable = !0, "value" in descriptor && (descriptor.writable = !0), Object.defineProperty(target, descriptor.key, descriptor) } } return function (Constructor, protoProps, staticProps) { return protoProps && defineProperties(Constructor.prototype, protoProps), staticProps && defineProperties(Constructor, staticProps), Constructor } }(); function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) throw new TypeError("Cannot call a class as a function") } var RocketBrowserCompatibilityChecker = function () { function RocketBrowserCompatibilityChecker(options) { _classCallCheck(this, RocketBrowserCompatibilityChecker), this.passiveSupported = !1, this._checkPassiveOption(this), this.options = !!this.passiveSupported && options } return _createClass(RocketBrowserCompatibilityChecker, [{ key: "_checkPassiveOption", value: function (self) { try { var options = { get passive() { return !(self.passiveSupported = !0) } }; window.addEventListener("test", null, options), window.removeEventListener("test", null, options) } catch (err) { self.passiveSupported = !1 } } }, { key: "initRequestIdleCallback", value: function () { !1 in window && (window.requestIdleCallback = function (cb) { var start = Date.now(); return setTimeout(function () { cb({ didTimeout: !1, timeRemaining: function () { return Math.max(0, 50 - (Date.now() - start)) } }) }, 1) }), !1 in window && (window.cancelIdleCallback = function (id) { return clearTimeout(id) }) } }, { key: "isDataSaverModeOn", value: function () { return "connection" in navigator && !0 === navigator.connection.saveData } }, { key: "supportsLinkPrefetch", value: function () { var elem = document.createElement("link"); return elem.relList && elem.relList.supports && elem.relList.supports("prefetch") && window.IntersectionObserver && "isIntersecting" in IntersectionObserverEntry.prototype } }, { key: "isSlowConnection", value: function () { return "connection" in navigator && "effectiveType" in navigator.connection && ("2g" === navigator.connection.effectiveType || "slow-2g" === navigator.connection.effectiveType) } }]), RocketBrowserCompatibilityChecker }();
	</script>
	<script id='rocket-delay-js-js-after'>
		(function () {
			"use strict"; var e = function () { function n(e, t) { for (var r = 0; r < t.length; r++) { var n = t[r]; n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n) } } return function (e, t, r) { return t && n(e.prototype, t), r && n(e, r), e } }(); function n(e, t) { if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function") } var t = function () { function r(e, t) { n(this, r), this.attrName = "data-rocketlazyloadscript", this.browser = t, this.options = this.browser.options, this.triggerEvents = e, this.userEventListener = this.triggerListener.bind(this) } return e(r, [{ key: "init", value: function () { this._addEventListener(this) } }, { key: "reset", value: function () { this._removeEventListener(this) } }, { key: "_addEventListener", value: function (t) { this.triggerEvents.forEach(function (e) { return window.addEventListener(e, t.userEventListener, t.options) }) } }, { key: "_removeEventListener", value: function (t) { this.triggerEvents.forEach(function (e) { return window.removeEventListener(e, t.userEventListener, t.options) }) } }, { key: "_loadScriptSrc", value: function () { var r = this, e = document.querySelectorAll("script[" + this.attrName + "]"); 0 !== e.length && Array.prototype.slice.call(e).forEach(function (e) { var t = e.getAttribute(r.attrName); e.setAttribute("src", t), e.removeAttribute(r.attrName) }), this.reset() } }, { key: "triggerListener", value: function () { this._loadScriptSrc(), this._removeEventListener(this) } }], [{ key: "run", value: function () { RocketBrowserCompatibilityChecker && new r(["keydown", "mouseover", "touchmove", "touchstart", "wheel"], new RocketBrowserCompatibilityChecker({ passive: !0 })).init() } }]), r }(); t.run();
		}());
	</script>
	<script id='rocket-preload-links-js-extra'>
		var RocketPreloadLinksConfig = { "excludeUris": "\/(.+\/)?feed\/?.+\/?|\/(?:.+\/)?embed\/|\/(index\\.php\/)?wp\\-json(\/.*|$)|\/wp-admin\/|\/logout\/|\/wp-login.php", "usesTrailingSlash": "1", "imageExt": "jpg|jpeg|gif|png|tiff|bmp|webp|avif", "fileExt": "jpg|jpeg|gif|png|tiff|bmp|webp|avif|php|pdf|html|htm", "siteUrl": "https:\/\/www.bancocapital.com", "onHoverDelay": "100", "rateThrottle": "3" };
	</script>
	<script id='rocket-preload-links-js-after'>
		(function () {
			"use strict"; var r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) { return typeof e } : function (e) { return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e }, e = function () { function i(e, t) { for (var n = 0; n < t.length; n++) { var i = t[n]; i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i) } } return function (e, t, n) { return t && i(e.prototype, t), n && i(e, n), e } }(); function i(e, t) { if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function") } var t = function () { function n(e, t) { i(this, n), this.browser = e, this.config = t, this.options = this.browser.options, this.prefetched = new Set, this.eventTime = null, this.threshold = 1111, this.numOnHover = 0 } return e(n, [{ key: "init", value: function () { !this.browser.supportsLinkPrefetch() || this.browser.isDataSaverModeOn() || this.browser.isSlowConnection() || (this.regex = { excludeUris: RegExp(this.config.excludeUris, "i"), images: RegExp(".(" + this.config.imageExt + ")$", "i"), fileExt: RegExp(".(" + this.config.fileExt + ")$", "i") }, this._initListeners(this)) } }, { key: "_initListeners", value: function (e) { -1 < this.config.onHoverDelay && document.addEventListener("mouseover", e.listener.bind(e), e.listenerOptions), document.addEventListener("mousedown", e.listener.bind(e), e.listenerOptions), document.addEventListener("touchstart", e.listener.bind(e), e.listenerOptions) } }, { key: "listener", value: function (e) { var t = e.target.closest("a"), n = this._prepareUrl(t); if (null !== n) switch (e.type) { case "mousedown": case "touchstart": this._addPrefetchLink(n); break; case "mouseover": this._earlyPrefetch(t, n, "mouseout") } } }, { key: "_earlyPrefetch", value: function (t, e, n) { var i = this, r = setTimeout(function () { if (r = null, 0 === i.numOnHover) setTimeout(function () { return i.numOnHover = 0 }, 1e3); else if (i.numOnHover > i.config.rateThrottle) return; i.numOnHover++, i._addPrefetchLink(e) }, this.config.onHoverDelay); t.addEventListener(n, function e() { t.removeEventListener(n, e, { passive: !0 }), null !== r && (clearTimeout(r), r = null) }, { passive: !0 }) } }, { key: "_addPrefetchLink", value: function (i) { return this.prefetched.add(i.href), new Promise(function (e, t) { var n = document.createElement("link"); n.rel = "prefetch", n.href = i.href, n.onload = e, n.onerror = t, document.head.appendChild(n) }).catch(function () { }) } }, { key: "_prepareUrl", value: function (e) { if (null === e || "object" !== (void 0 === e ? "undefined" : r(e)) || !1 in e || -1 === ["http:", "https:"].indexOf(e.protocol)) return null; var t = e.href.substring(0, this.config.siteUrl.length), n = this._getPathname(e.href, t), i = { original: e.href, protocol: e.protocol, origin: t, pathname: n, href: t + n }; return this._isLinkOk(i) ? i : null } }, { key: "_getPathname", value: function (e, t) { var n = t ? e.substring(this.config.siteUrl.length) : e; return n.startsWith("/") || (n = "/" + n), this._shouldAddTrailingSlash(n) ? n + "/" : n } }, { key: "_shouldAddTrailingSlash", value: function (e) { return this.config.usesTrailingSlash && !e.endsWith("/") && !this.regex.fileExt.test(e) } }, { key: "_isLinkOk", value: function (e) { return null !== e && "object" === (void 0 === e ? "undefined" : r(e)) && (!this.prefetched.has(e.href) && e.origin === this.config.siteUrl && -1 === e.href.indexOf("?") && -1 === e.href.indexOf("#") && !this.regex.excludeUris.test(e.href) && !this.regex.images.test(e.href)) } }], [{ key: "run", value: function () { "undefined" != typeof RocketPreloadLinksConfig && new n(new RocketBrowserCompatibilityChecker({ capture: !0, passive: !0 }), RocketPreloadLinksConfig).init() } }]), n }(); t.run();
		}());
	</script>
	<script src='https://www.bancocapital.com/wp-content/plugins/jet-menu/assets/public/lib/vue/vue.min.js?ver=2.6.11'
		id='jet-vue-js'></script>
	<script id='jet-menu-public-scripts-js-extra'>
		var jetMenuPublicSettings = { "version": "2.1.7", "ajaxUrl": "https:\/\/www.bancocapital.com\/wp-admin\/admin-ajax.php", "isMobile": "false", "templateApiUrl": "https:\/\/www.bancocapital.com\/wp-json\/jet-menu-api\/v1\/elementor-template", "menuItemsApiUrl": "https:\/\/www.bancocapital.com\/wp-json\/jet-menu-api\/v1\/get-menu-items", "restNonce": "e6d88886a5", "devMode": "false", "wpmlLanguageCode": "", "menuSettings": { "jetMenuRollUp": "true", "jetMenuMouseleaveDelay": 500, "jetMenuMegaWidthType": "container", "jetMenuMegaWidthSelector": "", "jetMenuMegaOpenSubType": "hover", "jetMenuMegaAjax": "false" } };
	</script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-menu/assets/public/js/legacy/jet-menu-public-scripts.js?ver=1651677306'
		id='jet-menu-public-scripts-js'></script>
	<script id='jet-menu-public-scripts-js-after'>
		function CxCSSCollector() { "use strict"; var t, e = window.CxCollectedCSS; void 0 !== e && ((t = document.createElement("style")).setAttribute("title", e.title), t.setAttribute("type", e.type), t.textContent = e.css, document.head.appendChild(t)) } CxCSSCollector();
	</script>
	<script src='https://www.bancocapital.com/wp-includes/js/underscore.min.js?ver=1.13.3' id='underscore-js'></script>
	<script id='wp-util-js-extra'>
		var _wpUtilSettings = { "ajax": { "url": "\/wp-admin\/admin-ajax.php" } };
	</script>
	<script src='https://www.bancocapital.com/wp-includes/js/wp-util.min.js?ver=6.0.7' id='wp-util-js'></script>
	<script src='https://www.bancocapital.com/wp-includes/js/imagesloaded.min.js?ver=6.0.7'
		id='imagesLoaded-js'></script>
	<script id='jet-search-js-extra'>
		var jetSearchSettings = { "ajaxurl": "https:\/\/www.bancocapital.com\/wp-admin\/admin-ajax.php", "action": "jet_ajax_search", "nonce": "52e810fbf3" };
	</script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-search/assets/js/jet-search.js?ver=1651677306'
		id='jet-search-js'></script>
	<script
		src='https://www.bancocapital.com/wp-content/plugins/elementor-pro/assets/lib/smartmenus/jquery.smartmenus.min.js?ver=1.0.1'
		id='smartmenus-js'></script>
	<script src='https://www.bancocapital.com/wp-includes/js/imagesloaded.min.js?ver=4.1.4'
		id='imagesloaded-js'></script>
	<script
		src='https://www.bancocapital.com/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.7.2'
		id='elementor-pro-webpack-runtime-js'></script>
	<script src='https://www.bancocapital.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.6'
		id='elementor-webpack-runtime-js'></script>
	<script src='https://www.bancocapital.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.6'
		id='elementor-frontend-modules-js'></script>
	<script src='https://www.bancocapital.com/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9'
		id='regenerator-runtime-js'></script>
	<script src='https://www.bancocapital.com/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0'
		id='wp-polyfill-js'></script>
	<script src='https://www.bancocapital.com/wp-includes/js/dist/hooks.min.js?ver=c6d64f2cb8f5c6bb49caca37f8828ce3'
		id='wp-hooks-js'></script>
	<script src='https://www.bancocapital.com/wp-includes/js/dist/i18n.min.js?ver=ebee46757c6a411e38fd079a7ac71d94'
		id='wp-i18n-js'></script>
	<script id='wp-i18n-js-after'>
		wp.i18n.setLocaleData({ 'text direction\u0004ltr': ['ltr'] });
	</script>
	<script id='elementor-pro-frontend-js-before'>
		var ElementorProFrontendConfig = { "ajaxurl": "https:\/\/www.bancocapital.com\/wp-admin\/admin-ajax.php", "nonce": "c62e224645", "urls": { "assets": "https:\/\/www.bancocapital.com\/wp-content\/plugins\/elementor-pro\/assets\/", "rest": "https:\/\/www.bancocapital.com\/wp-json\/" }, "shareButtonsNetworks": { "facebook": { "title": "Facebook", "has_counter": true }, "twitter": { "title": "Twitter" }, "linkedin": { "title": "LinkedIn", "has_counter": true }, "pinterest": { "title": "Pinterest", "has_counter": true }, "reddit": { "title": "Reddit", "has_counter": true }, "vk": { "title": "VK", "has_counter": true }, "odnoklassniki": { "title": "OK", "has_counter": true }, "tumblr": { "title": "Tumblr" }, "digg": { "title": "Digg" }, "skype": { "title": "Skype" }, "stumbleupon": { "title": "StumbleUpon", "has_counter": true }, "mix": { "title": "Mix" }, "telegram": { "title": "Telegram" }, "pocket": { "title": "Pocket", "has_counter": true }, "xing": { "title": "XING", "has_counter": true }, "whatsapp": { "title": "WhatsApp" }, "email": { "title": "Email" }, "print": { "title": "Print" } }, "facebook_sdk": { "lang": "es_ES", "app_id": "" }, "lottie": { "defaultAnimationUrl": "https:\/\/www.bancocapital.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json" } };
	</script>
	<script src='https://www.bancocapital.com/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.7.2'
		id='elementor-pro-frontend-js'></script>
	<script
		src='https://www.bancocapital.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'
		id='elementor-waypoints-js'></script>
	<script src='https://www.bancocapital.com/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1'
		id='jquery-ui-core-js'></script>
	<script src='https://www.bancocapital.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6'
		id='swiper-js'></script>
	<script
		src='https://www.bancocapital.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.6'
		id='share-link-js'></script>
	<script src='https://www.bancocapital.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0'
		id='elementor-dialog-js'></script>
	<script id='elementor-frontend-js-before'>
		var elementorFrontendConfig = { "environmentMode": { "edit": false, "wpPreview": false, "isScriptDebug": false }, "i18n": { "shareOnFacebook": "Compartir en Facebook", "shareOnTwitter": "Compartir en Twitter", "pinIt": "Pinear", "download": "Descargar", "downloadImage": "Descargar imagen", "fullscreen": "Pantalla completa", "zoom": "Zoom", "share": "Compartir", "playVideo": "Reproducir v\u00eddeo", "previous": "Anterior", "next": "Siguiente", "close": "Cerrar" }, "is_rtl": false, "breakpoints": { "xs": 0, "sm": 480, "md": 768, "lg": 1025, "xl": 1440, "xxl": 1600 }, "responsive": { "breakpoints": { "mobile": { "label": "M\u00f3vil", "value": 767, "default_value": 767, "direction": "max", "is_enabled": true }, "mobile_extra": { "label": "M\u00f3vil grande", "value": 880, "default_value": 880, "direction": "max", "is_enabled": false }, "tablet": { "label": "Tableta", "value": 1024, "default_value": 1024, "direction": "max", "is_enabled": true }, "tablet_extra": { "label": "Tableta grande", "value": 1200, "default_value": 1200, "direction": "max", "is_enabled": false }, "laptop": { "label": "Port\u00e1til", "value": 1366, "default_value": 1366, "direction": "max", "is_enabled": false }, "widescreen": { "label": "Pantalla grande", "value": 2400, "default_value": 2400, "direction": "min", "is_enabled": false } } }, "version": "3.6.6", "is_static": false, "experimentalFeatures": { "e_import_export": true, "e_hidden_wordpress_widgets": true, "theme_builder_v2": true, "landing-pages": true, "elements-color-picker": true, "favorite-widgets": true, "admin-top-bar": true, "page-transitions": true, "notes": true, "form-submissions": true, "e_scroll_snap": true }, "urls": { "assets": "https:\/\/www.bancocapital.com\/wp-content\/plugins\/elementor\/assets\/" }, "settings": { "page": [], "editorPreferences": [] }, "kit": { "active_breakpoints": ["viewport_mobile", "viewport_tablet"], "global_image_lightbox": "yes", "lightbox_enable_counter": "yes", "lightbox_enable_fullscreen": "yes", "lightbox_enable_zoom": "yes", "lightbox_enable_share": "yes", "lightbox_title_src": "title", "lightbox_description_src": "description" }, "post": { "id": 48, "title": "Banco%20Capital%20%E2%80%93%20Somos%20Parte%20de%20tu%20Familia", "excerpt": "", "featuredImage": false } };
	</script>
	<script src='https://www.bancocapital.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.6'
		id='elementor-frontend-js'></script>
	<script
		src='https://www.bancocapital.com/wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.7.2'
		id='pro-preloaded-elements-handlers-js'></script>
	<script id='jet-blocks-js-extra'>
		var JetHamburgerPanelSettings = { "ajaxurl": "https:\/\/www.bancocapital.com\/wp-admin\/admin-ajax.php", "isMobile": "false", "templateApiUrl": "https:\/\/www.bancocapital.com\/wp-json\/jet-blocks-api\/v1\/elementor-template", "devMode": "false" };
	</script>
	<script src='https://www.bancocapital.com/wp-content/plugins/jet-blocks/assets/js/jet-blocks.min.js?ver=1.3.2'
		id='jet-blocks-js'></script>
	<script id='jet-elements-js-extra'>
		var jetElements = { "ajaxUrl": "https:\/\/www.bancocapital.com\/wp-admin\/admin-ajax.php", "isMobile": "false", "templateApiUrl": "https:\/\/www.bancocapital.com\/wp-json\/jet-elements-api\/v1\/elementor-template", "devMode": "false", "messages": { "invalidMail": "Please specify a valid e-mail" } };
	</script>
	<script src='https://www.bancocapital.com/wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.6'
		id='jet-elements-js'></script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-menu/includes/elementor/assets/public/js/legacy/widgets-scripts.js?ver=1651677306'
		id='jet-menu-elementor-widgets-scripts-js'></script>
	<script id='jet-tabs-frontend-js-extra'>
		var JetTabsSettings = { "ajaxurl": "https:\/\/www.bancocapital.com\/wp-admin\/admin-ajax.php", "isMobile": "false", "templateApiUrl": "https:\/\/www.bancocapital.com\/wp-json\/jet-tabs-api\/v1\/elementor-template", "devMode": "false" };
	</script>
	<script src='https://www.bancocapital.com/wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17'
		id='jet-tabs-frontend-js'></script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-tricks/assets/js/lib/tippy/popperjs.js?ver=1651677306'
		id='jet-tricks-popperjs-js'></script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-tricks/assets/js/lib/tippy/tippy-bundle.js?ver=1651677306'
		id='jet-tricks-tippy-bundle-js'></script>
	<script id='jet-tricks-frontend-js-extra'>
		var JetTricksSettings = { "elements_data": { "sections": { "460dade": { "view_more": false, "particles": "false", "particles_json": null }, "7b06ba1e": { "view_more": false, "particles": "false", "particles_json": null }, "7e30951e": { "view_more": false, "particles": "false", "particles_json": null }, "e0eaa1d": { "view_more": false, "particles": "false", "particles_json": null }, "2d51cdfe": { "view_more": false, "particles": "false", "particles_json": null }, "fd44877": { "view_more": false, "particles": "false", "particles_json": null }, "90e41f6": { "view_more": false, "particles": "false", "particles_json": null }, "785c974": { "view_more": false, "particles": "false", "particles_json": null }, "39b3d31": { "view_more": false, "particles": "false", "particles_json": null }, "76843a5": { "view_more": false, "particles": "false", "particles_json": null }, "f08f278": { "view_more": false, "particles": "false", "particles_json": null }, "91f275b": { "view_more": false, "particles": "false", "particles_json": null }, "0ed2367": { "view_more": false, "particles": "false", "particles_json": null }, "b4e5e30": { "view_more": false, "particles": "false", "particles_json": null }, "b54e767": { "view_more": false, "particles": "false", "particles_json": null }, "760d614": { "view_more": false, "particles": "false", "particles_json": null }, "64a3aed": { "view_more": false, "particles": "false", "particles_json": null }, "b3de484": { "view_more": false, "particles": "false", "particles_json": null }, "cdfd813": { "view_more": false, "particles": "false", "particles_json": null }, "534aaba": { "view_more": false, "particles": "false", "particles_json": null }, "d015fa1": { "view_more": false, "particles": "false", "particles_json": null }, "9fbc56e": { "view_more": false, "particles": "false", "particles_json": null }, "0896877": { "view_more": false, "particles": "false", "particles_json": null }, "6d9fff8": { "view_more": false, "particles": "false", "particles_json": null }, "33a6eed": { "view_more": false, "particles": "false", "particles_json": null }, "e912a40": { "view_more": false, "particles": "false", "particles_json": null } }, "columns": [], "widgets": { "a6a3245": [], "3e5570b": [], "1cfe5a5": [], "21cb640d": [], "ff847d7": [], "af97723": [], "6ffbfbf9": [], "44c4fd28": [], "28216b1b": [], "70a5eed": [], "7a171": [], "2720834": [], "0f842ad": [], "3512e44": [], "f1e6301": [], "491211a": [], "df82826": [], "1c514d1": [], "c4101ed": [], "de7ec83": [], "5b30432": [], "ccd42d0": [], "fdd87ed": [], "4819ecb": [], "aeb4c13": [], "08ebc76": [], "da48058": [], "5541df0": [], "2d13094": [], "dd0898a": [], "db9e20d": [], "45d5621": [], "f945199": [], "263c4fe": [], "613349b": [], "37d9bff": [], "c369acb": [], "09fe99f": [], "cc57233": [], "1cbc6fa": [], "515bfee": [], "8b3d2bc": [], "ecd5b06": [], "994019d": [], "44eb6c4": [], "e9cd0ca": [], "e3ebde6": [], "e4a7796": [], "5d3141c": [], "bf61d00": [], "440259c": [], "6216a90": [], "25be139": [], "e2f3e89": [], "c072200": [], "0b7f355": [], "26b0e40": [], "99d6e79": [], "c4d5b65": [], "e162997": [], "a2409fa": [], "66ba288": [], "44998de": [], "951bb6f": [], "2d108bf": [], "868cd5c": [], "68dfc09": [], "c543662": [], "830e7cf": [], "a1ac416": [], "78bb4c1": [], "a2a96c2": [], "c98a40a": [], "087a5aa": [], "0f66559": [], "82533c8": [], "c8f369b": [], "2a2160e": [], "0994f85": [], "9287bb2": [], "43827aa": [], "d33e55b": [], "778bc74": [] } } };
	</script>
	<script data-minify="1"
		src='https://www.bancocapital.com/wp-content/cache/min/1/wp-content/plugins/jet-tricks/assets/js/jet-tricks-frontend.js?ver=1651677306'
		id='jet-tricks-frontend-js'></script>
	<script src='https://www.bancocapital.com/wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.6'
		id='preloaded-modules-js'></script>
	<script
		src='https://www.bancocapital.com/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.7.2'
		id='e-sticky-js'></script>

</body>

</html>