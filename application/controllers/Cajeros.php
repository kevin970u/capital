<?php

class Cajeros extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Cajero");
        $this->load->model("Agencia");
    }

    public function index()
    {
        $data["cajeros"] = $this->Cajero->obtenerTodos();
        $data["agencias"] = $this->Agencia->obtenerTodos();

        $this->load->view("header");
        $this->load->view('cajeros/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $data["agencias"] = $this->Agencia->obtenerTodos();
        $this->load->view("header");
        $this->load->view('cajeros/nuevo', $data);
        $this->load->view('footer');
    }

    public function editar($id)
    {
        $data["cajeroEditar"] = $this->Cajero->obtenerPorId($id);
        $data["agencias"] = $this->Agencia->obtenerTodos();

        $this->load->view("header");
        $this->load->view('cajeros/editar', $data);
        $this->load->view('footer');
    }

    public function mapa() {
        $data["cajeros"] = $this->Cajero->obtenerTodos();

        $this->load->view("header");
        $this->load->view('cajeros/mapa', $data);
        $this->load->view('footer');
    }

    public function eliminar($id)
    {
        $this->Cajero->eliminar($id);
        $this->session->set_flashdata('mensaje', 'El cajero fue eliminado correctamente');
        redirect('cajeros/index');
    }

    public function guardarCajero()
    {
        /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
        $config['upload_path'] = APPPATH . '../uploads/cajeros/'; //ruta de subida de archivos
        $config['allowed_types'] = 'jpeg|jpg|png'; //tipo de archivos permitidos
        $config['max_size'] = 5 * 1024; //definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "cajero_" . time() * rand(100, 10000); //creando un nombre aleatorio
        $config['file_name'] = $nombre_aleatorio; //asignando el nombre al archivo subido
        $this->load->library('upload', $config); //cargando la libreria UPLOAD
        if ($this->upload->do_upload("imagen_caj")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data(); //capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"]; //obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = ""; //Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosNuevoCajero = array(
            "tipo_caj" => $this->input->post("tipo_caj"),
            "modelo_caj" => $this->input->post("modelo_caj"),
            "capacidad_caj" => $this->input->post("capacidad_caj"),
            "provincia_caj" => $this->input->post("provincia_caj"),
            "ciudad_caj" => $this->input->post("ciudad_caj"),
            "direccion_caj" => $this->input->post("direccion_caj"),
            "fecha_apertura_caj" => $this->input->post("fecha_apertura_caj"),
            "latitud_caj" => $this->input->post("latitud_caj"),
            "longitud_caj" => $this->input->post("longitud_caj"),
            #foreing key agencia
            "id_age" => $this->input->post("id_age"),
            "imagen_caj" => $nombre_archivo_subido
        );

        $this->Cajero->insertar($datosNuevoCajero);
        $this->session->set_flashdata('mensaje', 'El cajero fue registrado correctamente');
        redirect("cajeros/index");
    }

    public function actualizarCajero()
    {
        $id_caj = $this->input->post("id_caj");

        $cajeroEditar = $this->Cajero->obtenerPorId($id_caj);

        // Configuración de carga de archivos
        $config['upload_path'] = APPPATH . '../uploads/cajeros/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 5000; // 5MB

        if (!empty($_FILES['imagen_caj']['name'])) {
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('imagen_caj')) {
                $dataArchivoSubido = $this->upload->data();
                $nombre_archivo_subido = $dataArchivoSubido['file_name'];

                // Eliminar la imagen anterior si existe
                if (!empty($cajeroEditar->imagen_caj)) {
                    unlink($config['upload_path'] . $cajeroEditar->imagen_caj);
                }
            } else {
                // Manejar errores de carga de archivo según sea necesario
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                return;
            }
        } else {
            // Si no se está cargando una nueva imagen, mantener la imagen existente
            $nombre_archivo_subido = $cajeroEditar->imagen_caj;
        }

        // Datos del cajero para actualizar
        $datosCajero = array(
            "tipo_caj" => $this->input->post("tipo_caj"),
            "modelo_caj" => $this->input->post("modelo_caj"),
            "capacidad_caj" => $this->input->post("capacidad_caj"),
            "provincia_caj" => $this->input->post("provincia_caj"),
            "ciudad_caj" => $this->input->post("ciudad_caj"),
            "direccion_caj" => $this->input->post("direccion_caj"),
            "fecha_apertura_caj" => $this->input->post("fecha_apertura_caj"),
            "latitud_caj" => $this->input->post("latitud_caj"),
            "longitud_caj" => $this->input->post("longitud_caj"),
            #foreing key agencia
            "id_age" => $this->input->post("id_age"),
            "imagen_caj" => $nombre_archivo_subido
        );

        // Actualizar el Cajero
        $this->Cajero->actualizar($id_caj, $datosCajero);
        $this->session->set_flashdata('mensaje', 'El cajero fue actualizado correctamente');
        redirect("cajeros/index");
    }
}
