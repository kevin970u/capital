<?php

class Corresponsales extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Corresponsal");
        $this->load->model("Agencia");
    }

    public function index()
    {
        $data["corresponsales"] = $this->Corresponsal->obtenerTodos();
         $data["agencias"] = $this->Agencia->obtenerTodos();

        $this->load->view("header");
        $this->load->view('corresponsales/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {

         $data["agencias"] = $this->Agencia->obtenerTodos();

        $this->load->view("header");
        $this->load->view('corresponsales/nuevo', $data);
        $this->load->view('footer');
    }

    public function editar($id)
    {
        $data["corresponsalEditar"] = $this->Corresponsal->obtenerPorId($id);
        $data["agencias"] = $this->Agencia->obtenerTodos();

        $this->load->view("header");
        $this->load->view('corresponsales/editar', $data);
        $this->load->view('footer');
    }

    public function mapa() {
        $data["corresponsales"] = $this->Corresponsal->obtenerTodos();

        $this->load->view("header");
        $this->load->view('corresponsales/mapa', $data);
        $this->load->view('footer');
    }

    public function eliminar($id)
    {
        $this->Corresponsal->eliminar($id);
        $this->session->set_flashdata('mensaje', 'El corresponsalfue eliminado correctamente');
        redirect('corresponsales/index');
    }

    public function guardarCorresponsal()
    {
        /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
        $config['upload_path'] = APPPATH . '../uploads/corresponsales/'; //ruta de subida de archivos
        $config['allowed_types'] = 'pdf'; //tipo de archivos permitidos
        $config['max_size'] = 5 * 1024; //definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "corresponsal" . time() * rand(100, 10000); //creando un nombre aleatorio
        $config['file_name'] = $nombre_aleatorio; //asignando el nombre al archivo subido
        $this->load->library('upload', $config); //cargando la libreria UPLOAD
        if ($this->upload->do_upload("contrato_cor")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data(); //capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"]; //obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = ""; //Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosNuevoCorresponsal = array(
            "tipo_negocio_cor" => $this->input->post("tipo_negocio_cor"),
            "propietario_cor" => $this->input->post("propietario_cor"),
            "fecha_apertura_cor" => $this->input->post("fecha_apertura_cor"),
            "telefono_cor" => $this->input->post("telefono_cor"),
            "provincia_cor" => $this->input->post("provincia_cor"),
            "ciudad_cor" => $this->input->post("ciudad_cor"),
            "direccion_cor" => $this->input->post("direccion_cor"),
            "latitud_cor" => $this->input->post("latitud_cor"),
            "longitud_cor" => $this->input->post("longitud_cor"),
            #foreing key agencia
            "id_age" => $this->input->post("id_age"),
            "contrato_cor" => $nombre_archivo_subido
        );

        $this->Corresponsal->insertar($datosNuevoCorresponsal);
        $this->session->set_flashdata('mensaje', 'El corresponsal fue registrado correctamente');
        redirect("corresponsales/index");
    }

    public function actualizarCorresponsal()
    {
        $id_cor = $this->input->post("id_cor");

        $corresponsalEditar = $this->Corresponsal->obtenerPorId($id_cor);

        // Configuración de carga de archivos
        $config['upload_path'] = APPPATH . '../uploads/corresponsales/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 5000; // 5MB

        if (!empty($_FILES['contrato_cor']['name'])) {
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('contrato_cor')) {
                $dataArchivoSubido = $this->upload->data();
                $nombre_archivo_subido = $dataArchivoSubido['file_name'];

                // Eliminar la imagen anterior si existe
                if (!empty($corresponsalEditar->contrato_cor)) {
                    unlink($config['upload_path'] . $corresponsalEditar->contrato_cor);
                }
            } else {
                // Manejar errores de carga de archivo según sea necesario
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                return;
            }
        } else {
            // Si no se está cargando una nueva imagen, mantener la imagen existente
            $nombre_archivo_subido = $corresponsalEditar->contrato_cor;
        }

        // Datos del corresponsal para actualizar
        $datosCorresponsal = array(
            "tipo_negocio_cor" => $this->input->post("tipo_negocio_cor"),
            "propietario_cor" => $this->input->post("propietario_cor"),
            "fecha_apertura_cor" => $this->input->post("fecha_apertura_cor"),
            "telefono_cor" => $this->input->post("telefono_cor"),
            "provincia_cor" => $this->input->post("provincia_cor"),
            "ciudad_cor" => $this->input->post("ciudad_cor"),
            "direccion_cor" => $this->input->post("direccion_cor"),
            "latitud_cor" => $this->input->post("latitud_cor"),
            "longitud_cor" => $this->input->post("longitud_cor"),
            #foreing key agencia
            "id_age" => $this->input->post("id_age"),
            "contrato_cor" => $nombre_archivo_subido
        );

        // Actualizar el Corresponsal
        $this->Corresponsal->actualizar($id_cor, $datosCorresponsal);
        $this->session->set_flashdata('mensaje', 'El corresponsal fue actualizado correctamente');
        redirect("corresponsales/index");
    }
}
