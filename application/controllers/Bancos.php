<?php

class Bancos extends CI_Controller
{

     function __construct()
    {
        parent::__construct();
        $this->load->model("Agencia");
        $this->load->model("Cajero");
        $this->load->model("Corresponsal");
        $this->load->model("Banco");
    }

    public function index()
    {

        $data["agencias"] = $this->Agencia->obtenerTodos();
        $data["cajeros"] = $this->Cajero->obtenerTodos();
        $data["corresponsales"] = $this->Corresponsal->obtenerTodos();
        $data["bancos"] = $this->Banco->obtenerTodos();


        $this->load->view("header");
        $this->load->view('bancos/index', $data);
        $this->load->view('footer');
    }

    public function institucion() {

        $data["banco"] = $this->Banco->obtenerPorId(1);

        $this->load->view("header");
        $this->load->view('bancos/institucion', $data);
        $this->load->view('footer');
    }

    public function editar()
    {
        $data["banco"] = $this->Banco->obtenerPorId(1);

        $this->load->view("header");
        $this->load->view('bancos/editar', $data);
        $this->load->view('footer');
    }

   public function actualizarBanco()
    {
        $id_ban = $this->input->post("id_ban");

        $bancoEditar = $this->Banco->obtenerPorId($id_ban);

        // Configuración de carga de archivos
        $config['upload_path'] = APPPATH . '../uploads/bancos/';
        $config['allowed_types'] = 'png';
        $config['max_size'] = 5000; // 5MB

        // Verificar si se está cargando un nuevo archivo
        if (!empty($_FILES['logo_ban']['name'])) {
            $this->load->library('upload', $config);

            // Obtener el nombre del archivo
            $nombre_archivo = $_FILES['logo_ban']['name'];

            // Verificar si ya existe un archivo con el mismo nombre
            if (file_exists($config['upload_path'] . $nombre_archivo)) {
                // Eliminar el archivo existente
                unlink($config['upload_path'] . $nombre_archivo);
            }

            // Subir el nuevo archivo
            if ($this->upload->do_upload('logo_ban')) {
                $dataArchivoSubido = $this->upload->data();
                $nombre_archivo_subido = $dataArchivoSubido['file_name'];
            } else {
                // Manejar errores de carga de archivo según sea necesario
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                return;
            }
        } else {
            // Si no se está cargando un nuevo archivo, mantener el nombre existente
            $nombre_archivo_subido = $bancoEditar->logo_ban;
        }

        // Datos del banco para actualizar
        $datosBanco = array(
            "nombre_ban" => $this->input->post("nombre_ban"),
            "provincia_ban" => $this->input->post("provincia_ban"),
            "ciudad_ban" => $this->input->post("ciudad_ban"),
            "direccion_ban" => $this->input->post("direccion_ban"),
            "vision_ban" => $this->input->post("vision_ban"),
            "mision_ban" => $this->input->post("mision_ban"),
            "telefono_ban" => $this->input->post("telefono_ban"),
            "email_ban" => $this->input->post("email_ban"),
            "fecha_apertura_ban" => $this->input->post("fecha_apertura_ban"),
            "latitud_ban" => $this->input->post("latitud_ban"),
            "longitud_ban" => $this->input->post("longitud_ban"),
            "logo_ban" => $nombre_archivo_subido
        );

        // Actualizar el Banco
        $this->Banco->actualizar($id_ban, $datosBanco);
        $this->session->set_flashdata('mensaje', 'Los datos del Banco fue actualizado correctamente');
        redirect("bancos/institucion");
    }

}
