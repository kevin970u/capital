<?php

class Agencias extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Agencia");
    }

    public function index()
    {
        $data["agencias"] = $this->Agencia->obtenerTodos();

        $this->load->view("header");
        $this->load->view('agencias/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view('agencias/nuevo');
        $this->load->view('footer');
    }

    public function editar($id)
    {
        $data["agenciaEditar"] = $this->Agencia->obtenerPorId($id);

        $this->load->view("header");
        $this->load->view('agencias/editar', $data);
        $this->load->view('footer');
    }

    public function mapa() {
        $data["agencias"] = $this->Agencia->obtenerTodos();

        $this->load->view("header");
        $this->load->view('agencias/mapa', $data);
        $this->load->view('footer');
    }

    public function eliminar($id)
    {

        try {
            $this->Agencia->eliminar($id);
            $this->session->set_flashdata('mensaje', 'La agencia fue eliminada correctamente');

            redirect('agencias/index');
        } catch (\Throwable $th) {
            var_dump($th);
            $this->session->set_flashdata('error', 'No se puede eliminar la agencia');
            redirect('agencias/index');
        }


    }

    public function guardarAgencia()
    {
        /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
        $config['upload_path'] = APPPATH . '../uploads/agencias/'; //ruta de subida de archivos
        $config['allowed_types'] = 'jpeg|jpg|png'; //tipo de archivos permitidos
        $config['max_size'] = 5 * 1024; //definir el peso maximo de subida (5MB)
        $nombre_aleatorio = "agencia_" . time() * rand(100, 10000); //creando un nombre aleatorio
        $config['file_name'] = $nombre_aleatorio; //asignando el nombre al archivo subido
        $this->load->library('upload', $config); //cargando la libreria UPLOAD
        if ($this->upload->do_upload("imagen_age")) { //intentando subir el archivo
            $dataArchivoSubido = $this->upload->data(); //capturando informacion del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"]; //obteniendo el nombre del archivo
        } else {
            $nombre_archivo_subido = ""; //Cuando no se sube el archivo el nombre queda VACIO
        }

        $datosNuevaAgencia = array(
            "gerente_age" => $this->input->post("gerente_age"),
            "fecha_apertura_age" => $this->input->post("fecha_apertura_age"),
            "telefono_age" => $this->input->post("telefono_age"),
            "email_age" => $this->input->post("email_age"),
            "provincia_age" => $this->input->post("provincia_age"),
            "ciudad_age" => $this->input->post("ciudad_age"),
            "direccion_age" => $this->input->post("direccion_age"),
            "latitud_age" => $this->input->post("latitud_age"),
            "longitud_age" => $this->input->post("longitud_age"),
            "imagen_age" => $nombre_archivo_subido
        );

        $this->Agencia->insertar($datosNuevaAgencia);
        $this->session->set_flashdata('mensaje', 'La agencia fue registrada correctamente');
        redirect("agencias/index");
    }

    public function actualizarAgencia()
    {
        $id_age = $this->input->post("id_age");

        $agenciaEditar = $this->Agencia->obtenerPorId($id_age);

        // Configuración de carga de archivos
        $config['upload_path'] = APPPATH . '../uploads/agencias/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 5000; // 5MB

        if (!empty($_FILES['imagen_age']['name'])) {
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('imagen_age')) {
                $dataArchivoSubido = $this->upload->data();
                $nombre_archivo_subido = $dataArchivoSubido['file_name'];

                // Eliminar la imagen anterior si existe
                if (!empty($agenciaEditar->imagen_age)) {
                    unlink($config['upload_path'] . $agenciaEditar->imagen_age);
                }
            } else {
                // Manejar errores de carga de archivo según sea necesario
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                return;
            }
        } else {
            // Si no se está cargando una nueva imagen, mantener la imagen existente
            $nombre_archivo_subido = $agenciaEditar->imagen_age;
        }

        // Datos de la agencia para actualizar
        $datosAgencia = array(
            "gerente_age" => $this->input->post("gerente_age"),
            "fecha_apertura_age" => $this->input->post("fecha_apertura_age"),
            "telefono_age" => $this->input->post("telefono_age"),
            "email_age" => $this->input->post("email_age"),
            "provincia_age" => $this->input->post("provincia_age"),
            "ciudad_age" => $this->input->post("ciudad_age"),
            "direccion_age" => $this->input->post("direccion_age"),
            "latitud_age" => $this->input->post("latitud_age"),
            "longitud_age" => $this->input->post("longitud_age"),
            "imagen_age" => $nombre_archivo_subido
        );

        // Actualizar la Agencia
        $this->Agencia->actualizar($id_age, $datosAgencia);
        $this->session->set_flashdata('mensaje', 'La agencia fue actualizada correctamente');
        redirect("agencias/index");
    }
}
