<?php

class Corresponsal extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert('corresponsal', $datos);
    }

    function obtenerTodos()
    {
        $corresponsal = $this->db->get('corresponsal');
        if ($corresponsal->num_rows() > 0) {
            return $corresponsal->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where('id_cor', $id);
        $corresponsal = $this->db->get('corresponsal');
        if ($corresponsal->num_rows() > 0) {
            return $corresponsal->row();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {
        $this->db->where('id_cor', $id);
        return $this->db->delete('corresponsal');
    }

    function actualizar($id, $datos)
    {
        $this->db->where('id_cor', $id);
        return $this->db->update('corresponsal', $datos);
    }
}
