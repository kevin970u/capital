<?php

class Cajero extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert('cajero', $datos);
    }

    function obtenerTodos()
    {
        $cajeros = $this->db->get('cajero');
        if ($cajeros->num_rows() > 0) {
            return $cajeros->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where('id_caj', $id);
        $cajero = $this->db->get('cajero');
        if ($cajero->num_rows() > 0) {
            return $cajero->row();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {
        $this->db->where('id_caj', $id);
        return $this->db->delete('cajero');
    }

    function actualizar($id, $datos)
    {
        $this->db->where('id_caj', $id);
        return $this->db->update('cajero', $datos);
    }
}
