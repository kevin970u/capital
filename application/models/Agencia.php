<?php

class Agencia extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert('agencia', $datos);
    }

    function obtenerTodos()
    {
        $agencias = $this->db->get('agencia');
        if ($agencias->num_rows() > 0) {
            return $agencias->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where('id_age', $id);
        $agencia = $this->db->get('agencia');
        if ($agencia->num_rows() > 0) {
            return $agencia->row();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {

        try {
            $this->db->where('id_age', $id);
            return $this->db->delete('agencia');
        } catch (\Throwable $th) {
            return false;
        }

    }

    function actualizar($id, $datos)
    {
        $this->db->where('id_age', $id);
        return $this->db->update('agencia', $datos);
    }
}
