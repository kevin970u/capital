<?php

class Banco extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function obtenerTodos()
    {
        $bancos = $this->db->get('banco');
        if ($bancos->num_rows() > 0) {
            return $bancos->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where('id_ban', $id);
        $banco = $this->db->get('banco');
        if ($banco->num_rows() > 0) {
            return $banco->row();
        } else {
            return false;
        }
    }

    function actualizar($id, $datos)
    {
        $this->db->where('id_ban', $id);
        return $this->db->update('banco', $datos);
    }

}
