$.validator.addMethod(
	"lettersonly",
	function (value, element) {
		return this.optional(element) || /^[a-zA-Z\s]*$/.test(value);
	},
	"Solo se permiten letras en este campo"
);

$("#form-add-agencia").validate({
	rules: {
		gerente_age: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		fecha_apertura_age: {
			required: true,
			date: true,
		},
		telefono_age: {
			required: true,
			digits: true,
			maxlength: 10,
			minlength: 10,
		},
		email_age: {
			required: true,
			email: true,
			maxlength: 50,
		},
		provincia_age: {
			required: true,
			maxlength: 50,
		},
		ciudad_age: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		direccion_age: {
			required: true,
			maxlength: 50,
		},
		latitud_age: {
			required: true,
		},
		longitud_age: {
			required: true,
		},
		imagen_age: {
			maxlength: 50,
		},
	},
	messages: {
		gerente_age: {
			required: "Por favor, ingrese el nombre del gerente de la agencia",
			maxlength: "El nombre del gerente no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		fecha_apertura_age: {
			required: "Por favor, ingrese la fecha de apertura de la agencia",
			date: "Por favor, ingrese una fecha válida",
		},
		telefono_age: {
			required: "Por favor, ingrese el teléfono de la agencia",
			digits: "Por favor, ingrese solo números",
			maxlength: "El teléfono no puede superar los 10 caracteres",
			minlength: "El teléfono debe tener 10 caracteres",
		},
		email_age: {
			required: "Por favor, ingrese el email de la agencia",
			email: "Por favor, ingrese un email válido",
			maxlength: "El email no puede superar los 50 caracteres",
		},
		provincia_age: {
			required: "Por favor, seleccione la provincia",
			maxlength: "La provincia no puede superar los 50 caracteres",
		},
		ciudad_age: {
			required: "Por favor, ingrese la ciudad de la agencia",
			maxlength: "La ciudad no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		direccion_age: {
			required: "Por favor, ingrese la dirección de la agencia",
			maxlength: "La dirección no puede superar los 50 caracteres",
		},
		latitud_age: {
			required: "Por favor, ingrese la latitud de la agencia",
		},
		longitud_age: {
			required: "Por favor, ingrese la longitud de la agencia",
		},
		imagen_age: {
			required: "Por favor, seleccione una imagen de la agencia",
			maxlength: "El nombre de la imagen no puede superar los 50 caracteres",
		},
	},
});


$("#form-editar-banco").validate({
	rules: {
		nombre_ban: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		telefono_ban: {
			required: true,
			digits: true,
			maxlength: 10,
			minlength: 10,
		},
		email_ban: {
			required: true,
			email: true,
			maxlength: 50,
		},
		fecha_apertura_ban: {
			required: true,
			date: true,
		},
		provincia_ban: {
			required: true,
			maxlength: 50,
		},
		ciudad_ban: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		direccion_ban: {
			required: true,
			maxlength: 50,
		},
		mision_ban: {
			required: true,
			maxlength: 500,
		},
		vision_ban: {
			required: true,
			maxlength: 500,
		},
		latitud_ban: {
			required: true,
		},
		longitud_ban: {
			required: true,
		},
	},
	messages: {
		nombre_ban: {
			required: "Por favor, ingrese el nombre del banco",
			maxlength: "El nombre del banco no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		telefono_ban: {
			required: "Por favor, ingrese el teléfono del banco",
			digits: "Por favor, ingrese solo números",
			maxlength: "El teléfono no puede superar los 10 caracteres",
			minlength: "El teléfono debe tener 10 caracteres",
		},
		email_ban: {
			required: "Por favor, ingrese el email del banco",
			email: "Por favor, ingrese un email válido",
			maxlength: "El email no puede superar los 50 caracteres",
		},
		fecha_apertura_ban: {
			required: "Por favor, ingrese la fecha de apertura del banco",
			date: "Por favor, ingrese una fecha válida",
		},
		provincia_ban: {
			required: "Por favor, seleccione la provincia",
			maxlength: "La provincia no puede superar los 50 caracteres",
		},
		ciudad_ban: {
			required: "Por favor, ingrese la ciudad del banco",
			maxlength: "La ciudad no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		direccion_ban: {
			required: "Por favor, ingrese la dirección del banco",
			maxlength: "La dirección no puede superar los 50 caracteres",
		},
		mision_ban: {
			required: "Por favor, ingrese la mision del banco",
			maxlength: "La mision no puede superar los 500 caracteres",
		},
		vision_ban: {
			required: "Por favor, ingrese la vision del banco",
			maxlength: "La vision no puede superar los 500 caracteres",
		},
		logo_ban: {
			required: "Por favor, seleccione una imagen del banco ",
		},
		latitud_age: {
			required: "Por favor, ingrese la latitud del banco",
		},
		longitud_age: {
			required: "Por favor, ingrese la longitud del banco",
		},
		
	},
});


$("#form-add-cajero").validate({
	rules: {
		id_age: {
			required: true,
		},
		tipo_caj: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		modelo_caj: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		provincia_caj: {
			required: true,
		},
		ciudad_caj: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		fecha_apertura_caj: {
			required: true,
			maxlength: 50,
		},
		capacidad_caj: {
			required: true,
			digits: true,
			max: 150000,
			min: 0,
		},
		direccion_caj: {
			required: true,
			maxlength: 50,
		},
		imagen_caj: {
			required: true,
		},
		latitud_caj: {
			required: true,
		},
		longitud_caj: {
			required: true,
		},
	},
	messages: {
		id_age: {
			required: "Por favor, seleccione la agencia",
		},
		tipo_caj: {
			required: "Por favor, ingrese el tipo de cajero",
			maxlength: "El tipo de cajero no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		modelo_caj: {
			required: "Por favor, ingrese el modelo del cajero",
			lettersonly: "Por favor, ingrese solo letras",
		},
		provincia_caj: {
			required: "Por favor, seleccione la provincia",
		},
		ciudad_caj: {
			required: "Por favor, ingrese la ciudad del cajero",
			maxlength: "La ciudad no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		direccion_caj: {
			required: "Por favor, ingrese la dirección del cajero",
			maxlength: "La dirección no puede superar los 50 caracteres",
		},
		fecha_apertura_caj: {
			required: "Por favor, ingrese la fecha de apertura de apertura",
			date: "Por favor, ingrese una fecha válida",
		},
		capacidad_caj: {
			required: "Por favor, ingrese la capacidad del cajero",
			max: "La capacidad del cajero no puede superar los 50 000",
			min: "La capacidad del cajero debe tener 0 caracteres",
		},
		imagen_caj: {
			required: "Por favor, seleccione una imagen del cajero ",
		},
		latitud_caj: {
			required: "Por favor, ingrese la latitud del cajero",
		},
		longitud_caj: {
			required: "Por favor, ingrese la longitud del cajero",
		},
		
	},
});


$("#form-add-corresponsal").validate({
	rules: {
		id_age: {
			required: true,
		},
		tipo_negocio_cor: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		fecha_apertura_cor: {
			required: true,
			date: true,
		},
		propietario_cor: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		telefono_cor: {
			required: true,
			digits: true,
			maxlength: 10,
			minlength: 10,
		},
		provincia_cor: {
			required: true,
		},
		ciudad_cor: {
			required: true,
			maxlength: 50,
			lettersonly: true,
		},
		direccion_cor: {
			required: true,
			maxlength: 100,
		},
		contrato_cor: {
			required: true,
		},
		latitud_cor: {
			required: true,
		},
		longitud_cor: {
			required: true,
		},
	},
	messages: {
		id_age: {
			required: "Por favor, seleccione la agencia",
		},
		tipo_negocio_cor: {
			required: "Por favor, ingrese el tipo de negocio",
			maxlength: "El tipo de negocio no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		fecha_apertura_cor: {
			required: "Por favor, ingrese la fecha de apertura de apertura",
			date: "Por favor, ingrese una fecha válida",
		},
		propietario_cor: {
			required: "Por favor, ingrese el nombre del propietario",
			maxlength: "El nombre del propietario no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		telefono_cor: {
			required: "Por favor, ingrese el teléfono del corresponsal",
			digits: "Por favor, ingrese solo números",
			maxlength: "El teléfono no puede superar los 10 caracteres",
			minlength: "El teléfono debe tener 10 caracteres",
		},
		provincia_cor: {
			required: "Por favor, seleccione la provincia",
			maxlength: "La provincia no puede superar los 50 caracteres",
		},
		ciudad_cor: {
			required: "Por favor, ingrese la ciudad del corresponsal",
			maxlength: "La ciudad no puede superar los 50 caracteres",
			lettersonly: "Por favor, ingrese solo letras",
		},
		direccion_cor: {
			required: "Por favor, ingrese la dirección del corresponsal",
			maxlength: "La direccion del corresponsal no puede superar los 50 caracteres",
		},
		contrato_cor: {
			required: "Por favor, seleccione el contrato que tiene el corresponsal",
		},
		latitud_cor: {
			required: "Por favor, ingrese la latitud del corresponsal",
		},
		longitud_cor: {
			required: "Por favor, ingrese la longitud del corresponsal",
		},
		
	},
});